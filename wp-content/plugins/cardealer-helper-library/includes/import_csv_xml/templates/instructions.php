<div class="clear"></div>
<div class="cdhl_configuration postbox metabox-holder">
	<h2 class="hndle"><?php esc_html_e('Troubleshooting', 'cardealer-helper')?></h2>
	<div class="inside" id="cdhl_troubleshoot_accordion">
		<h3><?php esc_html_e('Common Server Configuration Issues', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p>Import files are usually rejected because the file you’re trying to upload exceeds your server’s hard limits on file size. Here are the most common hard limits that users encounter:
					</p>
					<p><strong>Maximum Upload File Size (PHP):&nbsp;</strong>This is set in php.ini with<strong> upload_max_filesize</strong>. It determines the maximum file size that your server will allow to be uploaded.&nbsp;This value must be larger than the size of the file you wish to upload to import process.
					</p>
					<p><strong>Maximum Post Size (PHP):&nbsp;</strong>This is set in php.ini with&nbsp;<strong>post_max_size</strong>. It determines the maximum file size allowed to be used in PHP process.&nbsp;This should be set higher&nbsp;than upload_max_filesize.
					</p>
					<p><strong>Memory Limit (PHP):&nbsp;</strong>This is set in php.ini with&nbsp;<strong>memory_limit</strong>. It determines how much memory a script is able to allocate. This should&nbsp;be set higher&nbsp;than post_max_size.
					</p>
					<p><strong>Maximum Execution Time (PHP):&nbsp;</strong>This is&nbsp;set in php.ini with&nbsp;<strong>max_execution_time</strong>. It&nbsp;determines how long a process is allowed to run before it’s terminated.&nbsp;You can ask your host to increase the limit, but this should be considered a&nbsp;last resort.</p>
					If you’re not able to successfully complete an import, it may be that your server is missing necessary components.Please check <a href="%1$s" target="_blank" rel="noopener noreferrer">server configuration</a> page.</p>', 'cardealer-helper'),
					array(
						'p' => array(),
						'strong' => array(),
						'a'=> array(
							'href'=> array(),
							'target'=> array(),
						)
					)
				),
				esc_url_raw( admin_url('admin.php?page=cardealer-system-status') )
			);
			?>
		</div>
		<h3><?php esc_html_e('Invalid file type', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p>There are pretty strict rules that CSV files must conform to in order for them to work. You must upload csv file with .csv extension to import. You can validate your data and URL using following free online service:
					</p>
					<p>CSV:&nbsp;<a href="%1$s" target="_blank">%1$s</a></p>', 'cardealer-helper'),
					array(
						'p'=> array(),
						'strong'=> array(),
						'a'=> array(
							'href'=> array(),
							'target'=> array(),
						)
					)
				),
				esc_url('http://csvlint.io')
			);
			?>
		</div>
		<h3><?php esc_html_e('Is Import not working as expected?', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p>This process will solve pretty much any problem you’re having:</p>
					<ol>
						<li>Make sure you are using the latest versions of WordPress, Car Dealer - Helper Library plugin and Car Dealer Theme.</li>
						<li>Eliminate other themes and plugins which are not belong to Car Dealer Theme as the potential cause of a conflict. Deactivate all other active&nbsp;WordPress&nbsp;plugins.</li>
						<li>Ask your host to check your server’s error log to see if&nbsp;something is stopping Import from&nbsp;working properly. Often,&nbsp;artificial limits on script execution time&nbsp;or&nbsp;MySQL queries prevent import process from finishing imports.</li>
					</ol>', 'cardealer-helper'),
					array(
						'p'=> array(),
						'ol'=> array(),
						'li'=> array(),
						'a'=> array(
							'href'=> array(),
							'target'=> array(),
						)
					)
				),
				esc_url('https://themeforest.net/downloads')
			);
			?>
		</div>
		<h3><?php esc_html_e('Common Issues', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p><strong>Drag and drop not working? Admin screens look strange? </strong></p>
					<p>First, try clearing your browser cache or using a different web browser. If the problem persists, the issue is usually other plugin conflict.</p>
					<p><strong>Can’t get from one step to another?</strong></p><p>Broken PHP session settings will prevent you from being able to move between the different steps of the import process. If you suspect this is the cause, you’ll need to contact your host. Also try from first step.</p>
					<p><strong>Running in to a Security Check error?</strong></p><p>Clear your browser cache or try using a different browser.</p>
					<p><strong>Concerned your server isn’t properly configured?</strong></p><p>If your imports complete successfully, your server is properly configured. If you’re not able to successfully complete an import, it may be that your server is missing necessary components.Please check <a href="%1$s" target="_blank" rel="noopener noreferrer">server configuration</a> page.</p>', 'cardealer-helper'),
					array(
						'p'=> array(),
						'a'=> array( 'href'=> array(), 'target'=> array() ),
						'strong'=> array(),
					)
				),
				esc_url_raw( admin_url('admin.php?page=cardealer-system-status') )
			);
			?>
		</div>
		<h3><?php esc_html_e('Encoding', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p><strong>CSV Files Must Be UTF-8 Or UTF-8 Compatible </strong></p>
					<p>Since there is no way to specify the character encoding for a CSV file, Import just assumes all uploaded CSV files are UTF-8, which is the standard character encoding for the web.</p><p>This works the vast majority of the time, but if you are importing a CSV file with special characters (things like umlauts, non-Latin characters, etc.) and those special characters aren’t appearing correctly in the posts created by Import, try opening your CSV file in a text editor using the correct encoding, and then re-save it using UTF-8.</p>', 'cardealer-helper'),
					array(
						'p'=> array(),
						'strong'=> array(),
					)
				)
			);
			?>
		</div>
		<h3><?php esc_html_e('Import Speed', 'cardealer-helper')?></h3>
		<div>
			<?php 
			printf( 
				wp_kses( 
					__('<p><strong>Slow Imports - An Explanation</strong></p><p>There are three main issues that will affect import speed:</p>
					<p><strong>1. Server Resources: </strong>If your server doesn’t have enough available resources this will slow down your import. So if you see that your server is maxing out its CPU or memory usage during an import, upgrading to a more powerful server will help.</p>
					<p><strong>2. Database Size:</strong>&nbsp;WordPress stores data in a SQL database. During the import process, it does the actual importing by interacting with your database. The larger your database, the longer each interaction will take.</p>
					<p><strong>3. External Images: </strong>Downloading images from somewhere else during your import will always make your import take longer. If the images are large, or the server providing the images is slow, the impact will be even greater.</p>
					<p><strong>Specific Problems & Solutions</strong></p>
					<p><strong>1. Large Databases, Bloated Tables, and Big Import Files</strong></p>
					<p>As your database grows in size your imports will take longer. It is not possible to avoid this slowdown because as your database grows larger your server and import process must do more work.</p>
					<p>For example, when checking for duplicates, import process must search through more records. When updating existing posts with new data, the more posts on the site, the longer it takes MySQL to find the correct post to update.</p>
					<p><strong><em>▸ </em><em>Solution</em></strong></p>
					<p>Sometimes your database is bigger than it should be. We’ve seen some users suffering from slow imports, and when we look inside their database we find that the wp_options table has over a million entries. The most common reason for wp_options bloat&nbsp;is the transients cache, which you can manage with a plugin like <a href="%1$s">Transients Manager</a>. Check with your host or a database professional to see if your database is similarly bloated.</p>
					<p>If you’re trying to build a site with&nbsp;500,000 products, you’ll need a very powerful and professionally optimized server.</p>
					<p>Typically on a shared host, somewhere between 50,000 to 100,000 records seems to be the upper limit. Anything beyond that often results in server timeouts, slow imports, and long load times.</p>
					<p><strong>2. Shared Hosts and Low Server Resources</strong></p>
					<p>All servers were not created equally. Many times hosting companies will cram as many sites as possible into their hardware to keep costs down. Sometimes they will limit the amount of PHP processing time you are allowed or the number of SQL queries you’re able to make. Or maybe they’ll throttle your processing power. Or maybe they don’t do anything, but the 1,000 other websites running on the same piece of hardware are consuming so many resources that your import has to fight for CPU time to finish processing.</p>
					<p><strong><em>▸ </em><em>Solution</em></strong></p>
					<p>If you’re running low on available resources, upgrading to a server with more available resources (amount of RAM, number of CPUs, etc) will help. But it will usually&nbsp;only help if you’re running low on available resources. While the&nbsp;actual clock speed of your server’s&nbsp;CPU, RAM, and disk do play a role in how long your import takes, it’s&nbsp;not a very big one.</p>
					<p>Think of it like a moving truck.</p>
					<p>If you’re moving and your truck&nbsp;is too small you’ll have to make more trips and this will slow you down.&nbsp;Buying a bigger truck&nbsp;will help. But if your truck&nbsp;is already big enough to fit all of your stuff, buying a bigger truck (more CPUs, RAM, etc) isn’t going to help you move any faster. Upgrading to a server with a faster CPU and disk would be like putting a new engine in your&nbsp;truck. Sure, your move might go a little faster,&nbsp;but it should not be the focus of your efforts.</p>
					
					', 'cardealer-helper'),
					array(
						'p'=> array(),
						'strong'=> array(),
					)
				),
				esc_url('https://wordpress.org/plugins/transients-manager/')
			);
			?>
		</div>
	</div>
</div>
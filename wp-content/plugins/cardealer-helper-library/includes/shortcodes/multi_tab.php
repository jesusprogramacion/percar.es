<?php
/******************************************************************************
 *
 * Shortcode : cd_multi_tabs
 *
 ******************************************************************************/
add_shortcode( 'cd_multi_tabs', 'cdhl_shortcode_multi_tabs' );
function cdhl_shortcode_multi_tabs($atts) {
	extract( shortcode_atts(  array(
        'number_of_item'=>5,
        'number_of_column'=>4,
		'multi_tabs' => 'multi_tab_1',
		'tab_type' => 'pgs_new_arrivals',
        'car_make_slugs' => array(),
        'element_id'      => uniqid(),
        'vehicle_category' => '',
	   ), $atts )
	);
	extract( $atts );
    
    if(empty($car_make_slugs)){
       return; 
    }    
	
	// car compare code
	if(isset($_COOKIE['cars']) && !empty($_COOKIE['cars'])){
		$carInCompare = json_decode($_COOKIE['cars']);
	}
    
	
	$car_make_slugs = explode(',',$car_make_slugs);
    
	ob_start();
        ?>		
        <div data-uid="<?php echo esc_attr($element_id)?>" data-active="<?php echo esc_attr($car_make_slugs[0])?>" class="isotope-filters multi-tab  multi-tab-isotope-filter  multi-tab-isotope-filter-<?php echo esc_attr($element_id)?>">
            <?php
            foreach( $car_make_slugs as $tab):?>
            	<button data-uid="<?php echo esc_attr($element_id);?>" <?php if($car_make_slugs[0]==$tab){ echo 'class="active"';} ?> data-filter="<?php echo '.'.$tab.'_'.$element_id?>"><?php echo esc_html($tab);?></button>
                <?php
            endforeach;?>
       </div>
	   <div class="horizontal-tabs isotope cd-multi-tab-isotope-<?php echo esc_attr($element_id);?> column-<?php echo esc_attr($number_of_column);?>">
			<?php
			$args=array(
                'post_type' => 'cars',
                'posts_status' => 'publish',
                'posts_per_page' => $number_of_item,
            );
			if($tab_type=='pgs_featured'){
				/* Featured cars */
				$args['meta_query']=array(
							array('key'     => 'featured',
								  'value'   => '1',
							      'compare' => '='
							),
						);
			}elseif($tab_type=='pgs_on_sale'){
				/* On Sale cars */
				$args['meta_query']=array(
                    'relation' => 'AND',
            		array(
            			'key'     => 'sale_price',
						'value'   => '',
						'compare' => '!=',
                        'type'    => 'NUMERIC'
            		),
					array(
                        'key'     => 'regular_price',
						'value'   => '',
						'compare' => '!=',
                        'type'    => 'NUMERIC'
					)
				);               
				$args['meta_value_num'] = 'sale_price';
				$args['orderby'] = 'meta_value_num';
				$args['order'] = 'asc';
			}elseif($tab_type=='pgs_cheapest'){
				/* Cheapest Product */
				unset($args['meta_query']);
				$args['meta_query'] = array(
        			'relation' => 'AND',
                    array(
                        'key'     => 'final_price',
						'value'   => '',
						'compare' => '!=',
                        'type'    => 'NUMERIC'
					)
        		);                
				$args['meta_value_num'] = 'final_price';
				$args['orderby'] = 'meta_value_num';
				$args['order'] = 'asc';
			}
            $vehicle_category = trim($vehicle_category);
            if(!empty($vehicle_category)){
        		$vehicle_cat_array = array(
    				'taxonomy' => 'vehicle_cat',
    				'field' => 'slug',
    				'terms' => $vehicle_category
        		);		
        	}
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'car_make',
                    'field'    => 'slug',
                    'terms'    => 'sss',
                ) 
            );                            
                     
            foreach( $car_make_slugs as $tab):
                
                $car_make_array = array(
        			'taxonomy' => 'car_make',
        			'field'    => 'slug',
        			'terms'    => $tab,
        		);                
                if(!empty($vehicle_category)){
                    $args['tax_query'] = array(
                		'relation' => 'AND',                    
                        $vehicle_cat_array,
                        $car_make_array
                	);
                } else {
                    $args['tax_query'] = array(
                        $car_make_array
                	);    
                }
    			$loop = new WP_Query( $args );$data='';
                if($loop->have_posts()){
                    while ( $loop->have_posts() ) : $loop->the_post();                        
                        $cars_cat_slug='';
                        $terms = get_the_terms( get_the_ID(), 'car_make');                        
                        if(empty($terms)){                           
                            $style='';                    
                            if(isset($padding) && $padding  > 0){
                                $style = 'style="padding:'.$padding.'px;"';
                            }
                            echo '<div class="grid-item no-data '.$tab.'_'.$element_id.'" '.$style.'><div class="no-data-found">'.esc_html__("No vehicle found","cardealer-helper").'.</div></div>';
                        }else{
                            foreach ($terms  as $term  ) {
                                $cars_cat_id = $term->term_id;
                                $cars_cat_slug .= $term->slug.'_'.$element_id.' ';
                                $carscatslug[] = $term->slug;                            
                            }
                            $style='';                        
                            if(isset($padding) && $padding  > 0){
                                $style = 'style="padding:'.$padding.'px"';
                            }                    
                            ?>
                            <div class="grid-item <?php echo esc_attr($cars_cat_slug)?> grid-item-<?php echo esc_attr($element_id)?>" <?php echo $style;?>>
                                    <?php
                                    if($multi_tabs == "multi_tab_2"){                                    
                                        ?>
                                        <div class='car-item text-center'>
                                            <div class='car-image'>
                                                <?php
                                                $id = get_the_ID();                            
                                                $is_hover_overlay = cardealer_is_hover_overlay();
                                                do_action( 'cardealer_car_loop_link_open', $id, $is_hover_overlay  );
                                                /**                            				 
                                                 * car_before_overlay_banner hook.
                                                 * 
                                                 * @hooked cardealer_get_cars_condition - 10
                                                 * @hooked cardealer_get_cars_status - 20                              				 
                                                 */
                                                do_action( 'car_before_overlay_banner', $id,true  );                                                                    
                                                echo (function_exists('cardealer_get_cars_image'))? cardealer_get_cars_image('car_tabs_image') : '';
                                                if( $is_hover_overlay == 'yes' ){ ?>                        
                                                    <div class='car-overlay-banner'>
                                                        <ul> 
                                                            <?php
                                                            /**                            				 
                                                			 * car_overlay_banner hook.
                                                             * 
                                                			 * @hooked cardealer_view_cars_overlay_link - 10
                                                			 * @hooked cardealer_compare_cars_overlay_link - 20
                                                             * @hooked cardealer_images_cars_overlay_link - 30                            				 
                                                			 */
                                                            do_action( 'car_overlay_banner', $id ); 
                                                            ?>                                              
                                                        </ul>
                                                    </div>
                                                    <?php 
                                                }
                                                do_action( 'cardealer_car_loop_link_close', $id, $is_hover_overlay  );
                                                if(function_exists('cardealer_get_cars_list_attribute')){cardealer_get_cars_list_attribute();}?>                                                 
											</div>                        
                                            <div class='car-content'>
                                                <?php
                                                /**                            				 
                                    			 * cardealer_list_car_title hook.
                                                 * 
                                    			 * @hooked cardealer_list_car_link_title - 5
                                    			 * @hooked cardealer_list_car_title_separator - 10                                         				 
                                    			 */
                                                do_action( 'cardealer_list_car_title' );
                                                ?>
                                                <?php cardealer_car_price_html();
                                                cardealer_get_vehicle_review_stamps($id)?>                            
                                            </div>                        
                                        </div>
                                        <?php
                                    }else{?>
                                        <div class="car-item-3">                                            
                                            <?php echo cardealer_get_cars_image("car_tabs_image");?>                                       
                                            <div class="car-popup">
                                                <a class="popup-img" href="<?php echo esc_url(get_the_permalink());?>"><i class="fa fa-plus"></i></a>
                                            </div>                                                                
                                            <div class="car-overlay text-center">
											<a class="link" href="<?php echo esc_url(get_the_permalink());?>"><?php the_title()?></a>
                                            </div>                                                                                
                                        </div>
                                        <?php  
                                    }?>                            
                            </div>
                            <?php
                        }                        
        			endwhile;
                    wp_reset_postdata();                                    
                } else {                        
                    $style='style=""';                     
                    if(isset($padding) && $padding  > 0){
                        $style = 'style="padding:'.$padding.'px;"';
                    }
                    echo '<div class="grid-item no-data '.$tab.'_'.$element_id.'" '.$style.'><div class="no-data-found">'.esc_html__("No vehicle found","cardealer-helper").'.</div></div>';
                }
            endforeach;
            /*End cars type tab */?>
		</div>
    <?php	
    return ob_get_clean();
}

/******************************************************************************
 *
 * Visual Composer Integration
 *
 ******************************************************************************/
function cdhl_multi_tabs_shortcode_vc_map() {
	if ( function_exists( 'vc_map' ) ) {
		$car_make = array();
        $car_make= cdhl_get_terms( array( // You can pass arguments from get_terms (except hide_empty)
			'taxonomy'  => 'car_make',
		));
        $vehicle_cat = cdhl_get_terms( array('taxonomy' => 'vehicle_cat') );
        $array1 = array(
            array(
					'type'       => 'cd_radio_image',
					'heading'    => esc_html__("Tabs type", 'cardealer-helper'),
					'param_name' => 'multi_tabs',
					'options'    => cdhl_get_shortcode_param_data('cd_multi_tabs'),
				),
				array(
                    "type"        => "cd_number_min_max",
                    "class"       => "",
                    'edit_field_class'=> 'vc_col-sm-4 vc_column',
                    "heading"     => esc_html__("Number of item", 'cardealer-helper'),                    
                    "param_name"  => "number_of_item",
					"admin_label" => false,
                ),
                array(
    				'type'        => 'cd_number_min_max',
    				'heading'     => esc_html__('Padding', 'cardealer-helper'),                
    				'edit_field_class'=> 'vc_col-sm-4 vc_column',
                    'param_name'  => 'padding',
                    'min'             => '1',
                    'max'             => '200',                    
    			),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Number of column', 'cardealer-helper' ),
                    'param_name' => 'number_of_column',
                    'edit_field_class'=> 'vc_col-sm-4 vc_column',
                    'value' => array( 
                        esc_html__('3','cardealer-helper')=>'3',
                        esc_html__('4','cardealer-helper')=>'4',
                        esc_html__('5','cardealer-helper')=>'5',                        
    				),
                    'default' => '4',
                    'save_always' => true,                    
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => esc_html__( 'Select makes', 'cardealer-helper' ),
                    'param_name' => 'car_make_slugs',
                    'value' => $car_make,                    
                ),                
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Display Vehicles Type', 'cardealer-helper' ),
                    'edit_field_class'=> 'vc_col-sm-6 vc_column',
					'param_name' => 'tab_type',
                    'value' => array( 
                        esc_html__('Newest','cardealer-helper')=>'pgs_new_arrivals',
                        esc_html__('Featured','cardealer-helper')=>'pgs_featured',                        
                        esc_html__('On sale','cardealer-helper')=>'pgs_on_sale',
                        esc_html__('Cheapest','cardealer-helper')=>'pgs_cheapest',
    				),
                    'save_always' => true,                
                    'description' => esc_html__( 'Select vehicles type for tabs. which will be displayed on frontend', 'cardealer-helper' ),
                )
        );
        $array2 = array();
        if(isset($vehicle_cat) && !empty($vehicle_cat)){
            $vehicle_cat_array = array('Select'=>'')+ $vehicle_cat;;
            $array2 = array(
                array(
    				'type'            => 'dropdown',
    				'heading'     => esc_html__('Vehicle Category', 'cardealer-helper'),
    				'param_name'  => 'vehicle_category',
    				'value'       => $vehicle_cat_array,
    				'edit_field_class'=> 'vc_col-sm-6 vc_column',    				
    				'save_always' => true,
                    'admin_label' => true,
                    'default'     => ''					
    			)
            );
        }
        $params = array_merge($array1,$array2);			
        vc_map( array(
        	"name"                    => esc_html__( "Potenza Multi Tabs", 'cardealer-helper' ),            	
        	"base"                    => "cd_multi_tabs",
        	"class"                   => "cardealer_helper_element_wrapper",
        	"controls"                => "full",
            "icon"                    => cardealer_vc_shortcode_icon( 'cd_multi_tabs' ),
        	"category"                => esc_html__('Potenza', 'cardealer-helper'),            	
        	"params"                  => $params
        ) );
	}
}
add_action( 'vc_before_init', 'cdhl_multi_tabs_shortcode_vc_map' );
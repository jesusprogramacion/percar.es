<?php
/**
 * Register meta box(es).
 */
function wpdocs_register_meta_boxes() {    
    global $car_dealer_options;
    $map_option = $car_to_pro_map_option = (isset($car_dealer_options['car_to_pro_map_option']))?$car_dealer_options['car_to_pro_map_option']:0;    
    if ( !is_plugin_active('woocommerce/woocommerce.php') || !$map_option ){
        return;
    }
    add_meta_box( 'cdhl-car-to-product-mapping', esc_html__( 'Vehicle Mapping with Woo', 'cardealer-helper' ), 'cdhl_car_to_product_mapping', 'cars', 'side' );
}
add_action( 'add_meta_boxes', 'wpdocs_register_meta_boxes' );
 
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function cdhl_car_to_product_mapping( $post ) {
    
    $car_to_woo_product_id = get_post_meta($post->ID, "car_to_woo_product_id", true);
        // Query args
    $args = array(
    	'post_type'      => 'product',
    	'posts_per_page' => -1
    );
    
    $loop = get_posts( $args );    
    if ( ! empty( $loop ) ) {
        $html  = '<label for="car_to_woo_product">WooCommerce Product list</label>';
        $html .= '<select name="car_to_woo_product_id" id="car_to_woo_product_meta_id" class="postbox">';
            $html .= '<option value="">'.esc_html__( 'Select','cardealer-helper' ).'</option>';        
            foreach ( $loop as $product ) {
            	$current_id = $product->ID;    
            	$html .= "<option value='" . $current_id . "'" . selected( $current_id, $car_to_woo_product_id, false ) . ">" . $product->post_title . "</option>";
            }            
        $html .= '</select>';
    } else {
        $html = esc_html__( "No product found!","cardealer-helper" );
    }    
    wp_reset_query();
    echo $html;
}



 
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function wpdocs_save_meta_box( $post_id ) {    
    // Set final price
	if ( get_post_type( $post_id ) == 'cars' && isset($_POST['car_to_woo_product_id'])) {		
        update_post_meta( $post_id, 'car_to_woo_product_id', $_POST['car_to_woo_product_id']);
	}
}
add_action( 'save_post', 'wpdocs_save_meta_box' );
?>
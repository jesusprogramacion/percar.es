<?php   
/*
 * This function used to compare cars.
 */
add_action('wp_ajax_car_compare_action', 'cdhl_car_compare_action' );
add_action('wp_ajax_nopriv_car_compare_action', 'cdhl_car_compare_action' );
function cdhl_car_compare_action(){	
	cdhl_get_template_part( 'content', 'compare' );
    die;    
}
?>
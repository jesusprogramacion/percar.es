<?php   
/**
 * Compare popup title
 */
if ( ! function_exists( 'cdhl_compare_popup_title' ) ) {
	
	function cdhl_compare_popup_title( $echo = true ) {

		$popup_title = esc_html__('Compare Vehicles', 'cardealer-helper'); 

		$popup_title = apply_filters( 'cdhl_compare_popup_title', $popup_title );

		if ( $echo ) {
			echo $popup_title; // WPCS: XSS ok.
		} else {
			return $popup_title;
		}
	}
}


/**
 * Compare popup title
 */
if ( ! function_exists( 'cdhl_compare_column_fields' ) ) {
	
	function cdhl_compare_column_fields() {

		$data = array(
            'remove' => '',
            'car_image' => '',
            'price' => esc_html__('Price', 'cardealer-helper'),
            'year' => esc_html__('Year', 'cardealer-helper'),
            'make' => esc_html__('Make', 'cardealer-helper'),
            'model' => esc_html__('Model', 'cardealer-helper'),
            'body_style' => esc_html__('Body Style', 'cardealer-helper'),
            'mileage' => esc_html__('Mileage', 'cardealer-helper'),
            'fuel_economy' => esc_html__('Fuel Economy', 'cardealer-helper'),
            'transmission' => esc_html__('Transmission', 'cardealer-helper'),
            'condition' => esc_html__('Condition', 'cardealer-helper'),
            'drivetrain' => esc_html__('Drivetrain', 'cardealer-helper'),
            'engine' => esc_html__('Engine', 'cardealer-helper'),
            'exterior_color' => esc_html__('Exterior Color', 'cardealer-helper'),
            'interior_color' => esc_html__('Interior Color', 'cardealer-helper'),
            'stock_number' => esc_html__('Stock Number', 'cardealer-helper'),
            'vin_number' => esc_html__('Vin Number', 'cardealer-helper'),
            'features_options' => esc_html__('Features & Options', 'cardealer-helper')                        
        );
        $data = apply_filters( 'cdhl_compare_column_fields', $data );
        return $data;
	}
}


/** 
 * 
 * @see cdhl_compare_column_delete()
 * @see cdhl_compare_column_image()
 * @see cdhl_compare_column_price()
 */
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_delete', 10, 2 );
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_image', 20, 2 );
add_action( 'cdhl_compare_column_before_attributes', 'cdhl_compare_column_price', 30, 2 );
if ( ! function_exists( 'cdhl_compare_column_delete' ) ) {	
	function cdhl_compare_column_delete( $class, $car_id ) {        
        $html = '<tr class="delete">';
			$html .= '<td class="'.$class.'" data-id="'.esc_attr($car_id).'">';	
				$html .= '<a href="javascript:void(0)" data-car_id="'.esc_attr($car_id).'" class="drop_item"><span class="remove">x</span></a>';
			$html .= '</td>';
		$html .= '</tr>';
        echo $html;
	}
}

if ( ! function_exists( 'cdhl_compare_column_image' ) ) {	
	function cdhl_compare_column_image( $class, $car_id ) {        
        $html = '<tr class="image">';
			$html .= '<td class="'.$class.'" data-id="'.esc_attr($car_id).'">';
				$html .= '<a href="'.esc_url($carlink).'">'; 
					$html .= (function_exists('cardealer_get_cars_image'))?cardealer_get_cars_image('car_thumbnail', $car_id):'';
				$html .= '</a>';
			$html .= '</td>';
		$html .= '</tr>';
        echo $html;
	}
}

if ( ! function_exists( 'cdhl_compare_column_price' ) ) {	
	function cdhl_compare_column_price( $class, $car_id ) {
        
        $html = '<tr class="price car-item">';
        	$html .= '<td class="'.$class.'" data-id="'.esc_attr($car_id).'">';
        		$html .= cardealer_get_car_price('', $car_id);
        	$html .= '</td>';
        $html .= '</tr>';
        echo $html;
	}
}

/** 
 * 
 * @see cdhl_compare_column_attributes_data() 
 */
add_action( 'cdhl_compare_column_attributes', 'cdhl_compare_column_attributes_data', 10, 3 );
if ( ! function_exists( 'cdhl_compare_column_attributes_data' ) ) {	
	function cdhl_compare_column_attributes_data( $compare_fields, $class, $car_id) {
        $html = '';
        $cars_taxonomy_array = cdhl_get_cars_taxonomy();        
        foreach( $compare_fields as $key => $val ){
            $cars_taxonomy = "car_$key"; 
            if(in_array( $cars_taxonomy, $cars_taxonomy_array )) {
                $html .= '<tr class="'.$key.'">';
                	$html .= '<td class="'.$class.'" data-id="'.esc_attr($car_id).'">';        	
                		$car_year = wp_get_post_terms($car_id, $cars_taxonomy);
                		$html .= (!isset($car_year) || empty($car_year))? '&nbsp;' : $car_year[0]-> name;        	
                	$html .= '</td>';
                $html .= '</tr>';                
            }
            if( $key === 'features_options'){
                $html .= '<tr class="'.$key.'">';
                    $html .= '<td class="'.$class.'" data-id="'.esc_attr($car_id).'">';
                		$html .= '<div>';                			
            				$car_features_options = wp_get_post_terms($car_id, 'car_features_options');
            				$json  = json_encode($car_features_options); // Conver Obj to Array
            				$car_features_options = json_decode($json, true); // Conver Obj to Array
            				$name_array = array_map(function ($options) {return $options['name'];}, (array) $car_features_options); // get all name term array
            				$options = implode(',', $name_array);
            				$options_data = (!isset($options) || empty($options))? '&nbsp;' : $options;
                			$html .= $options_data; 
                		$html .= '</div>';
                	$html .= '</td>';
                $html .= '</tr>';
            }                            
        }
        echo $html;        
	}
}
?>
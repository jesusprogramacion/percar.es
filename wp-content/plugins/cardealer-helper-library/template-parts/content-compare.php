<?php
global $car_dealer_options;
$carIds = array();
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'car_compare_action') {
	$carIds = json_decode($_REQUEST['car_ids'], true);
	if(empty($carIds)) return;
	$num_of_cars = count($carIds);
	?>
	<div class="modal-header">
		<button type="button" class="close_model" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h1><?php cdhl_compare_popup_title();?></h1>
	</div>
	<div class="modal-content">
		<div class="table-Wrapper">
			<div class="heading-Wrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<?php $compare_fields = cdhl_compare_column_fields();                            
                        foreach( $compare_fields as $key => $val ){
                            echo '<tr><td class="'.$key.'">'.$val.'</td></tr>';    
                        }
                    ?>                        
				</table>
			</div>
			<div class="table-scroll modal-body" id="getCode">
				<div id="sortable" style="width:<?php echo esc_attr($num_of_cars * 258);?>px;">
					<?php
						for( $cols=1; $cols <= $num_of_cars; $cols++ ) {
							$car_id = $carIds[ $cols - 1 ];
							$carlink = get_permalink($car_id);
							$class = ($cols%2 == 0)? 'even': 'odd';
                            ?>
							<div class="compare-list compare-datatable" data-id="<?php echo esc_attr($car_id);?>">	
								<table class="compare-list compare-datatable" width="100%" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<?php
                                        /**                            				 
                        				 * cdhl_compare_column_before_attributes hook.
                                         * 
                        				 * @hooked cdhl_compare_column_delete - 10
                        				 * @hooked cdhl_compare_column_image - 20
                                         * @hooked cdhl_compare_column_price - 30                            				 
                        				 */
                        				do_action( 'cdhl_compare_column_before_attributes', $class, $car_id  );                                        
                                        
                                        /**                            				 
                        				 * cdhl_compare_column_attributes hook.
                                         * 
                        				 * @hooked cdhl_compare_column_attributes - 10                            				                          				 
                        				 */
                        				do_action( 'cdhl_compare_column_attributes', $compare_fields, $class, $car_id  );?>    										
									  </tbody>
								</table>
							</div>
				<?php 	}?>
				</div>
			</div>
		</div>	
	</div>
<?php
}	
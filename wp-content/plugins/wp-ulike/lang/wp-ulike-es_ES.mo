��    <      �  S   �      (     )     /     7  	   @     J  
   S     ^  
   k  
   v     �     �     �     �  	   �     �     �     �     �     �     �     �     �          	               #     (     .  	   4     >     D     T     j     }     �     �     �  	   �     �     �     �     �     �     �     �  
   �     �                         $     -  	   5     ?  	   G     Q     T  j  W  	   �	     �	     �	     �	     �	     �	     �	  
   
     
     
     '
  
   3
  	   >
     H
     U
     [
     g
     v
     z
     �
     �
     �
     �
     �
  	   �
     �
  	   �
     �
     �
     �
            (   )  "   R  
   u     �     �     �     �     �     �     �     �     �  	   �     	               %     .     2     8     E     M     V  
   d     o     t     {     
      !          '   (      <   -             +   ,   &          %   0   5           #   ;              2          8                 	       9                     1      4   /                                      :   6              .      "      )   *                   $   7                3        About Actions Activate Alignment Archives Background Border Color BuddyPress Categories Center Comments Contact Credits Customize Days Default Documentation FAQ For more information: General Get Started Getting Started Home ID Left Like this plugin? Logs Month Pages Permalink Posts Project Leaders Recent Comments Liked Recent Posts Liked Reviews Right Save Settings Search Results Select %s Settings saved. Show on screen Status Summary Support Tags Text Text Color Themes Title: Today Total Translations Username Version View Logs Welcome Yesterday on or Project-Id-Version: WP ULike
Report-Msgid-Bugs-To: https://wordpress.org/plugins/wp-ulike/
POT-Creation-Date: 2017-11-20 12:11:13+00:00
PO-Revision-Date: 2018-03-13 11:42+0100
Last-Translator: Alimir <info@alimir.ir>
Language-Team: Spanish (Spain) (http://www.transifex.com/projects/p/wp-ulike/language/es_ES/)
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: .
X-Poedit-KeywordsList: _e;__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Sobre mí Acciones Activar Alineación Archives Fondo Color del borde BuddyPress Categorías Centrar Comentarios Contacto:  Créditos Personalizar Días Por defecto Documentación FAQ Para más información: General Comienza Cómo empezar Inicio ID Izquierda ¿Te gusta este plugin? Registros Mes Páginas Enlace permanente Publicaciones Líderes de proyecto Comentarios recientes que me han gustado Posts recientes que me han gustado Revisiones Derecha Salvar Ajustes Resultados de la búsqueda Seleccionar %s Ajustes guardados. Mostrar en pantalla Estado Resumen Soporte Etiquetas Texto Color del texto Temas Título: Hoy Total Traducciones Usuario Versión Ver registros Bienvenido Ayer activa o 
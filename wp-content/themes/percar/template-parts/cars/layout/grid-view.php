<div <?php cardealer_grid_view_class()?>>
    <div class="car-item gray-bg text-center <?php echo cardealer_cars_loop();?>">        
        <div class="car-image">            
            <?php                        
            $id = get_the_ID();
            echo cardealer_get_cars_condition($id);
            echo cardealer_get_cars_status($id);
            echo cardealer_get_cars_image('car_catalog_image',$id);			
            ?>            
            <div class="car-overlay-banner">
                <ul>
                    <?php
                    /**                            				 
        			 * car_overlay_banner hook.
                     * 
        			 * @hooked cardealer_view_cars_overlay_link - 10
        			 * @hooked cardealer_compare_cars_overlay_link - 20
                     * @hooked cardealer_images_cars_overlay_link - 30                            				 
        			 */
                    do_action( 'car_overlay_banner', $id  ); 
                    ?>
                </ul>
            </div><?php			            
            cardealer_get_cars_list_attribute($id);?>
        </div>                                               
        <div class="car-content">                
            <?php
            /**                            				 
			 * cardealer_list_car_title hook.
             * 
			 * @hooked cardealer_list_car_link_title - 5
			 * @hooked cardealer_list_car_title_separator - 10                                         				 
			 */
            do_action( 'cardealer_list_car_title' );
            
            cardealer_car_price_html($id);
            cardealer_get_vehicle_review_stamps($id);
            ?>                            
        </div>
    </div>
</div>
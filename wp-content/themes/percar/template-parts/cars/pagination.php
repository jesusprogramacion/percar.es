<?php
/**
 * Pagination - Show numbered pagination for catalog pages
 * 
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div id="cars-pagination-nav" class="pagination-nav text-center">
    <?php cardealer_cars_pagination();?>    
</div>
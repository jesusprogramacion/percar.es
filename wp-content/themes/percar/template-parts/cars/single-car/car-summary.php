<div class="car-details-sidebar">
    <div class="details-block details-weight">
        <h5><?php esc_html_e('DESCRIPTION','cardealer')?></h5>
        <?php cardealer_get_cars_attributes(get_the_ID())?>        
    </div>    
    <?php cardealer_add_vehicale_to_cart(get_the_ID())?>    
</div>
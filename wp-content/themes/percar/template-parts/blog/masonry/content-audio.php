<?php 
$audio_file = get_post_meta(get_the_ID(),'audio_file',true);
$audio_file_data = false;
if( $audio_file ){
	$audio_file_data = cardealer_acf_get_attachment( $audio_file );
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog-2 blog-no-image">
		<?php
		if($audio_file_data){
			?>
			<div class="blog-image blog-entry-audio audio-video">
				<audio id="player2" src="<?php echo esc_url($audio_file_data['url']);?>" width="100%" controls="controls"></audio>
				<div class="date-box">
					<span><?php echo sprintf( '%1$s',esc_html( get_the_date('M Y') ));?></span>
				</div>
			</div>
			<?php
		}	
		
		?>
		<div class="blog-content">
			<div class="blog-admin-main">
				<div class="blog-admin">
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?>
					<span><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );?>"><?php echo get_the_author();?></a></span>
				</div>
				<div class="blog-meta pull-right">
					<ul>
						<li><a href="<?php echo esc_url( get_comments_link(get_the_ID()) )?>"> <i class="fa fa-comment"></i><br /> <?php $comments_count = wp_count_comments(get_the_ID()); echo esc_html($comments_count->total_comments);?></a></li>
						<li class="share"><a href="#"> <i class="fa fa-share-alt"></i><br /> ...</a>
							<?php
							global $car_dealer_options;
							$facebook_share   = $car_dealer_options['facebook_share']; 
							$twitter_share    = $car_dealer_options['twitter_share'];
							$linkedin_share   = $car_dealer_options['linkedin_share'];
							$google_plus_share= $car_dealer_options['google_plus_share'];
							$pinterest_share  = $car_dealer_options['pinterest_share'];

							if($facebook_share!='' || $twitter_share!='' || $linkedin_share!='' || $google_plus_share!='' || $pinterest_share!=''){
							?>
							
								<div class="blog-social"> 
									<ul>
									<?php if($facebook_share){?>
										<li> 
											<a href="#" class="facebook-share" data-title="<?php echo esc_attr(get_the_title())?>" data-url="<?php echo esc_url(get_permalink())?>"><i class="fa fa-facebook"></i></a>
										</li>
									<?php }
										if($twitter_share){?>
										<li>
											<a href="#"  data-title="<?php echo esc_attr(get_the_title())?>" data-url="<?php echo esc_url(get_permalink())?>" class="twitter-share"><i class="fa fa-twitter"></i></a>
										</li>
									<?php }
										if($linkedin_share){?>
										<li>
											<a href="#"  data-title="<?php echo esc_attr(get_the_title())?>" data-url="<?php echo esc_url(get_permalink())?>" class="linkedin-share"><i class="fa fa-linkedin"></i></a>
										</li>
									<?php }
										if($google_plus_share){?>
										<li>
											<a href="#"  data-title="<?php echo esc_attr(get_the_title())?>" data-url="<?php echo esc_url(get_permalink())?>" class="googleplus-share"><i class="fa fa-google-plus"></i></a>
										</li>
									<?php }
										if($pinterest_share){?>
										<li>
											<a href="#"  data-title="<?php echo esc_attr(get_the_title())?>" data-url="<?php echo esc_url(get_permalink())?>" class="pinterest-share" data-image="<?php the_post_thumbnail_url('full')?>"><i class="fa fa-pinterest-p"></i></a>
										</li>
									<?php }
										?>
									</ul>
							   </div>
							<?php
							}
						 ?>
						</li>
					</ul>
				</div>
			</div>
			<div class="blog-description text-center">
			<?php
				if(!is_single()){
				?>	
				<div class="entry-title">
					<i class="fa fa-file-audio-o"></i> 
				</div>	
				<?php 
				}?>
				<a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_html(get_the_title());?></a>
				<div class="separator"></div>
				<?php the_excerpt();?>
			</div>
		</div>
	</div>
</article><!-- #post -->
<?php
 /**
 * Get sold cars query arguments.
 * @return query arguments array
 */
function get_sold_query(){
    $data_html='';$pagination_html='';$cars_orderby="date (post_date)";$data_order="asc";
    global $car_dealer_options;
    parse_str($_SERVER['QUERY_STRING'], $params);
    $per_page = 12;$cars_order = "date (post_date)";
    if(isset($car_dealer_options['cars-per-page'])) {
    	$per_page = $car_dealer_options['cars-per-page'];
    }
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    if(isset($params['cars_pp']) && !empty($params['cars_pp'])){
       $per_page = $params['cars_pp'];
    }
    
    if(isset($params['cars_order']) && !empty($params['cars_order'])){
       $data_order = $params['cars_order'];
    }
    
    $args=array(
        'post_type' => 'cars',
    	'post_status' => 'publish',
    	'posts_per_page' => $per_page,
        'order' => $data_order,
        'paged' => $paged,
    );
    
    $pgs_min_price = isset( $params['min_price'] ) ? esc_attr( $params['min_price'] ) : 0;
	$pgs_max_price = isset( $params['max_price'] ) ? esc_attr( $params['max_price'] ) : 0;
	if($pgs_min_price > 0  || $pgs_max_price > 0 ){
		$prices = cardealer_get_sold_car_filtered_price();
		$min    = floor( $prices->min_price );
		$max    = ceil( $prices->max_price );

		if($min != $pgs_min_price || $max != $pgs_max_price){
		   $args['meta_query'][] = array(
									'key'     => 'final_price',
									'value'   => array( $pgs_min_price, $pgs_max_price ),
									'compare' => 'BETWEEN',
									'type'    => 'NUMERIC',
								);
		}
	}
    
    
    if(isset($params['cars_orderby']) && !empty($params['cars_orderby'])){
       $cars_orderby = $params['cars_orderby'];
    }
    
    if(isset($cars_orderby) && !empty($cars_orderby)){
        if($cars_orderby == "sale_price"){
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = 'final_price';
        } else {
            $args['orderby'] = $cars_orderby;
        }
    
    }
    
    $args['meta_query'][] =
    array(
    	'key'     => 'car_status',
    	'value'   => 'sold',
    	'compare' => '='
    );
    return $args;    
}

function cardealer_sold_list_layout_style(){
    $lay_style = 'view-grid-full'; 
    if(isset($_GET['lay_style']) && $_GET['lay_style'] == 'view-list-full'){
        $lay_style = 'view-list-full';
    }
    return $lay_style;              
}



/**
 * Sold cars price filter slider html 
 */
 if ( ! function_exists( 'cardealer_get_sold_cars_price_filters' ) ) :
function cardealer_get_sold_cars_price_filters () {
	$pgs_min_price = isset( $_GET['min_price'] ) ? esc_attr( $_GET['min_price'] ) : '';
	$pgs_max_price = isset( $_GET['max_price'] ) ? esc_attr( $_GET['max_price'] ) : '';

	// Find min and max price in current result set
	$prices = cardealer_get_sold_car_filtered_price();
	$min    = floor( $prices->min_price );
	$max    = ceil( $prices->max_price );

	if ( $min === $max ) {
		return;
	}

    $html='';
    $html .= '<div class="price_slider_wrapper">';
        $html .= '<div class="price-slide">';
            $html .= '<div class="price">';
                $html .= '<input type="hidden" id="pgs_min_price" name="min_price" value="' . esc_attr( $pgs_min_price ) . '" data-min="' . esc_attr( $min ) . '" />';
                $html .= '<input type="hidden" id="pgs_max_price" name="max_price" value="' . esc_attr( $pgs_max_price ) . '" data-max="' . esc_attr( $max ) . '" />';

                $html .= '<label for="dealer-slider-amount">'.esc_html__('Price Range','cardealer').'</label>';
                $html .= '<input type="text" id="dealer-slider-amount" readonly="" class="amount" value="" />';
                $html .= '<div id="slider-range"></div>';
            $html .= '</div>';
        $html .= '</div>';
	$html .= '</div>';
    echo $html;
 }
 endif;
 
 
 /**
 * Get filtered min price for current list query.
 * @return int
 */
function cardealer_get_sold_car_filtered_price() {
	global $wpdb;

    // Current site prefix
    $tbprefix = $wpdb->prefix;
    $sql   = "SELECT ";
    $sql  .= " min( FLOOR( price_meta.meta_value ) ) as min_price,";
    $sql  .= " max( CEILING( price_meta.meta_value ) ) as max_price";
    $sql  .= " FROM ".$tbprefix."posts";
    $sql  .= " LEFT JOIN ".$tbprefix."postmeta as price_meta ON ".$tbprefix."posts.ID = price_meta.post_id";
    $sql  .= " INNER JOIN ".$tbprefix."postmeta ON (".$tbprefix."posts.ID = ".$tbprefix."postmeta.post_id )";
    $sql  .= " WHERE ".$tbprefix."posts.post_type IN ('cars')";
    $sql  .= " AND ".$tbprefix."posts.post_status = 'publish'";
    $sql  .= " AND ".$tbprefix."postmeta.meta_key = 'car_status'";
    $sql  .= " AND ".$tbprefix."postmeta.meta_value = 'sold'";
    $sql  .= " AND price_meta.meta_key IN ('sale_price','regular_price')";	
    return $wpdb->get_row( $sql );

}



/**
* Sold cars layout grid / list view icon buttons html
*/
function cardealer_get_sold_view(){    
    global $car_dealer_options;
    $theme_color = isset($car_dealer_options['site_color_scheme_custom']['color'])?$car_dealer_options['site_color_scheme_custom']['color'] : '';
    
    add_filter('cardealer_list_layout_style','cardealer_sold_list_layout_style');
    $getlayout = cardealer_get_cars_list_layout_style();
    
    $grid_sts = (isset($getlayout) && $getlayout == "view-grid-full") ?  "act" : '';
    $list_sts = (isset($getlayout) && $getlayout == "view-list-full") ?  "act" : '';
    
    $class2 = (isset($getlayout) && $getlayout == "view-grid-full") ?  "style='background-color:$theme_color'" : '';    
    $class5 = (isset($getlayout) && $getlayout == "view-list-full") ?  "style='background-color:$theme_color'" : '';
    $html = '';
    $html .= '<div class="grid-view change-view-button">';
        $html .= '<div class="view-icon">';
            $html .= '<a class="catlog-layout-sold view-grid-sold" data-sts="'.$grid_sts.'" data-id="view-grid-full" href="javascript:void(0)"><span '.$class2.'><i class="view-grid-full"></i></span></a>';
            $html .= '<a class="catlog-layout-sold view-list-sold" data-sts="'.$list_sts.'" data-id="view-list-full" href="javascript:void(0)"><span '.$class5.'><i class="view-list-full"></i></span></a>';
        $html .= '</div>';
    $html .= '</div><!--.grid-view-->';
    echo $html;
}


/**
* Sold cars per page / sort by / order by html   
*/
if ( ! function_exists( 'cardealer_cars_sold_ordering' ) ) :
function cardealer_cars_sold_ordering() {
    global $wp,$car_dealer_options;
	parse_str($_SERVER['QUERY_STRING'], $params);
	$query_string = '?'.$_SERVER['QUERY_STRING'];    
	// replace it with theme option
	if(isset($car_dealer_options['cars-per-page'])) {
		$per_page = $car_dealer_options['cars-per-page'];
	} else {
		$per_page = 12;
	}

    $pob = cardealer_get_default_sort_by();//get default option value
    if(isset($params['cars_orderby']) && !empty($params['cars_orderby'])){
        $pob = $params['cars_orderby'];
    }

    $po = cardealer_get_default_sort_by_order();//get default option value
    if(isset($params['cars_order']) && !empty($params['cars_order'])){
        $po = $params['cars_order'];
    }

	$pc = !empty($params['cars_pp']) ? $params['cars_pp'] : $per_page;
    $html = '';
        $html .= '<div class="selected-box">';
                $html .= '<select name="cars_pp" id="pgs_cars_pp_sold">';
                    $html .= '<option value="'.esc_attr($per_page).'" '.(( $pc == $per_page) ? 'selected': '' ).'>'.esc_attr($per_page).'</option>';
                    $html .= '<option value="'.( esc_attr($per_page*2) ).'" '.(( $pc == $per_page*2) ? 'selected': '' ).'>'.( esc_attr($per_page*2) ).'</option>';
                    $html .= '<option value="'.( esc_attr($per_page*3) ).'" '.(( $pc == $per_page*3) ? 'selected': '' ).'>'.( esc_attr($per_page*3) ).'</option>';
                    $html .= '<option value="'.( esc_attr($per_page*4) ).'" '.(( $pc == $per_page*4) ? 'selected': '' ).'>'.( esc_attr($per_page*4) ).'</option>';
                    $html .= '<option value="'.( esc_attr($per_page*5) ).'" '.(( $pc == $per_page*5) ? 'selected': '' ).'>'.( esc_attr($per_page*5) ).'</option>';
                $html .= '</select>';
        $html .= '</div>';
        $html .= '<div class="selected-box">';
            $html .= '<div class="select">';
                $html .= '<select class="select-box" name="cars_orderby" id="pgs_cars_orderby_sold">';
                    $html .= '<option value="">'.esc_html__('Sort by ', 'cardealer').esc_html__('Default', 'cardealer').' </option>';
                    $html .= '<option value="'.esc_attr('name').'" '.(( $pob == 'name') ? 'selected': '' ).'>'.esc_html__('Sort by ', 'cardealer').esc_html__('Name', 'cardealer').' </option>';
                    $html .= '<option value="'.esc_attr('sale_price').'" '.(( $pob == 'sale_price') ? 'selected': '' ).'>'.esc_html__('Sort by ', 'cardealer').esc_html__('Price', 'cardealer').' </option>';
                    $html .= '<option value="'.esc_attr('date').'" '.(( $pob == 'date') ? 'selected': '' ).'>'.esc_html__('Sort by ', 'cardealer').esc_html__('Date', 'cardealer').' </option>';
                $html .= '</select>';
            $html .= '</div>';
        $html .= '</div>';
        if($po == 'asc'):
            $html .= '<div class="cars-order text-right"><a id="pgs_cars_order_sold" class="cars-order-sold" data-order="desc" data-current_order="asc" href="javascript:void(0)"><i class="fa fa-arrow-up"></i></a></div>';
        endif;
    	if($po == 'desc'):
    	   $html .= '<div class="cars-order text-right"><a id="pgs_cars_order_sold" class="cars-order-sold" data-order="asc" data-current_order="desc" href="javascript:void(0)"><i class="fa fa-arrow-down"></i></a></div>';
    	endif;        
	echo $html;
}
endif;
?>
<!DOCTYPE html>
<?php //if(is_home()){ ?>
<html <?php language_attributes(); ?> class="no-js"  ng-app="SearcherApp" class="ng-cloak">
<?php //}else{ ?>
<!-- <html <?php //language_attributes(); ?> class="no-js"> -->
<?php //} ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="alola.es" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<?php if(is_archive('coches-km0/ofertas-vigo-pontevedra')){ ?>
	<meta name="title" content="Vehículos km 0 - Percar | Concesionario Oficial Audi y Volkswagen" />
	<title>Vehículos km 0 - Percar | Concesionario Oficial Audi y Volkswagen</title>
	<?php } ?>

	<?php do_action( 'cardealer_head_before' );?>
	<?php wp_head(); ?>   


	<!-- Global site tag (gtag.js) - Google Analytics --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71084692-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-71084692-1');
	</script>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49247128 = new Ya.Metrika2({
                    id:49247128,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });
 
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";
 
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2"); </script> 
    <noscript><div><img src="https://mc.yandex.ru/watch/49247128" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

</head>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
<?php
	global $hide_header_banner;
	$postID = cardealer_get_current_post_id();
	$hide_header_banner = get_post_meta($postID,'hide_header_banner',true);
	if( !empty( $hide_header_banner ) && ( is_search() ) ){
		$hide_header_banner = false;
	}
	do_action( 'cardealer_page_before' );
?>	
	<!-- Main Body Wrapper Element -->
	<div id="page" class="hfeed site page-wrapper <?php echo ( $hide_header_banner && !is_front_page() )? 'header-hidden' : ''; ?>">
       <?php if( !is_page('cita-previa') && !is_page_template( 'templates/promociones.php' )){?>
		<div class="cita-previa-fix"><i class="fa fa-calendar-check-o icon-cita" aria-hidden="true"></i><a href="<?php echo get_site_url(); ?>/cita-previa" title="Solicita aquí tu cita previa">Cita previa</a></div>
		<?php }?>
		<!-- preloader -->
		<?php cardealer_display_loader(); ?>
		
		<!-- header -->
		<?php get_template_part('template-parts/header/site_header'); ?>
		<div class="wrapper" id="main">
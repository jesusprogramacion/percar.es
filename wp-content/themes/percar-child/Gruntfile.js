module.exports = function(grunt) {

    // Initialize configuration object
    grunt.initConfig({
        // Read in project settings
        pkg: grunt.file.readJSON('package.json'),
        // User editable project settings & variables
        options: {
            // Base path to your public assets folder
            base: 'js',
            bow: 'bower_components',
            node: 'node_modules',
            js: {
                base: '<%= options.base %>', // Base path to your JS folder
                bow: '<%= options.bow %>', // Base path to your JS folder
                node: '<%= options.node %>', // Base path to your JS folder
                files: [
                // ocasion


                    '<%= options.js.bow %>/angular/angular.js',
                    '<%= options.js.base %>/angular-route.min.js',
                    '<%= options.js.base %>/angular/app.js',
                    '<%= options.js.base %>/angular/controllers.js',
                    '<%= options.js.base %>/angular/filters.js',
                    '<%= options.js.base %>/angular/directives.js',
                    '<%= options.js.base %>/angular/services.js',

                    '<%= options.js.bow %>/angular-slick/dist/slick.min.js',
                    '<%= options.js.bow %>/slick-carousel/slick/slick.min.js',
                    
                    '<%= options.js.base %>/ui-niceselect.min.js',

                    '<%= options.js.bow %>/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
                    '<%= options.js.bow %>/ng-scrollbars/dist/scrollbars.min.js',

                    '<%= options.js.node %>/angular-ui-bootstrap/dist/ui-bootstrap.js',
                    '<%= options.js.base %>/js/angular-resource.min.js',
                    '<%= options.js.node %>/angular-sanitize/angular-sanitize.min.js',
                    '<%= options.js.node %>/angular-sanitize/angular-sanitize.min.js.map.js',

                    '<%= options.js.base %>/js/jPages.min.js',

                    '<%= options.js.node %>/angular-owl-carousel2/src/angular-owl-carousel-2.js',




                ],   // JavaScript files in order you'd like them concatenated and minified
                concat: '<%= options.js.base %>/uglify-ocasion.js',     // Name of the concatenated JavaScript file
                min: '<%= options.js.base %>/uglify-ocasion.min.js'    // Name of the minified JavaScript file

                //concat: '<%= options.js.base %>/uglify/uglify.js',     // Name of the concatenated JavaScript file
                //min: '<%= options.js.base %>/uglify/uglify.min.js'    // Name of the minified JavaScript file
            },

        },

        concat: {

            js: {
                options: {
                    block: true,
                    line: true,
                    stripBanners: true
                },
                files: {
                    '<%= options.js.concat %>': '<%= options.js.files %>'
                }
            }
        },

        uglify: {
            options: {
                preserveComments: false
            },
            js: {
                src: '<%= options.js.concat %>',
                dest: '<%= options.js.min %>'
            }
        },

    });

    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.loadNpmTasks('grunt-contrib-uglify');
     grunt.loadNpmTasks('grunt-contrib-clean');


    grunt.registerTask('production', [ 'concat',  'uglify' ]);
};

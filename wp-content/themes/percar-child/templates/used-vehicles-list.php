<?php

/**
 * Template Name: Ocasión - listado
 *
 * @package WordPress
 * @subpackage Total
 * @since 1.0
 */


/// Set the slider




get_header('ocasion');
// the_post();
// global $rd_data;
// global $bp;
// $header_top_bar = get_post_meta( $post->ID, 'rd_top_bar', true );
// $header_transparent = get_post_meta( $post->ID, 'rd_header_transparent', true );
// $p_sidebar = get_post_meta( $post->ID, 'rd_sidebar', true );
// $title = get_post_meta($post->ID, 'rd_title', true);
// $title_height = get_post_meta($post->ID, 'rd_title_height', true);
// $title_color = get_post_meta($post->ID, 'rd_title_color', true);
// $titlebg_color = get_post_meta($post->ID, 'rd_titlebg_color', true);
// $ctbg = get_post_meta($post->ID, 'rd_ctbg', true);
// $bc = get_post_meta($post->ID, 'rd_bc', true);
// $sb_style = $rd_data['rd_sidebar_style'];
// $content_border_color = $rd_data['rd_content_border_color'];

?>

    <!--start-body-section-->

    <div class="wrapper-template">

        <div class="container">

            <div class="row">

                <?php //include_once('searcher.php'); ?>

                <?php include_once('partials-ocasion/vehicles-list.php'); ?>

            </div>

            <?php //include_once( 'sibucascoche-powered.html' ); ?>

        </div>

    </div>

<?php get_footer(); ?>
<?php

/**
 * Template Name: Ocasión - página principal
 *
 * @package WordPress
 * @subpackage Total
 * @since 1.0
 */


get_header('ocasion');

?>

<!--start-body-section-->

    <div class="wrapper-template">

        <?php include_once('partials-ocasion/vehicles-search.php'); ?> 
        <div class="container" >

            <div class="row">

                <?php include_once('partials-ocasion/vehicles-list.php'); // vehicles-in-offer-slider.php ?> 

            </div>

        </div>

    </div>


<?php get_footer(); ?>
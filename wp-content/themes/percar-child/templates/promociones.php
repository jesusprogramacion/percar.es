<?php
/**
 * Template Name: Promociones
 * Description: template para promociones
 *
 * @package Alola
 * @author Alola media
 */
get_header();
   
    get_template_part('template-parts/content','intro');    
    ?>
<div class="template-promociones">
    <div class="site-content container" id="primary">
    	<div role="main" id="content">
    		<div class="container">
    			<div class="row">
	    			<div class="col-md-12 col-sm-12">
				    <?php
				    while ( have_posts() ) : the_post();     
				   // echo do_shortcode(the_content());       
				        get_template_part('template-parts/promocode/content','promocode');
				    endwhile; // end of the loop. ?>

				</div>
				</div>
			</div>
    	</div>
    </div>
</div>

<?php get_footer();?>

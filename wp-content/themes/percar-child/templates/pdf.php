<?php
/**
 * Template Name: pdf
 * Description: pdf información vehículo.
 *
 * @package Alola
 * @author Alola media
 */



//include( plugin_dir_path( __FILE__ ) . 'cardealer-helper-library/includes/pdf_generator/doPdf.php');
require_once( ABSPATH . 'wp-load.php' );

$id = $_GET['id'];

$post_title =  get_the_title( $id );
$new_title = sanitize_title($post_title);
$file_name = $id."_".$new_title.".pdf";


$car_price=cardealer_get_car_price_array($id);

$car_images = get_field('car_images',$id);



$caryear = wp_get_post_terms($id, 'car_year');
$fecha_matriculacion = (isset($caryear) && !empty($caryear))? $caryear[0]->name : '';

$carmake = wp_get_post_terms($id, 'car_make');
$marca = (isset($carmake) && !empty($carmake))? $carmake[0]->name : '';

$carmodel = wp_get_post_terms($id, 'car_model');
$modelo = (isset($carmodel) && !empty($carmodel))? $carmodel[0]->name : '';



$mileage = wp_get_post_terms($id, 'car_mileage');
$kilometraje = (isset($mileage) && !empty($mileage))? $mileage[0]->name : '';


$engine = wp_get_post_terms($id, 'car_engine');
$motor = (isset($engine) && !empty($engine))? $engine[0]->name : '';

// $fuel_economy = wp_get_post_terms($id, 'car_fuel_economy');
// $car_fuel_economy = (isset($fuel_economy) && !empty($fuel_economy))? $fuel_economy[0]->name : '';

// $exterior_color = wp_get_post_terms($id, 'car_exterior_color');
// $car_exterior_color = (isset($exterior_color) && !empty($exterior_color))? $exterior_color[0]->name : '';

$interior_color = wp_get_post_terms($id, 'car_interior_color');
$cambio = (isset($interior_color) && !empty($interior_color))? $interior_color[0]->name : '';


$fuel_type = wp_get_post_terms($id, 'car_fuel_type');
$combustible = (isset($fuel_type) && !empty($fuel_type))? $fuel_type[0]->name : '';

$info = get_post_meta($id, 'general_information', true);
$general_information = (isset($info) && !empty($info))? $info : '';

$tec = get_post_meta($id, 'technical_specifications', true);
$especificaciones_tecnicas = (isset($tec) && !empty($tec))? $tec : '';

$option="";


//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */
$url_plugin = $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/cardealer-helper-library/includes/pdf_generator/';
// Include the main TCPDF library (search for installation path).
//require_once('pdf/examples/tcpdf_include.php');
require_once( $url_plugin. 'pdf/examples/tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// // Include the main TCPDF library (search for installation path).
// require_once('tcpdf_include.php');

// // create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Percar');
$pdf->SetTitle('Percar -'. $marca.' '.$modelo);
$pdf->SetSubject('Dossier información vehículo');
$pdf->SetKeywords('Vehículo, venta, percar, ocasion, km 0');

// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
//     require_once(dirname(__FILE__).'/lang/eng.php');
//     $pdf->setLanguageArray($l);
// }

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
// $pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
// $img = file_get_contents('https://percar.aloladev.com/wp-content/themes/percar-child/images/logo-pr.jpg');
$pdf->setJPEGQuality(113);



// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$pdf->SetXY(10, 10);

$pdf->Image($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/percar-child/images/logo-pr.jpg', 145, '', 50, '', 'JPG', '', '', false, 150, '', false, false, false, false, false, false);
$logo = $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/percar-child/images/logo-pr.jpg';

 if(function_exists('get_field')){
 		$images_cont = '';
        $images = get_field('car_images', $id);
        $imagen_prin = $_SERVER['DOCUMENT_ROOT'].substr(parse_url($images[0]['sizes']['car_single_slider'])['path'],1);

        if( isset($images) && !empty($images) ){
        	$i=1;
            foreach( $images as $image ){
            			 
                ${'imag_'.$i} = $_SERVER['DOCUMENT_ROOT'].substr(parse_url($images[$i]['sizes']['car_single_slider'])['path'],1);
                $i++;
           }
        }
}
$regular_price = function_exists('get_field')?get_field('regular_price',$id):get_post_meta($id,'regular_price', $single= true);
if( !empty($regular_price) && ( $regular_price > 0 ) )
	{
		$regular_price = number_format($regular_price, 0, ',', '.') ;
	}

setlocale(LC_TIME,"es_ES");


$fecha = strftime("%d de %B del %Y");


$html = <<<EOD
<style>
.caracteristicas td{
	border-bottom:1pt solid #CBCBCB;
	line-heigth:1.6;
	font-size:12px;
	color:#414141;
}
.presentacion,.equipamiento,.galeria{
	font-size:12px;
}
.separate{
	width:100%;
	height:20px;
}
.precio{
	background-color:#9ebde4;
	font-size:20px;
	color:white;
	margin-top:20px;
}
</style>

<div style="width:100%;height:140px;"></div>
<div style="font-size:12px;padding-top:300px;color:#414141;margin-bottom:20px">Estimado Sr./Sra.:</div>
<div style="font-size:12px;padding-top:40px;color:#414141">Gracias por interesarse por el vehículo $marca $modelo $motor. Estamos a su disposición para
ayudarle a tomar la mejor decisión de compra para que adquiera un coche que satisfaga sus expectativas.
</div>
<div style="text-align:right;width:100%;font-size:12px;color:#414141"><i>$fecha</i></div>
<h1 style="color:#9ebde4;padding-top:50px;font-size:19px;"><strong>$marca</strong> $modelo $motor</h1>


<table class="presentacion">
	<tr>
	 	<td>
			<img src="$imagen_prin" style="width:320px;display:block;text-align:center">
		</td>
		<td>
			<table style="padding-top:10px;">
			  <tr>
			 	<td>
			 	<table class="caracteristicas"  cellpadding="5">
					  <tr valign="middle" style="vertical-align:middle">
					    <td width='100px'>Combustible</td><td width='100px' align="right">$combustible</td>
					   </tr>
					  <tr valign="middle">
					    	<td width='100px'>Cambio</td><td width='100px' align="right">$cambio</td> 
					   </tr>
					   <tr valign="middle">
					    	<td width='100px'>Kilometraje</td><td width='100px' align="right">$kilometraje</td>
					    </tr>
					    <tr valign="middle">
					    	<td width='100px'>Fecha de matriculación</td><td width='100px' align="right">$fecha_matriculacion</td>
					  </tr>
					  </table>
			 	</td>
			  </tr>

			</table>
	  	</td>
	</tr>

</table>
<div class="separate"></div>

<table cellpadding="10" class="equipamiento">
  <tr>
    <td width=100% style="margin-top:20px;font-size:16px;margin-right:20px;background-color:#9ebde4;padding:50px;color:white;font-weight:bold">Equipamiento</td>
  </tr>
   <tr style="padding-top:20px;">
    <td width=100% style="font-size:12px;color:#414141">$especificaciones_tecnicas</td>
  </tr>
</table>

<div class="separate"></div>

<div id="apDiv1">
<table width="100%" cellpadding="5" class="galeria">
<tr>
	<td style="padding-right:10px"><img src="$imag_1"></td>
	<td><img src="$imag_2"></td>
	<td><img src="$imag_3"></td>
	<td><img src="$imag_4"></td>
</tr>
<tr>
	<td><img src="$imag_5"></td>
	<td><img src="$imag_6"></td>
	<td><img src="$imag_7"></td>
	<td><img src="$imag_8"></td>
</tr>
<tr>
	<td><img src="$imag_9"></td>
	<td><img src="$imag_10"></td>
	<td><img src="$imag_11"></td>
	<td><img src="$imag_12"></td>
</tr>
<tr>
	<td><img src="$imag_13"></td>
	<td><img src="$imag_14"></td>
	<td><img src="$imag_15"></td>
	<td><img src="$imag_16"></td>
</tr>
</table>
<div class="separate"></div>
	<table cellpadding="5" class="precio">
		<tr style="vertical-align:middle">
			<td width='100px'>Importe final</td><td width='100px' align="right">$regular_price €</td>
		</tr>
			
	 </table>

<p style="font-size:10px;color:#414141">Esta oferta de venta tiene una validez de 30 días salvo su venta o reserva, en dicho supuesto quedará sin efecto. Puede visitar nuestra página de
Facebook en https://www.facebook.com/perezrumbaocar y nuestro perfil de Twitter en https://twitter.com/perezrumbaocar. Le informamos de que
www.percar.com es un nombre de dominio en Internet, que contiene un sitio web titularidad corresponde a Pérez Rumbao S.A.U. Puede
ponerse en contacto con la empresa responsable de este sitio web en info@percar.com.
</p>
<div style="font-size:10px;color:#414141">© 2018 percar.com</div>

EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('percar-'.$marca.'-'.$modelo.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
































// // set document information
// $pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('cardealer');
// // set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// // set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// // set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// // set some language-dependent strings (optional)
// // if (@file_exists(dirname(__FILE__).'cardealer-helper-library/pdf/examples/lang/eng.php')) {
// // require_once(dirname(__FILE__).'pdf/examples/lang/eng.php');
// // $pdf->setLanguageArray($l);
// // }
// if (@file_exists( $url_plugin.'pdf/examples/lang/eng.php')) {
// include( $url_plugin. 'pdf/examples/lang/eng.php');
// $pdf->setLanguageArray($l);
// }

// // ---------------------------------------------------------

// // set font
// $pdf->SetFont('helvetica', '', 10);

// // add a page
// $pdf->AddPage();

// /* NOTE:
//  * *********************************************************
//  * You can load external XHTML using :
//  *
//  * $html = file_get_contents('/path/to/your/file.html');
//  *
//  * External CSS files will be automatically loaded.
//  * Sometimes you need to fix the path of the external CSS.
//  * *********************************************************
//  */

// // define some HTML content with style
// $html_ul = "";
// if(isset($attribute['features_options']) && !empty($attribute['features_options'])){
//     $myArray = explode('-', $attribute['features_options']);
//     $counter=sizeof($myArray);
//     $html_ul= "<ul>";  $i="0"; 
//     foreach( $myArray as $feature):
//     if($i!=0){ 
//         $html_ul.= "<li>".$feature."</li>";
//        }
//        $i++;
//     endforeach; 
//     $html_ul.= "</ul>";     
// }
                           
// if( isset($car_images) && !empty($car_images) ){                    
//     foreach($car_images as $image ):
//         $carimage=$image['url'];
//         break;
//     endforeach;                                 
// }

// if( isset($car_price) && !empty($car_price) ){                   
//      $regprice=$car_price['regular_price'];
//      $saleprice=$car_price['sale_price'];
//      $symbol=$car_price['currency_symbol'];  
// }

// $fuel_image=plugin_dir_url('').'cardealer-helper-library/images/pdf/fuel.jpg';
// $city_image=plugin_dir_url('').'cardealer-helper-library/images/pdf/city.jpg';
// $hwy_image =plugin_dir_url('').'cardealer-helper-library/images/pdf/hwy.jpg';
// $newhtml = '--';


// // output the HTML content
// $pdf->writeHTML($newhtml, true, false, true, false, '');

// // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// // reset pointer to the last page
// $pdf->lastPage();

// // ---------------------------------------------------------
// // Close and output PDF document
// // This method has several options, check the source code documentation for more information.
// $pdf->Output($url_plugin."pdf/examples/uploads/".$file_name, 'F');


// // Need to require these files
// if ( !function_exists('media_handle_upload') ) {	
//     require_once( ABSPATH . 'wp-admin/includes/image.php' );
//     require_once( ABSPATH . 'wp-admin/includes/file.php' );
//     require_once( ABSPATH . 'wp-admin/includes/media.php' );
// }

// $url = CDHL_URL.'includes/pdf_generator/pdf/examples/uploads/'.$file_name;	
// //$url = $url_plugin.'pdf/examples/uploads/'.$file_name;	

// $tmp = download_url( $url );

// if( is_wp_error( $tmp ) ){
// 	// download failed, handle error
// }
// $post_id = $id;
// $file_array = array();

// // Set variables for storage
// // fix file filename for query strings
// preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png|pdf)/i', $url, $matches);
// $file_array['name'] = basename($matches[0]);
// $file_array['tmp_name'] = $tmp;

// // If error storing temporarily, unlink

// if ( is_wp_error( $tmp ) ) {
// 	if( file_exists($file_array['tmp_name']) ) {
// 		@unlink($file_array['tmp_name']);
// 	}
// 	$file_array['tmp_name'] = '';
// }
// // do the validation and storage stuff
// $iid = media_handle_sideload( $file_array, $post_id, $post_title );
// // If error storing permanently, unlink
// if ( is_wp_error($iid) ) {
// 	if( file_exists($file_array['tmp_name']) ) {
// 		@unlink($file_array['tmp_name']);
// 		return $iid;
// 	}
// }

// // change the file attachment for pdf brochure
// update_post_meta( $post_id, 'pdf_file', $iid);
// $src = wp_get_attachment_url( $iid );

// // unlink pdf file generated inside CDHL plugin
// if( isset( $src ) ){
// 	if( file_exists( $url_plugin."pdf/examples/uploads/".$file_name ) ) { 
// 		@unlink( $url_plugin."pdf/examples/uploads/".$file_name );
// 	}
// }
// echo $src;
// die;
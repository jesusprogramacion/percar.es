<?php

/**
 * Template Name: Ocasión - detalles de vehículo
 *
 * @package WordPress
 * @subpackage Total
 * @since 1.0
 */


/// Set the slider



get_header('ocasion');

?>


    <div class="wrapper-template">
        <div>

                <?php include_once('partials-ocasion/vehicle-details.php'); ?>

        </div>

    </div>

<?php get_footer(); ?>
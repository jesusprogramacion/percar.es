<?php
/**
 * Template Name: Noticias
 * Description: Formulario petición información de vehículo.
 *
 * @package Alola
 * @author Alola media
 */
// add_shortcode( 'cd_recent_posts', 'cdhl_recent_posts_shortcode' );
// function cdhl_recent_posts_shortcode($atts){
	global $car_dealer_options;
get_header();


    while ( have_posts() ) : the_post(); ?> 
        <div class="entry-content-page">
            <?php the_content(); ?> 
        </div>

    <?php
    endwhile; 
    wp_reset_query(); 
    ?>

<div id="noticias-page" class="filter-grid-cars">
    <div class="container">
        <div class="row">

<?php	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	// if ( get_query_var('paged') ) $paged = get_query_var('paged');
	// if ( get_query_var('page') ) $paged = get_query_var('page');
	$args = array(
		'post_type'          => 'post',
		'post_status'        => array('publish'),
		//'posts_per_page'     => $no_of_posts,
		'paged' 			 => $paged,

		'posts_per_page' 	 => 9
	);
	
	$post_meta_info = new WP_Query( $args );	
	?>
	
	<div class="noticias page-section-pb">
		<div class="row">
			<?php
			while($post_meta_info->have_posts()){
				$post_meta_info->the_post();
				$excerpt = get_the_excerpt();
				$excerpt = cdhl_shortenString($excerpt,120, false, true);

					$col_class = "col-lg-4 col-md-6 col-sm-12 item-noticias";
					$class='';
                    if(!has_post_thumbnail()){
                        $class = "blog-no-image";
					}
                    ?>
                    <div class="<?php echo esc_attr($col_class);?>">
                        <div class="blog-2 <?php echo esc_attr($class);?>">
                         	<a href="<?php the_permalink();?>"class="blog-image">
                            <?php the_post_thumbnail('medium_large',array('class'=>'img-responsive')); ?>
                            
                          	</a>
                          <div class="blog-content">
  
                            <div class="blog-description blog-description-slide">
                            	<div class="blog-categories">
									<ul class="post-categories">
			                            <?php 
										foreach((get_the_category()) as $category) { 
										    echo '<li class="'.$category->slug.'"><a href="'.get_site_url().'/categoria/'.$category->slug.'">'.$category->cat_name .'</a></li>';
										} //the_category();
                            			?>
		                            </ul>
		                        </div>
                                <a href="<?php the_permalink();?>" class="blog-title-slide"><?php the_title();?></a>
                                <?php if(is_page('noticias')) {?>
									<p><?php echo $excerpt;?></p> 
                                <?php } ?>
                                <a href="<?php the_permalink();?>" class="blog-link-slide">Leer más ></a>
                                
 
                            </div>
                          </div>
                        </div>
                     </div>
					<?php
			
			}

			?>
		<?php //wp_reset_query(); 

		$big = 999999999; // need an unlikely integer
		$translated = __( 'Page', 'mytextdomain' ); // Supply translatable string
	echo "<div class='pagination-pr'>";
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $post_meta_info->max_num_pages,
			'prev_text' => '«',
  			'next_text' => '»'
		    // 'before_page_number' => '<span class="screen-reader-text"> << </span>'
		) );
		echo '</div>';
		?>

</div>

		</div>
	</div>
	<?php
	/* Restore original Post Data */
	wp_reset_postdata();
		//return ob_get_clean();
// }
// function cdhl_shortcode_excerpt_more($more){
// 	return '';
// }
// function cdhl_shortcode_excerpt_length(){
// 	return '35';
// }
?>
</div>
</div>
</div>
<?php get_footer();?>


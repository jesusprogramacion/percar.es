<?php
/**
 * Template Name: Coches likes usuarios
 * Description: Formulario petición información de vehículo.
 *
 * @package Alola
 * @author Alola media
 */
get_header('ocasion');
  

    ?>

<div class="filter-single-cars filter-grid-cars filter-grid-cars-fav">
    <div class="container">
        <div class="row">
                <div class="col-sm-12">
                   <?php echo do_shortcode('[formularios-busqueda]');?>
                </div>
        </div>
    </div>
</div>
<div class="vehiculos-usuario-likes">
    <div class="site-content container" id="primary">
    	<div role="main" id="content" style="min-height: 500px">
    		<div class="container">
    		<div class="row all-cars-list-arch">
    			<div class="section-title text-center">
           			<h2>Tus vehículos favoritos</h2>
        		</div>

			<?php  
			global $wp_ulike_class,$ultimatemember;
			
			$args = array(
				"user_id" 	=> um_profile_id(),			//User ID
				"col" 		=> 'post_id',				//Table Column (post_id,comment_id,activity_id,topic_id)
				"table" 	=> 'ulike',					//Table Name
				"limit" 	=> 100,						//limit Number
			);	
			
			$user_logs = $wp_ulike_class->get_current_user_likes($args);

			$ids= array();
			foreach ($user_logs as $user_log) {
				array_push($ids, $user_log->post_id);					
			}
			$km0 = false;
 		if(sizeof($ids) > 0){

				$the_query= new WP_Query(array(
					'post_status' => 'published',
					'post_type' => 'cars',
					'orderby' => 'meta_value_num',
					'meta_key' => '_liked',
					'post__in'=> $ids,
					// 'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1
				));

				if ( $the_query->have_posts() ) {

					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$id = get_the_ID();
						?>
						<div class="col-md-3 col-sm-6 col-12">
						    <div class="car-item gray-bg text-center ">        
						        <div class="car-image">            
						            <?php                        
						        

						            //echo cardealer_get_cars_condition($id);
						            
						            echo cardealer_get_cars_image('car_catalog_image',$id);			
						            ?>            
						            <div class="car-overlay-banner">
						                <ul>
						                    <?php
						                    do_action( 'car_overlay_banner', $id  ); 
						                    ?>
						                </ul>
						            </div><?php			            
						            cardealer_get_cars_list_attribute($id);

									do_action( 'car_overlay_banner_mobile', $id  ); ?>

						        </div>                                               
						        <div class="car-content">         
						            <?php
						            do_action( 'cardealer_list_car_title' );
						            echo cardealer_get_cars_status($id);
						            cardealer_car_slide_price_html($id);
						            cardealer_get_vehicle_review_stamps($id);
						            ?>                            
						        </div>
						    </div>
						</div>
					<?php }
					$km0 = true;
						wp_reset_postdata();
						// if( $the_query->have_posts() ){
			   //              get_template_part('template-parts/cars/pagination');
			   //          }
				}
			}
			// else{
			// 	echo '<div class="all-cars-list-arch"><div class="col-sm-12"><div class="alert alert-warning">Todavía no has añadido ningún vehículo a favoritos.</div></<div></<div>';
			// }

			global $wpdb;

			$user_id =  get_current_user_id(); 
			$resultados = $wpdb->get_results( "SELECT subempresa,post_id,status FROM r5awp_ulike WHERE user_id = $user_id" );
			$likes_ocasion = '';
			$subempresas_ocasion = '';
			foreach($resultados as $res){
				$id_ocasion = strpbrk($res->post_id, '-');
				
				if($id_ocasion!=false && $res->status == 'like') {
					// $id_Sub= '-'.$id_ocasion.'_'.$res->subempresa;
					// $likes_ocasion .= $id_Sub;
					$likes_ocasion .= $id_ocasion; 
                    $subempresas_ocasion .= '_'.$res->subempresa;
				}

			}
				
			?>

			<div ng-controller="ocasionLikes" data-ng-init="listLikes(<?php echo htmlspecialchars(json_encode($likes_ocasion)); ?>,<?php echo htmlspecialchars(json_encode($subempresas_ocasion)); ?>, <?php echo htmlspecialchars(json_encode($user_id)); ?>)">
				<div class="product-listing">

					<?php 
					if($km0 == false && $likes_ocasion == ''){
						echo '<div class="alert alert-warning">No hay ningún vehículo añadido a favoritos.</div>';
					}?>
			
			<div ng-bind-html="likesContent | to_trusted">
				
			</div>
<!-- 					<div class="all-cars-list-arch vehiculos-ocasion-lista">
					    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" ng-repeat="vehicle in vehiclesLikes">
					        
					        <div class="car-item gray-bg text-center last" style="height:404px">
		                        <div class="car-image">

		                            <img class="img-responsive" src="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg" alt="" height="190">

		                            <div class="car-overlay-banner">
		                                <ul>
		                                 <li><a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" data-toggle="tooltip" title="" tooltip="Ver" tooltip-placement="top" class="psimages" data-original-title="Ver"><i class="fa fa-link"></i></a></li>
		                                    <li class="pssrcset"><a href="javascript:void(0)" data-toggle="tooltip" tooltip="Galería" tooltip-placement="top" data-original-title="Galería" title="" class="psimages" data-image="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg"><i class="fa fa-expand"></i></a></li>
		                                    <li>
		                                    	<?php 
                                //$slug = "{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}";
                               // $id_vehiculo = '{{vehicle.idVOPR}}';

                                //var_dump($id_vehiculo);
  //                               $args =	$defaults      = array(
		// 	"id"            => $id_vehiculo,				//Post ID
		// 	// "user_id"       => $return_userID,			//User ID (if the user is guest, we save ip as user_id with "ip2long" function)
		// 	// "user_ip"       => $wp_user_IP,				//User IP
		// 	// "get_like"      => $get_like,				//Number Of Likes
		// 	// "method"        => 'likeThis',				//JavaScript method
		// 	// "setting"       => 'wp_ulike_posts',		//Setting Key
		// 	// "type"          => 'post',					//Function type (post/process)
		// 	// "table"         => 'ulike',					//posts table
		// 	// "column"        => 'post_id',				//ulike table column name
		// 	// "key"           => '_liked',				//meta key
		// 	// "cookie"        => 'liked-',				//Cookie Name
		// 	"slug"          => $slug,					//Slug Name

		// );
 	// 						// if(function_exists('wp_ulike')) wp_ulike('get', $args);
    //                                 $id_vehiculo = file_get_contents("php://input");
    // $id_vehiculo = json_encode($id_vehiculo);id_vehiculo
								//$id_vehiculo=json_decode(file_get_contents('php://input')); s//get user from 


// $id_vehiculo = file_get_contents("php://input");
// $request = json_decode($id_vehiculo);
// $id = $request->vehicle->idVOPR;

                            //  echo do_shortcode('[wp_ulike_ocasion id="-{{vehicle.idVOPR}}"]'); ?>

		                                    </li>
		                                    <?php
		                                   // do_action( 'car_overlay_banner', $id  ); 
		                                    ?>
		                                </ul>
		                            </div>

		                        </div>
				                <div class="car-content"> 
				                    <div class="text-slider">
				                            <a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}"><span class="info-marca">{{ vehicle.marca }} </span><span class="info-modelo">{{ vehicle.gama | removeAccents }} </span><br><div class="info-caracteristicas"><span>{{ vehicle.version }} </span></div></a>


				                        <div class="price car-price">
				                            <div ng-if="vehicle.estado == 'VENDIDO'"><span class="label car-status sold"></span></div>
				                            <div class="vendido-slide"  ng-if="vehicle.estado == 'VENDIDO'">Vendido</div>
				                            <div class="reservado-slide"  ng-if="vehicle.estado == 'RESERVADO'">Reservado</div>
				                            <div class="oferta-slide"  ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'">Oferta</div>
				                            <span class="new-price"> {{ vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' }} €</span>
				                            <a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" class="car-slide-ver-coche">Ver coche</a>

				                        </div>
				                    </div>          
				                </div>
		            		</div>
					    </div>
					</div> -->






				</div>
			</div>

	    </div>
    		
    	</div>
    	</div>
    </div>
</div>

<?php get_footer();?>



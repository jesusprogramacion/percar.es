<?php
/**
 * Template Name: Ocasión - Formulario Interesados
 * Description: Formulario petición información de vehículo.
 *
 * @package Alola
 * @author Alola media
 */
get_header('ocasion');
   
   // get_template_part('template-parts/content','intro');    ng-controller="vehiclesListSliderCtrl"
    ?>
<div class="vehiculo-details-single car-details-ocasion" >
    <div class="site-content container" id="primary"  >
    	<div role="main" id="content" ng-controller="vehiclesDetailsController">
    		<div class="container">
    			<div class="row">
	    			<div class="col-md-1 col-sm-12 btn-back"><a href="https://percar.aloladev.com/" onclick="location.href = document.referrer; return false;"><i class="fa fa-angle-left fa-xs"></i> atrás</a></div>
	    			<div class="col-md-10 col-sm-12 p-left vehiculo-info-details">
				       
									<div class="col-sm-8 p-left">
										<div class="col-sm-4 p-left">
											 <img src="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg"/>
										</div>
										<div class="col-sm-8 inner-info">
											<span class='info-marca'>{{vehicle.marca}} {{vehicle.gama}}</span>
			                                
			                                <div class="info-caracteristicas">{{vehicle.version}}</div>
										</div>
									</div>
									<div class="col-sm-4 inner-info">
										<div class="price car-price text-right">
										   <span class="new-price" ng-bind="(vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' ) + ' €'"></span>
										</div>
									    <?php  //cardealer_car_price_html('text-right'); ?>
									</div>
					
	    			</div>
					<div class="col-md-1 col-sm-12"></div>
    			</div>
    		</div>
    		<?php 
    			$vehiculo = $_GET['v-link'];
    			$vehiculo_id = $_GET['id'];

    			$v = explode('?', $vehiculo);
    			$ruta_imagen = explode('-', $v[1]);

  
    			$imagen = "http://www.sibuscascoche.com/img/coches/".$ruta_imagen[0]."/".$ruta_imagen[1]."/".$vehiculo_id."/00.jpg";
				$user = wp_get_current_user();
				echo '<span class="info-usuario" data-nombre="'.$user->first_name.'" data-email="'.$user->user_email.'" data-telefono="'.$user->phone_number.'" data-imagen="'.$imagen.'" ></span>';
			
			?>
			<div class="col-md-3"></div>
	    		<div class="col-md-6">

	    			<h3>Estoy interesado en:</h3>
				    <?php
				   	while ( have_posts() ) : the_post();            
				       get_template_part('template-parts/promocode/content','promocode');
				    endwhile; // end of the loop. ?>
				</div>
			<div class="col-md-3"></div>
    	</div>
    </div>
</div>

<?php get_footer();?>

<?php
/**
 * Template Name: Formulario Interesados
 * Description: Formulario petición información de vehículo.
 *
 * @package Alola
 * @author Alola media
 */
get_header();
   
    get_template_part('template-parts/content','intro');    
    ?>
<div class="vehiculo-details-single">
    <div class="site-content container" id="primary">
    	<div role="main" id="content">
    		<div class="container">
    			<div class="row">
	    			<div class="col-md-1 col-sm-12 btn-back"><a href="https://percar.aloladev.com/" onclick="location.href = document.referrer; return false;"><i class="fa fa-angle-left fa-xs"></i> atrás</a></div>
	    			<div class="col-md-10 col-sm-12 p-left vehiculo-info-details">
				        <?php 
				        $id = get_the_ID();
							$args = array(
						        'post_type' => 'cars',
						        'p' =>  $_GET["id"],   // id of the post you want to query
						    );
						    $vehiculo = new WP_Query($args);  
						   if($vehiculo->have_posts()) : 

						        while ( $vehiculo->have_posts() ) : $vehiculo->the_post(); ?>
									<div class="col-sm-8 p-left">
										<div class="col-sm-4 p-left">
											 <?php //the_post_thumbnail(); 
												echo cardealer_get_cars_image('car_catalog_image',$id);	
											 ?>


										</div>
										<div class="col-sm-8 inner-info">

									<?php
                                        $make = get_the_terms($post->ID,'car_make');
                                        $model = get_the_terms($post->ID,'car_model');
                                        $motorizacion = get_the_terms($post->ID,'car_engine');
                                        $acabado = get_the_terms($post->ID,'car_body_style');

	                                    $titulo = explode(" ", get_the_title());
	                                    echo "<span class='info-marca'>".$make[0]->name."</span>";
	                                    echo "<span class='info-modelo'>".$model[0]->name."</span>";
	                                    echo '<div class="info-caracteristicas">'.$motorizacion[0]->name.' '.$acabado[0]->name.'</div>';
	                                 ?>
										</div>
									</div>
									<div class="col-sm-4 inner-info">
									    <?php  cardealer_car_price_html('text-right'); ?>
									</div>
						           <?php //get_template_part( 'template-parts/content', 'post' ); //Your Post Content comes here
						        endwhile;
							endif;
						 ?>
	    			</div>
					<div class="col-md-1 col-sm-12"></div>
    			</div>
    		</div>
    		<?php 
				$images = get_field('car_images', $id);
				$img = esc_url($images[0]['sizes']['thumbnail']);

				$imagen = str_replace('-150x150', '', $img);
	
				$user = wp_get_current_user();
				echo '<span class="info-usuario" data-nombre="'.$user->first_name.'" data-email="'.$user->user_email.'" data-telefono="'.$user->phone_number.'" data-imagen="'.$imagen.'" ></span>';
			
			?>
			<div class="col-md-3"></div>
	    		<div class="col-md-6">
	    			<h3>Estoy interesado en:</h3>
				    <?php
				    while ( have_posts() ) : the_post();            
				        get_template_part('template-parts/promocode/content','promocode');
				    endwhile; // end of the loop. ?>
				</div>
			<div class="col-md-3"></div>
    	</div>
    </div>
</div>

<?php get_footer();?>
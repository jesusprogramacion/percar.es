<div class="filter-single-cars-ocasion filter-grid-cars-ocasion">
    <div class="container">
        <div class="row">
                <div class="clearfix"></div>
                <div class="section-title text-center">
                    <span></span>
                    <?php if(is_page('detalles')){ echo '<h2>¿Deseas hacer otra búsqueda en <span>Ocasión</span>?</h2>';}else{echo '<h2>¿Qué tipo de coche buscas?</h2>';}?>
                    
                 </div>
                <div ng-controller="searcherCtrl">
                       
                        <form id="searcher-form" class="form-horizontal search-block" role="form" action="<?php echo WP_HOME ?>/vehiculos-de-ocasion/">
                        <div class="row">
                            <div class="col-lg-8 col-xs-12 col-md-12">
                                <div class="wrapper-select col-lg-4 col-md-4 col-sm-6 col-12">
                                            <select class="form-control selectpicker custom-filters-box" id="brand-select" name="brand-select" ng-model="selectedBrand"  ng-options="brand.marca for brand in brands track by brand.marca" ng-change='onBrandchange(selectedBrand)' ng-init="selectedBrand=<?php echo htmlspecialchars(json_encode($_GET['brand-select'])); ?>">
                                                <option value="">Selecciona Marca</option>

                                            </select>
                                  
                                </div>
                                <div class="wrapper-select col-lg-4 col-md-4 col-sm-6 col-12">
                                    
                                            <select class="form-control selectpicker custom-filters-box-ocasion" id="model-select" name="model-select" ng-model="selectedModel" <?php if($_GET['brand-select']=='') echo 'ng-disabled="!models"'; ?> ng-options="model.gama for model in models track by model.gama" ng-change='onModelchange(selectedModel, {{ selectedBrand }} )'  ng-init="onModelinit(<?php echo htmlspecialchars(json_encode($_GET['brand-select'])); ?>,<?php echo htmlspecialchars(json_encode($_GET['model-select'])); ?>,selectedModel); selectedModel = <?php echo htmlspecialchars(json_encode($_GET['model-select'])); ?>">
                                                <option value="">Selecciona Modelo</option>
                                            </select>
                                            
                                </div>
                                 <div class="wrapper-select col-lg-4 col-md-4 col-sm-6 col-12">
                                  
                                       
                                            <select class="form-control selectpicker custom-filters-box-ocasion" id="fuel-select" name="fuel-select" ng-init="onFuelchange(<?php echo htmlspecialchars(json_encode($_GET['fuel-select']));?>)">
                                                <option value="">Selecciona Combustible</option>
                                                <option ng-repeat="fuel in ['Gasolina', 'Diésel']" ng-selected="{{fuel == selectFuel}}" ng-change='onFuelchange(selectedFuel)' ng-model="selectedFuel"  value="{{fuel}}">{{fuel}}</option>
                                            </select>
                                  
                                  
                                </div>
                                <div class="wrapper-select col-lg-4 col-md-4 col-sm-6 col-12">
                                       
                                            <select class="form-control selectpicker custom-filters-box-ocasion" id="change-select" name="change-select" ng-init="onCambiochange(<?php echo htmlspecialchars(json_encode($_GET['change-select']));?>)">
                                                <option value="">Selecciona Cambio</option>
                                                <option ng-repeat="cambio in ['Manual', 'Automático']" ng-selected="{{cambio == selectCambio}}" ng-change='onCambiochange(selectedCambio)' ng-model="selectedCambio"  value="{{cambio}}">{{cambio}}</option>
                                            </select>
                                  
                                  
                                </div>
                                <div class="both col-lg-8 col-md-8 col-sm-12">
                                    <div class="price_slider_wrapper">                       
                                        <div class="price-slide">
                                            <div class="price">
                            
                                                <input id="pgs_min_price" name="min_price" value="0" data-min="0" type="hidden">
                                                <input id="pgs_max_price" name="max_price" value="130000" data-max="130000" type="hidden">

                                                 <input id="pgs_min_price_o" name="sincePrice-select" value="<?php echo ($_GET['untilPrice-select']!='') ? $_GET['untilPrice-select'] : '130000'; ?>" data-min="0" type="hidden">
                                                <input id="pgs_max_price_o" name="untilPrice-select" value="<?php echo ($_GET['untilPrice-select']!='') ? $_GET['untilPrice-select'] : '130000'; ?>" data-max="130000" type="hidden">
                                            
                                                <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>

                                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>
                                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span>
                                                </div>
                                            <label for="dealer-slider-amount">Quiero invertir entre <input id="dealer-slider-amount" readonly="" class="amount" value="" type="text"></label>
                                        </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                        <div class="wrapper-button col-lg-4 col-xs-12 col-md-12">
                            <input class="btn btn-md btn-primary btn-search col-xs-12"  type="submit" value="Ver coches">
<!--                             <input class="btn btn-md btn-primary btn-search col-xs-12" ng-click="verCoches($event)" type="submit" value="Ver coches">
 -->                        </div>
                        </div>

                        </form>
                    <!--     <span data-ng-init="formInit(<?php //echo htmlspecialchars(json_encode($_GET['brand-select'])); ?>)"></span> -->
                </div>
        </div>
    </div>
</div>

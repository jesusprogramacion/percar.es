
    <div class="owl-carousel owl-theme owl-loaded" data-autoplay="true" data-nav-arrow="1" data-nav-dots="true" data-items="4" data-md-items="3" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="20" data-loop="true">                                           
        <div class="owl-stage-outer" ng-repeat="vehicle in vehiclesRelated | limitTo:16">
            <div class="owl-stage" >
                <div class="owl-item" style="width: 270px; margin-right: 20px;" >
                    <div class="car-item gray-bg text-center" style="min-height: 414px;" >
                        <div class="car-image">                                      
                                            
                            <img class="img-responsive" src="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg" alt="" width="265" height="190">                                
                                            <div class="car-overlay-banner">
                                                <ul> 
                                                    <li><a href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" data-original-title="Ver" data-toggle="tooltip" tooltip="Ver" tooltip-placement="top" ><i class="fa fa-link"></i></a></li>
                                                    <li class="pssrcset"><a href="javascript:void(0)" data-toggle="tooltip" tooltip="Galería" tooltip-placement="top" class="psimages" data-image="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg"><i class="fa fa-expand"></i></a></li>   
                                                    <li>
                                       <?php echo do_shortcode('[wp_ulike_ocasion id="-{{vehicle.idVOPR}}"]'); ?>

                                    </li>                                     
                                                </ul>
                                            </div>
                        </div>                                                        
                        <div class="car-content">                                    
                            <a href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}">
                                <span class="info-marca">{{ vehicle.marca | lowercase }} {{ vehicle.gama | lowercase }}</span>
                              
                                <div class="info-caracteristicas"> {{ vehicle.version }}</div>
                            </a>
                            <div class="price car-price ">
                            <div ng-if="vehicle.estado == 'VENDIDO'"><span class="label car-status sold">-</span></div>
                            <div class="vendido-slide"  ng-if="vehicle.estado == 'VENDIDO'">Oferta</div>
                            <div class="reservado-slide"  ng-if="vehicle.estado == 'RESERVADO'">Reservado</div>
                            <div class="oferta-slide"  ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'">Oferta</div>
                                <span class="old-price" ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'"> {{ vehicle.pvpref | number: 0 | replace: ',': '.' }} € </span>
                                <span class="new-price">{{ vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' }} €</span>
                                <a href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" class="car-slide-ver-coche">Ver coche</a>
                            </div>
                        </div>                            
                    </div> 
                </div>
            </div>
        </div>  
    </div>                              



<div ng-controller="vehiclesDetailsController">
<div class="ficha-head" >
    <div class="container">

        <div class="row">
        </div>

        <div class="row  car-details-ocasion car-detail-galeria">
            <div class="col-lg-12 col-md-12">
                <div class="col-lg-12 col-md-12">
                    <div class="col-lg-6 col-md-6 car-detail-galeria-arrow-left">

                            <a rel="prev" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles?{{ prevVehicle.marca | lowercase }}-{{ prevVehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ prevVehicle.version | replaceSpaces: '_'  | lowercase }}-{{ prevVehicle.empresa | lowercase }}-{{ prevVehicle.idVOPR }}&sub={{ prevVehicle.subempresa }}" title="{{ prevVehicle.marca | lowercase }} {{ prevVehicle.gama }} {{ prevVehicle.version | lowercase }}" class="port-arrow port-arrow-car-left tooltip-content-1" data-original-title="{{ prevVehicle.marca | lowercase }} {{ prevVehicle.gama }} {{ prevVehicle.version }}" data-toggle="tooltip" data-placement="right"><i class="fa fa-angle-left"></i></a>
                    </div>
                    <div class="col-lg-6 col-md-6  car-detail-galeria-arrow-right">

                           <a rel="next" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles?{{ nextVehicle.marca | lowercase }}-{{ nextVehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ nextVehicle.version | replaceSpaces: '_'  | lowercase }}-{{ nextVehicle.empresa | lowercase }}-{{ nextVehicle.idVOPR }}&sub={{ prevVehicle.subempresa }}" title="{{ nextVehicle.marca | lowercase }} {{ nextVehicle.gama }} {{ nextVehicle.version  | lowercase }}" class="port-arrow port-arrow-car-right tooltip-content-1" data-original-title="{{ nextVehicle.marca | lowercase }} {{ nextVehicle.gama  | replaceSpaces: '_' }} {{ nextVehicle.version | replaceSpaces: '_'  | lowercase }}" data-toggle="tooltip" data-placement="left"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-0"></div>

                <div class="col-lg-8 col-md-8 col-sm-12 car-detail-galeria-inner">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <h3>

                                    <span>{{vehicle.marca | lowercase }}</span> {{ vehicle.gama | lowercase }}
                                    <div class="info-caracteristicas">{{vehicle.version}}</div>
                                 </h3>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6"> 
                                <div class="price car-price text-right">   
                                    <div class="vendido-single"  ng-if="vehicle.estado == 'VENDIDO'">Oferta</div>
                                    <div class="reservado-single"  ng-if="vehicle.estado == 'RESERVADO'">Reservado</div>
                                    <div class="oferta-single"  ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'">Oferta</div>
                                    <span class="old-price" ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'"> {{ vehicle.pvpref | number: 0 | replace: ',': '.' }} € </span>
                                  
                                    <span class="new-price" ng-bind="(vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' ) + ' €'"></span>
                                </div>
                            </div> 
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 galeria-vehiculo-details">


<!--             <div id="tabs">
                        <ul class="tabs tabs-view-detalles">
                        
                            
                            <li data-tabs="galeria" class="active">Galería</li>
                            <li data-tabs="exterior">Exterior</li>    

                            <li data-tabs="interior">Interior</li>  
                        </ul>

                        <div id="galeria" class="tabcontent">        
 -->

        <div class="galeria-over-ocasion pssrcset"><a href="javascript:void(0)" class="psimages" data-image="{{ fotosGaleria }}"></a></div>
        <slick class="slide-1" slides-to-show=1 slides-to-scroll=1 init-onload=true data="photoVehicleList" autoplay=false arrows=false as-nav-for=".slide-2">



                                        <div class="wrapper-slider-img" ng-repeat="photo in photoVehicleList">
                                            <div class="main-slider-img" custom-background-image="https://www.sibuscascoche.com/img/coches/{{ photo }}"></div>
                                        </div>
                                    </slick>
                                  
                                    <slick class="slide-2" slides-to-show=5 slides-to-scroll=1 init-onload=true data="photoVehicleList" responsive="responsive" arrows=true focus-on-select=true as-nav-for=".slide-1" next-arrow=".next-arrow" prev-arrow=".prev-arrow">
                                        <div class="wrapper-slider-img" ng-repeat="photo in photoVehicleList">
                                            <div class="carousel-img" custom-background-image="https://www.sibuscascoche.com/img/coches/{{ photo }}"></div>
                                        </div>
                                    </slick>
                                    <div class="wrapper-slider-arrows">
                                        <a href="" class="prev-arrow slick-prev">
                                        </a>
                                        <a href="" class="next-arrow slick-next">
                                        </a>
                                    </div>

                    <!--     </div> -->
<!--                         <div id="exterior" class="tabcontent">
                              <div class="threesixty-wrap">
                                <div class="threesixty car">
                                    <div class="spinner">
                                        <span>0%</span>
                                    </div>
                                    <ol class="threesixty_images"></ol>
                                </div>

                            </div>

                                <div class="pag">
                                    <a class="btn btn-danger custom_previous"><i class="fa fa-backward"></i></a>
                                    <a class="btn btn-inverse custom_play"><i class="fa fa-play"></i></a>
                                    <a class="btn btn-inverse custom_stop"><i class="fa fa-pause"></i></a>
                                    <a class="btn btn-danger custom_next"> <i class="fa fa-forward"></i></a>
                                </div>
                        </div> -->
<!--                         <div id="interior" class="tabcontent">
                       



                            <script>
                                /* If you want to stop starting auto scroll panorama  just change 'false' value below. */
                                var autoScrol = true;
                             </script>

                            <div class="panorama-body">
                               
                                <img id="panoramaImage" src="https://percar.aloladev.com/wp-content/themes/percar-child/images/interior/simulacion.jpg"> 

                                <div id="panorama">
                                    <span id="fullScreenMode" >
                                        <span id="fullscreenPan"><img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/fullscreen.png"></span>
                                    </span>
                                </div>

                         
                            </div>
                            <div class="keys">
                                <div class="control-key">
                                     <img class="control_keys" id="left_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/left_button.PNG">
                                    <img class="control_keys" id="right_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/right_button.PNG">
                                    <img class="control_keys" id="up_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/up_button.PNG">
                                    <img class="control_keys" id="down_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/down_button.PNG">
                                    <img class="control_keys" id="elipse_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/elipse.PNG">
                                </div>
                            </div> 
                              <div class="zoom_conrol">
                                    <span id="zoom_in"><img class="zoom" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/zoom-in.png"></span>
                                    <span id="zoom_out"><img class="zoom" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/zoom-out.png"></span>
                                </div>
 
                    </div> -->

<!-- 
            </div> -->

                    </div>                           
                </div>


                <div class="clearfix"></div>   
                <div class="details-nav">
                </div>         

            </div>
        </div>
    </div>
</div>
</div>
<?php 
    $url = array();
    $segments = array();
    $url = explode ('&', $_SERVER['QUERY_STRING']);
    $segments = explode ('-', $url[0]);
    $marca = $segments[count($segments)-5];
    $modelo = $segments[count($segments)-4];
    $version = $segments[count($segments)-3];
    $id_vehiculo =  $segments[count($segments)-1];
    $subempresa = $_GET['sub'];

    $related = $marca.'-'.$modelo;
?>
<div class="car-detail-post-option-share">
    <div class="container">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="car-detail-post-option">
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>/vehiculos-de-ocasion/informacion-vehiculo?&id=<?php echo $id_vehiculo;?>&v=<?php echo ucfirst(strtolower($marca)).'_'.ucfirst(strtolower($modelo)).'_'.ucfirst(strtolower($version)); ?>&v-link=<?php echo $_SERVER['REQUEST_URI'];?>" class="btn-me-interesa">Me interesa</a></li>

                            <li><?php 

                                echo do_shortcode('[wp_ulike_ocasion subempresa="'.$subempresa.'" id="-'.$id_vehiculo.'"]'); ?></li>
                                
                       <!--      <li><span onclick="window.open('http://www.sibuscascoche.com/pdf/<?php //echo $_SERVER['QUERY_STRING']; ?>');" class="btn-detalles-print"><i class="fa fa-print fa-sm"></i></span></li> -->
                        </ul>
                        <?php //get_template_part('template-parts/cars/single-car/share');?>
                        <div class="clearfix"></div>
                    </div>            
                </div>
                <div class="clearfix"></div>
        </div>
    </div>
</div>



<div class="ficha-metas"  ng-controller="vehiclesDetailsController">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="car-details-sidebar">
                    <div class="details-block details-weight">
                        <div class='col-md-3 col-sm-6'>
                            <div class="attr-single-vehiculo"><i class="ico-motorizacion"></i>
                                <div class="attr-value">
                                    <span>Motorización</span>
                                    <strong class="text-right" ng-if='vehicle.cilindrada'>{{vehicle.potencia/1.36 | number: 0}} kW ({{vehicle.potencia | number: 0}} CV)</strong>
                                    <strong class="text-right" ng-if='!vehicle.cambio'>--</strong>
                                </div>
                            </div>
                            <div class="attr-single-vehiculo"><i class="ico-combustible"></i>
                                <div class="attr-value">
                                    <span>Combustible</span>
                                    <strong class="text-right" ng-if='vehicle.combustible'>{{vehicle.combustible}}</strong>
                                    <strong class="text-right" ng-if='!vehicle.cambio'>--</strong>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-3 col-sm-6'>
                            <div class="attr-single-vehiculo"><i class="ico-cambio"></i>
                                <div class="attr-value">
                                    <span>Cambio</span>
                                    <strong class="text-right" ng-if='vehicle.cambio'>{{vehicle.cambio}}</strong>
                                    <strong class="text-right" ng-if='!vehicle.cambio'>--</strong>
                                </div>
                            </div>
                            <div class="attr-single-vehiculo"><i class="ico-consumo-medio"></i>
                                <div class="attr-value">
                                    <span>Consumo medio</span>
                                    <strong class="text-right" ng-if='vehicle.consumo'>{{vehicle.consumo}}</strong>
                                    <strong class="text-right" ng-if='!vehicle.consumo'>--</strong>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-3 col-sm-6'>
                            <div class="attr-single-vehiculo"><i class="ico-puertas"></i>
                                <div class="attr-value">
                                    <span>Puertas</span>
                                    <strong class="text-right" ng-if='vehicle.puertas'>{{vehicle.puertas}}</strong>
                                    <strong class="text-right" ng-if='!vehicle.puertas'>--</strong>
                                </div>
                            </div>
                            <div class="attr-single-vehiculo"><i class="ico-color"></i>
                                <div class="attr-value">
                                    <span>Color</span>
                                    <strong class="text-right" ng-if='vehicle.color'>{{vehicle.color}}</strong>
                                    <strong class="text-right" ng-if='!vehicle.color'>--</strong>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-3 col-sm-6'>
                            <div class="attr-single-vehiculo"><i class="ico-fecha-de-matriculacion"></i>
                                <div class="attr-value">
                                    <span>Fecha de matriculación</span>
                                    <strong class="text-right" ng-if='vehicle.fechaMatriculacion'>
                                    {{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'dd' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'MM' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'yyyy' }}
                                    </strong>
                                    <strong class="text-right" ng-if='!vehicle.fechaMatriculacion'>--</strong>
                                </div>
                            </div>
                             <div class="attr-single-vehiculo"><i class="ico-kilometraje"></i>
                                <div class="attr-value">
                                    <span>Kilometraje</span>
                                    <strong class="text-right" ng-if='vehicle.fechaMatriculacion'>
                                    {{ vehicle.km  | number: 0 | replace: ',' : '.' }} km
                                    </strong>
                                    <strong class="text-right" ng-if='!vehicle.fechaMatriculacion'>--</strong>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>              
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-ver-detalles" data-toggle="collapse" data-target="#demo">Ver detalles <i class="fa fa-angle-down"></i></button>

<div class="ficha-metas collapse" id="demo" ng-controller="vehiclesDetailsController">
    <div class="container">
        <div class="row">
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 ver-detalles-tabs">





<!-- Nav tabs -->
<ul class="nav nav-tabs tabs-ver-detalles" id="myTab" role="tablist">
  <li ng-if='vehicle.comentario' class="nav-item active">
    <a class="nav-link " id="descripcion-tab" data-toggle="tab" href="#descripcion" role="tab" aria-controls="descripcion" aria-selected="true"><i aria-hidden="true" class="fa fa-sliders"></i> Descripción</a>
  </li>
  <li ng-if='!vehicle.comentario' class="nav-item active">
    <a class="nav-link" id="equipacion-tab" data-toggle="tab" href="#equipacion" role="tab" aria-controls="equipacion" aria-selected="false"><i aria-hidden="true" class="fa fa-list"></i> Equipación</a>
  </li>
  <li ng-if='vehicle.comentario' class="nav-item">
    <a class="nav-link" id="equipacion-tab" data-toggle="tab" href="#equipacion" role="tab" aria-controls="equipacion" aria-selected="false"><i aria-hidden="true" class="fa fa-list"></i> Equipación</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="informacion-tab" data-toggle="tab" href="#informacion" role="tab" aria-controls="informacion" aria-selected="false"><i aria-hidden="true" class="fa fa-info-circle"></i> Información General</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#ubicacion" role="tab" aria-controls="settings" aria-selected="false"><i class="fa fa-map-marker" aria-hidden="true"></i> Ubicación</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div ng-if='vehicle.comentario' class="tab-pane active" id="descripcion" role="tabpanel" aria-labelledby="descripcion-tab">
        {{vehicle.comentario}}

  </div>
  <div ng-if='!vehicle.comentario' class="tab-pane active" id="equipacion" role="tabpanel" aria-labelledby="equipacion-tab">

     <p ng-repeat="equipment in vehicle.equipamiento | nl2br">{{ equipment }}</p>

  </div>

    <div ng-if='vehicle.comentario'class="tab-pane" id="equipacion" role="tabpanel" aria-labelledby="equipacion-tab">

     <p ng-repeat="equipment in vehicle.equipamiento | nl2br">{{ equipment }}</p>

  </div>
  <div class="tab-pane" id="informacion" role="tabpanel" aria-labelledby="informacion-tab">

        <div class="vehicle-data">

            <div ng-if='vehicle.combustible'>Combustible: <span>{{ vehicle.combustible | lowercase | ucfirst }}</span></div>
            <div>Fecha de Matriculación: <span>{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'dd' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'MM' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'yyyy' }}</span></div>
            <div>Kilometraje: <span>{{ vehicle.km  | lowercase | ucfirst }}</span></div>
            <div>Número de puertas: <span>{{ vehicle.puertas | lowercase | ucfirst }}</span></div>
            <div>Color: <span>{{ vehicle.color | lowercase | ucfirst }}</span></div>
            <div>Metalizado: <span>{{ vehicle.metalizado | lowercase | ucfirst }}</span></div>
            <div>Tipo de Carrocería: <span>{{ vehicle.tipoCarroceria | lowercase | ucfirst }}</span></div>
            <div>Cilindrada: <span>{{ vehicle.cilindrada | lowercase | ucfirst }}</span></div>
            <div>Garantía: <span>{{ vehicle.garantia | lowercase | ucfirst }}</span></div>
            <div class="specification-left-col">Número de Referencia: <span>{{ vehicle.idVOPR | lowercase }}</span></div>

        </div>

  </div>
  <div class="tab-pane" id="ubicacion" role="tabpanel" aria-labelledby="ubicacion-tab">
    <div class="acf-map-">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2954.847359446336!2d-8.704611584548232!3d42.217706879196676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd25882b185abd1f%3A0x217541b95a6fc3ec!2sPercar+%7C+Concesionario+Audi+y+Volkswagen!5e0!3m2!1ses!2ses!4v1519913721964" width="600" height="300" frameborder="0" style="border:0;width:100%" allowfullscreen></iframe>
    </div>
  </div>
</div>




                </div>

        </div>
    </div>
</div>
<?php 

// echo $related;


            // global $wp_ulike_class,$ultimatemember;
            
            // $args = array(
            //     "user_id"   => um_profile_id(),         //User ID
            //     "col"       => 'post_id',               //Table Column (post_id,comment_id,activity_id,topic_id)
            //     "table"     => 'ulike',                 //Table Name
            //     "limit"     => 100,                     //limit Number
            // );  
            
            // $user_logs = $wp_ulike_class->get_current_user_likes($args);

            // $ids= array();
            // foreach ($user_logs as $user_log) {
            //     array_push($ids, $user_log->post_id);                   
            // }


global $wpdb;

$user_id =  get_current_user_id();

$resultados= $wpdb->get_results( "SELECT subempresa, post_id,status FROM r5awp_ulike WHERE user_id = $user_id" );
$likes_ocasion = '';
$subempresas_ocasion = '';
foreach($resultados as $res){
    $id_ocasion = strpbrk($res->post_id, '-');
    if($id_ocasion!=false && $res->status == 'like'){
        $likes_ocasion .= $id_ocasion;
        $subempresas_ocasion .= '_'.$res->subempresa;
    }
}
?>

<div class="related-single-cars" data-ng-init="vehiclesRelated(<?php echo htmlspecialchars(json_encode($related)); ?>,<?php echo htmlspecialchars(json_encode($likes_ocasion)); ?>,<?php echo htmlspecialchars(json_encode($id_vehiculo)); ?>,<?php echo htmlspecialchars(json_encode($subempresas_ocasion)); ?>,<?php echo htmlspecialchars(json_encode($user_id)); ?>, <?php echo htmlspecialchars(json_encode($subempresa)); ?>)" ng-controller="vehiclesRelatedSliderCtrl">
<!-- 
<div class="col-md-12 product-listing product-listing-ocasion" ng-controller="vehiclesListController" data-ng-init="likesInit(<?php //echo htmlspecialchars(json_encode($likes_ocasion)); ?>)"> -->


    <div class="container">

        <div class="row car-details">
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <div class="feature-car">
                        <h3>Vehículos relacionados</h3>

                     <!--    <div id="listado-vehiculos-ocasion" ng-bind-html="listContent | to_trusted"  ></div> -->
                  <!--       <div id="listado-vehiculos-ocasion" owl-ready="ready($api)" ng-bind-html="owlItems | to_trusted"  >
                            <div class="all-cars-list-arch vehiculos-ocasion-lista owl-stage-outer" >
                                <div class="owl-stage"><div class="owl-item" style="width: 270px; margin-right: 20px;"><div class="car-item gray-bg text-center last"><div class="car-image"><img class="img-responsive" src="http://www.sibuscascoche.com/img/coches/VOLKSWAGEN/GOLF/2320/00.jpg" alt="" height="190"><div class="car-overlay-banner"><ul><li><a href="https://percar.aloladev.com//vehiculos-de-ocasion/listado-coches/detalles/?VOLKSWAGEN-GOLF-vi_2.0_tsi_210cv_gti-percar-2320" data-toggle="tooltip" title="" tooltip="Ver" tooltip-placement="top" class="psimages" data-original-title="Ver"><i class="fa fa-link"></i></a></li><li class="pssrcset"><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Galería" class="psimages" data-image=""><i class="fa fa-expand"></i></a></li><li><div id="wp-ulike-post--2320" class="wpulike wpulike-robeen "> <div class="wp_ulike_general_class wp_ulike_is_unliked"> <label> <input type="checkbox" data-ulike-id="-2320" data-ulike-nonce="0" data-ulike-type="likeThis" data-ulike-status="3" class="wp_ulike_btn wp_ulike_put_text"> 
                            </label> </div> </div></li></ul></div></div><div class="car-content"><div class="text-slider"><a href="https://percar.aloladev.com//vehiculos-de-ocasion/listado-coches/detalles/?VOLKSWAGEN-GOLF-vi_2.0_tsi_210cv_gti-PERCAR-2320"><span class="info-marca">volkswagen222</span><span class="info-modelo"> golf</span><br><div class="info-caracteristicas"><span>VI 2.0 TSI 210CV GTI</span></div></a><div class="price car-price"><div><span class="label car-status sold">--</span></div><div class="vendido-slide">Vendido</div><span class="new-price">14.990 €</span><a href="https://percar.aloladev.com//vehiculos-de-ocasion/listado-coches/detalles/?VOLKSWAGEN-GOLF-vi_2.0_tsi_210cv_gti-PERCAR-2320" class="car-slide-ver-coche">Ver coche</a></div></div></div></div></div></div>
                        </div> id=  "listado-vehiculos-ocasion" ng-bind-html="listContent | to_trusted"
                        </div> -->

<div class="pgs_cars_carousel-wrapper carousel-home">
        <div class="pgs_cars_carousel-items-ocasion" data-nav-arrow="true" data-nav-dots="true" data-items="4" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="25" data-autoplay="true" data-loop="true">
            
        </div>
</div>

        <!--                 <div  class="pgs_cars_carousel-wrapper ocasion-related-slide"  >
                            <div class="all-cars-list-arch vehiculos-ocasion-lista owl-stage-outer" >
                              <div class="owl-stage"></div> 
                            </div>
                        </div>  -->


<!-- 

        <div id="listado-vehiculos-ocasion" class="ocasion-related-slide owl-carousel owl-theme owl-loaded" data-autoplay="true" data-nav-arrow="1" data-nav-dots="true" data-items="4" data-md-items="3" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="20" data-loop="true">
                            <div class="all-cars-list-arch vehiculos-ocasion-lista owl-stage-outer">
                                <div class="owl-stage">
                                    <div class="owl-item"></div>
                                  

                            </div>
                        </div>
                    </div> -->


                   <?php //get_template_part('template-parts/cars/single-car/related');?>
                    <?php //include_once('vehicles-in-offer-slider.php'); ?>
                    </div>
                </div>
        </div>
    </div>
</div>
<div>
<?php 
include_once('vehicles-search.php'); 
//get_template_part('templates/partials-ocasion/vehicles-search');

?> 
</div>

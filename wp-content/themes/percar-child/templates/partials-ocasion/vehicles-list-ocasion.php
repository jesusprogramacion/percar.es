<?php 

    $v=file_get_contents('php://input');
    $vehicles = json_decode($v);
    $content='';
    $likesSub= array();
    $likes = explode('-', $vehicles->likes); // vehículos ocasión like
    
    $subempresas = explode('_', $vehicles->subempresas); 

    for($i = 0; $i <= count($likes); $i++){
        array_push($likesSub, $likes[$i].'_'.$subempresas[$i]);
    }

    $listado_vehiculos = array();

    foreach($vehicles->vehiclesL as $vehicle){
        $marca = str_replace(' ', '_', acentos($vehicle->marca));
        $gama = str_replace(' ', '_', acentos($vehicle->gama));
        $version = str_replace(' ', '_', acentos(strtolower($vehicle->version)));
        $id = $vehicle->idVOPR;

        $urlBase = 'https://percar.es';

        $content .= '<div class="all-cars-list-arch vehiculos-ocasion-lista">';
            $content .= '<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">';
                $content .= '<div class="car-item gray-bg text-center last">';

                    $content .= '<div class="car-image">';
                        $content .= '<img class="img-responsive" src="https://www.sibuscascoche.com/img/coches/'.$marca.'/'.$gama.'/'.$id.'/00.jpg" alt="" height="190">';
                            $content .= '<div class="car-overlay-banner">';
                                $content .=  '<ul>';
                                    $content .= '<li><a href="'.$urlBase.'/vehiculos-de-ocasion/listado-coches/detalles/?'.$marca.'-'.$gama.'-'.$version.'-'.strtolower($vehicle->empresa).'-'.$id.'&sub='.$vehicle->subempresa.'" data-toggle="tooltip" title="" tooltip="Ver" tooltip-placement="top" class="psimages" data-original-title="Ver"><i class="fa fa-link"></i></a></li>';
                                    $content .= '<li class="pssrcset"><a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Galer&iacute;a" class="psimages" data-image="';
                                      
                                            foreach($vehicle->imagenes as $img){
                                                    if ($img === end($vehicle->imagenes)) {
                                                        $content .= 'https://www.sibuscascoche.com/img/coches/'.$img;
                                                    } else{
                                                        $content .= 'https://www.sibuscascoche.com/img/coches/'.$img.',';
                                                    }
                                                   
                                            }

                                    $content .= '"><i class="fa fa-expand"></i></a></li>';

                    $content .= '</ul></div>';

                            $content .= '<div class="galeria-mobile pssrcset"><a href="javascript:void(0)" class="psimages" data-image="';
                                      
                                            foreach($vehicle->imagenes as $img){
                                                    if ($img === end($vehicle->imagenes)) {
                                                        $content .= 'https://www.sibuscascoche.com/img/coches/'.$img;
                                                    } else{
                                                        $content .= 'https://www.sibuscascoche.com/img/coches/'.$img.',';
                                                    }
                                            }

                                $content .= '"></a></div>';



                    $content .= '</div>'; // / car-image

                    $content .= '<div class="car-content">';
                        $content .= '<div class="text-slider">';

                                        $content .= '<a href="'.$urlBase.'/vehiculos-de-ocasion/listado-coches/detalles/?'.$marca.'-'.$gama.'-'.$version.'-'.$vehicle->empresa.'-'.$id.'&sub='.$vehicle->subempresa.'"><span class="info-marca">'.strtolower($vehicle->marca).'</span><span class="info-modelo"> '.strtolower($vehicle->gama).'</span><br><div class="info-caracteristicas"><span>'.$vehicle->version.'</span></div></a>';


                                        $content .= '<div class="price car-price">';
                                        if($vehicle->estado == 'VENDIDO'){
                                            $content .=   '<div><span class="label car-status sold">--</span></div>';
                                            $content .=   '<div class="vendido-slide">Vendido</div>';
                                        }else if($vehicle->estado == 'RESERVADO'){
                                            $content .=   '<div class="reservado-slide">Reservado</div>';
                                        }
                                        if($vehicle->oferta == 'oferta'){
                                            $content .=   '<div class="oferta-slide">Oferta</div>';
                                            $content .=   '<span class="old-price">'.number_format($vehicle->pvpref,0,'','.').' &euro;</span>';
                                        }
                                        $content .=  '<span class="new-price">'.number_format($vehicle->pvpOfertaWeb,0,'','.').' &euro;</span>';

                                        $content .= '<div class="clear"></div><a href="'.$urlBase.'/vehiculos-de-ocasion/listado-coches/detalles/?'.$marca.'-'.$gama.'-'.$version.'-'.$vehicle->empresa.'-'.$id.'&sub='.$vehicle->subempresa.'" class="car-slide-ver-coche">Ver coche</a><div class="like_mobile">';
                                                if($vehicles->user != 0 || $vehicles->user != '0'){
                                                        if(in_array($vehicle->idVOPR.'_'.$vehicle->subempresa, $likesSub)){
                                                            $content .= yesLike($vehicle->idVOPR, $vehicle->subempresa);
                                                        }else{
                                                            $content .= dontLike($vehicle->idVOPR,$vehicle->subempresa);
                                                        }
                                                    }else{
                                                        $content .= notLoggeIn($vehicle->idVOPR,$vehicle->subempresa);
                                                    }

                                        $content .=  '</div></div>';
                    $content .=  '</div></div>';
        $content .=  '</div></div></div>';

    }

    echo utf8_encode($content);

    function yesLike($id, $subempresa){

        return '<div id="wp-ulike-post--'.$id.'" class="wpulike wpulike-robeen "> <div class="wp_ulike_general_class wp_ulike_is_liked"> <label> <input type="checkbox" data-ulike-id="-'.$id.'" data-ulike-subempresa="'.$subempresa.'" data-ulike-nonce="0" data-ulike-type="likeThis" data-ulike-status="2" class="wp_ulike_btn wp_ulike_put_text" checked="checked"> <svg class="heart-svg" viewBox="467 392 58 57" xmlns="http://www.w3.org/2000/svg"><g class="Group" fill="none" fill-rule="evenodd" transform="translate(467 392)"><path d="M29.144 20.773c-.063-.13-4.227-8.67-11.44-2.59C7.63 28.795 28.94 43.256 29.143 43.394c.204-.138 21.513-14.6 11.44-25.213-7.214-6.08-11.377 2.46-11.44 2.59z" class="heart" fill="#AAB8C2"></path><circle class="main-circ" fill="#E2264D" opacity="0" cx="29.5" cy="29.5" r="1.5"></circle><g class="grp7" opacity="0" transform="translate(7 6)"><circle class="oval1" fill="#9CD8C3" cx="2" cy="6" r="2"></circle><circle class="oval2" fill="#8CE8C3" cx="5" cy="2" r="2"></circle></g><g class="grp6" opacity="0" transform="translate(0 28)"><circle class="oval1" fill="#CC8EF5" cx="2" cy="7" r="2"></circle><circle class="oval2" fill="#91D2FA" cx="3" cy="2" r="2"></circle></g><g class="grp3" opacity="0" transform="translate(52 28)"><circle class="oval2" fill="#9CD8C3" cx="2" cy="7" r="2"></circle><circle class="oval1" fill="#8CE8C3" cx="4" cy="2" r="2"></circle></g><g class="grp2" opacity="0" transform="translate(44 6)" fill="#CC8EF5"><circle class="oval2" transform="matrix(-1 0 0 1 10 0)" cx="5" cy="6" r="2"></circle><circle class="oval1" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp5" opacity="0" transform="translate(14 50)" fill="#91D2FA"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp4" opacity="0" transform="translate(35 50)" fill="#F48EA7"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp1" opacity="0" transform="translate(24)" fill="#9FC7FA"><circle class="oval1" cx="2.5" cy="3" r="2"></circle><circle class="oval2" cx="7.5" cy="2" r="2"></circle></g></g></svg><span class="count-box">1+</span></label></div></div>';
    }

    function dontLike($id, $subempresa){
        return '<div id="wp-ulike-post--'.$id.'" class="wpulike wpulike-robeen "> <div class="wp_ulike_general_class wp_ulike_is_unliked"> <label> <input type="checkbox" data-ulike-id="-'.$id.'"  data-ulike-subempresa="'.$subempresa.'" data-ulike-nonce="0" data-ulike-type="likeThis" data-ulike-status="3" class="wp_ulike_btn wp_ulike_put_text"> <svg class="heart-svg" viewBox="467 392 58 57" xmlns="http://www.w3.org/2000/svg"><g class="Group" fill="none" fill-rule="evenodd" transform="translate(467 392)"><path d="M29.144 20.773c-.063-.13-4.227-8.67-11.44-2.59C7.63 28.795 28.94 43.256 29.143 43.394c.204-.138 21.513-14.6 11.44-25.213-7.214-6.08-11.377 2.46-11.44 2.59z" class="heart" fill="#AAB8C2"></path><circle class="main-circ" fill="#E2264D" opacity="0" cx="29.5" cy="29.5" r="1.5"></circle><g class="grp7" opacity="0" transform="translate(7 6)"><circle class="oval1" fill="#9CD8C3" cx="2" cy="6" r="2"></circle><circle class="oval2" fill="#8CE8C3" cx="5" cy="2" r="2"></circle></g><g class="grp6" opacity="0" transform="translate(0 28)"><circle class="oval1" fill="#CC8EF5" cx="2" cy="7" r="2"></circle><circle class="oval2" fill="#91D2FA" cx="3" cy="2" r="2"></circle></g><g class="grp3" opacity="0" transform="translate(52 28)"><circle class="oval2" fill="#9CD8C3" cx="2" cy="7" r="2"></circle><circle class="oval1" fill="#8CE8C3" cx="4" cy="2" r="2"></circle></g><g class="grp2" opacity="0" transform="translate(44 6)" fill="#CC8EF5"><circle class="oval2" transform="matrix(-1 0 0 1 10 0)" cx="5" cy="6" r="2"></circle><circle class="oval1" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp5" opacity="0" transform="translate(14 50)" fill="#91D2FA"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp4" opacity="0" transform="translate(35 50)" fill="#F48EA7"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp1" opacity="0" transform="translate(24)" fill="#9FC7FA"><circle class="oval1" cx="2.5" cy="3" r="2"></circle><circle class="oval2" cx="7.5" cy="2" r="2"></circle></g></g></svg> </label> </div> </div>';
    }

    function notLoggeIn($id, $subempresa){
        return '<div id="wp-ulike-post--'.$id.'" class="wpulike wpulike-robeen "> <div class="wp_ulike_general_class wp_ulike_is_unliked"> <label> <input type="checkbox" data-ulike-id="-'.$id.'"  data-ulike-subempresa="'.$subempresa.'" data-ulike-nonce="0" data-ulike-type="likeThis" data-ulike-status="0" class="wp_ulike_btn wp_ulike_put_text"> <svg class="heart-svg" viewBox="467 392 58 57" xmlns="http://www.w3.org/2000/svg"><g class="Group" fill="none" fill-rule="evenodd" transform="translate(467 392)"><path d="M29.144 20.773c-.063-.13-4.227-8.67-11.44-2.59C7.63 28.795 28.94 43.256 29.143 43.394c.204-.138 21.513-14.6 11.44-25.213-7.214-6.08-11.377 2.46-11.44 2.59z" class="heart" fill="#AAB8C2"></path><circle class="main-circ" fill="#E2264D" opacity="0" cx="29.5" cy="29.5" r="1.5"></circle><g class="grp7" opacity="0" transform="translate(7 6)"><circle class="oval1" fill="#9CD8C3" cx="2" cy="6" r="2"></circle><circle class="oval2" fill="#8CE8C3" cx="5" cy="2" r="2"></circle></g><g class="grp6" opacity="0" transform="translate(0 28)"><circle class="oval1" fill="#CC8EF5" cx="2" cy="7" r="2"></circle><circle class="oval2" fill="#91D2FA" cx="3" cy="2" r="2"></circle></g><g class="grp3" opacity="0" transform="translate(52 28)"><circle class="oval2" fill="#9CD8C3" cx="2" cy="7" r="2"></circle><circle class="oval1" fill="#8CE8C3" cx="4" cy="2" r="2"></circle></g><g class="grp2" opacity="0" transform="translate(44 6)" fill="#CC8EF5"><circle class="oval2" transform="matrix(-1 0 0 1 10 0)" cx="5" cy="6" r="2"></circle><circle class="oval1" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp5" opacity="0" transform="translate(14 50)" fill="#91D2FA"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp4" opacity="0" transform="translate(35 50)" fill="#F48EA7"><circle class="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"></circle><circle class="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"></circle></g><g class="grp1" opacity="0" transform="translate(24)" fill="#9FC7FA"><circle class="oval1" cx="2.5" cy="3" r="2"></circle><circle class="oval2" cx="7.5" cy="2" r="2"></circle></g></g></svg> </label> </div> </div>';
    }

    function acentos($cont){ 
        $tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ"; 
        $replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn"; 
        return(strtr($cont,$tofind,$replac)); 
    }

?>

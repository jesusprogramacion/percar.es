<style>body{background:#e0e0e0 }</style>

<?php 
    global $wpdb;
    $user_id =  get_current_user_id();
            $resultados= $wpdb->get_results( "SELECT subempresa,post_id,status FROM r5awp_ulike WHERE user_id = $user_id");
            $likes_ocasion = '';
            $subempresas_ocasion = '';
            foreach($resultados as $res){
                $id_ocasion = strpbrk($res->post_id, '-');
                if($id_ocasion!=false && $res->status == 'like'){
                    $likes_ocasion .= $id_ocasion; 
                    $subempresas_ocasion .= '_'.$res->subempresa;
                } 
            }
?>
<div class="col-md-12 product-listing product-listing-ocasion" ng-controller="vehiclesListController" data-ng-init="likesInit(<?php echo htmlspecialchars(json_encode($likes_ocasion)); ?>, <?php echo htmlspecialchars(json_encode($subempresas_ocasion)); ?>, <?php echo htmlspecialchars(json_encode($user_id)); ?>)">

 <div class="section-title text-center">

                        <h2>{{vehiclesLength}} Vehículos coinciden con tu selección</h2>
                    </div>



<div class="cars-top-filters-box-right filtos-ocasion" ng-show="resultados == true">
    <div class="selected-box">
          <select name="cars_pp-ocasion" id="pgs_cars_pp-ocasion" ng-model="selectedNumpage" ng-change='onNumpagechange(selectedNumpage)'>
            <option value="">12</option>
            <option value="24">24</option>
            <option value="36">36</option>
            <option value="48">48</option>
            <option value="60">60</option>
        </select> 

        <select class="select-box" name="cars_orderby-ocasion" id="pgs_cars_orderby-ocasion" ng-model="selectedFilter" ng-change='onFilterchange(selectedFilter, <?php echo htmlspecialchars(json_encode($likes_ocasion)); ?>)'>
            <option value="" selected>Ordenar búsqueda</option>
            <option value="pvpOfertaWeb">Ordenar por Precio</option>
            <option value="marca">Ordenar por Marca</option>
            <option value="fechaMatriculacion">Ordenar por Fecha</option>
        </select>
 
    </div>
</div>

<div id="listado-vehiculos-ocasion" ng-bind-html="ListContent | to_trusted" ></div>

<div class="paginate-ocasion"  ng-show="resultados == true"></div>

<!--  <div class="all-cars-list-arch vehiculos-ocasion-lista"  ng-controller="vehiclesListController">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" ng-repeat="vehicle in vehiclespag">
        
            <div class="car-item gray-bg text-center last" style="height:404px">
                        <div class="car-image">

                            <img class="img-responsive" src="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg" alt="" height="190">

                            <div class="car-overlay-banner">
                                <ul>
                                 <li><a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" data-toggle="tooltip" tooltip="Ver" tooltip-placement="top" title="" class="psimages" data-original-title="Ver"><i class="fa fa-link"></i></a></li>
                                    <li class="pssrcset"><a href="javascript:void(0)" data-toggle="tooltip" tooltip="Galería" tooltip-placement="top" data-original-title="Galería" title="" class="psimages" data-image="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg"><i class="fa fa-expand"></i></a></li>
                                    <li>
                                       <?php //echo do_shortcode('[wp_ulike_ocasion id="-{{vehicle.idVOPR}}"]'); ?>

                                    </li>
                                    
                                             <?php
                                 //  do_action( 'car_overlay_banner', $id  ); 
                                    ?>
                                </ul>
                            </div>

                        </div>
                <div class="car-content"> 
                    <div class="text-slider">
                            <a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}"><span class="info-marca">{{ vehicle.marca | lowercase}} </span><span class="info-modelo">{{ vehicle.gama | lowercase }} </span><br><div class="info-caracteristicas"><span>{{ vehicle.version }} </span></div></a>


                        <div class="price car-price">
                            <div ng-if="vehicle.estado == 'VENDIDO'"><span class="label car-status sold"></span></div>
                            <div class="vendido-slide"  ng-if="vehicle.estado == 'VENDIDO'">Vendido</div>
                            <div class="reservado-slide"  ng-if="vehicle.estado == 'RESERVADO'">Reservado</div>
                            <div class="oferta-slide"  ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'">Oferta</div>
                            <span class="old-price" ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'VENDIDO' && vehicle.estado != 'RESERVADO'"> {{ vehicle.pvpref | number: 0 | replace: ',': '.' }} € </span>
                            <span class="new-price"> {{ vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' }} €</span>
                            <a href="<?php //echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}" class="car-slide-ver-coche">Ver coche</a>

                        </div>
                    </div>          
                </div>
            </div>

    </div>
<div  ng-show='vehicles.length == 0'>
    <br>
        <div class="alert alert-warning">No se encontraron resultados que coincidan con su selección.</div>
</div>
    <div class="paginate-ocasion" ng-show='vehicles.length > numPerPage'>
      <pagination boundary-links="true" total-items="totalItems" ng-model="currentPage" num-pages="numPages" items-per-page="numPerPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
    </div>

</div>  -->


</div>
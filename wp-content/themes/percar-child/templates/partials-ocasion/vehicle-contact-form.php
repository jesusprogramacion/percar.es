<div class="col-md-6">
    <div class="wrapper-sidebar" ng-controller="formCtrl">

        <div class="searcher-title">
            <h3>Tu coche de ocasión con garantía</h3>
        </div>

        <form id="contact-form" role="form" ng-submit="processForm()">
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Nombre
                            <input type="text" class="form-control" id="name" name="name" ng-model="formData.name" placeholder="Nombre *" title="Requiere un nombre" required>
                        </label>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Apellidos
                            <input type="text" class="form-control" id="surname" name="surname" ng-model="formData.surname" placeholder="Apellidos *" title="Requiere al menos un apellido" required>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Email
                            <input type="text" class="form-control" id="email" name="email" ng-model="formData.email" placeholder="Introducir Email *" pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" title="Requiere un formato de mail válido" required>
                        </label>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Localidad
                            <input type='text' class="form-control" id="place" name='place' ng-model="formData.place" placeholder="Localidad">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12 super-label">Horario de contacto</label>
                        <div class="wrapper-select col-xs-12">
                            <div class="row">
                                <label class="col-xs-6">
                                    <select class="form-control" id="sinceHour" ng-model="formData.sinceHour" ng-init="formData.sinceHour='9'" name="sinceHour">
                                        <option value="9">9:00 h</option>
                                        <option value="10">10:00 h</option>
                                        <option value="11">11:00 h</option>
                                        <option value="12">12:00 h</option>
                                        <option value="13">13:00 h</option>
                                        <option value="14">14:00 h</option>
                                        <option value="15">15:00 h</option>
                                        <option value="16">16:00 h</option>
                                        <option value="17">17:00 h</option>
                                        <option value="18">18:00 h</option>
                                        <option value="19">19:00 h</option>
                                        <option value="20">20:00 h</option>
                                        <option value="21">21:00 h</option>
                                    </select>
                                </label>
                                <label class="col-xs-6">
                                    <select class="form-control" id="untilHour" ng-model="formData.untilHour" ng-init="formData.untilHour='9'" name="untilHour">
                                        <option value="9">9:00 h</option>
                                        <option value="10">10:00 h</option>
                                        <option value="11">11:00 h</option>
                                        <option value="12">12:00 h</option>
                                        <option value="13">13:00 h</option>
                                        <option value="14">14:00 h</option>
                                        <option value="15">15:00 h</option>
                                        <option value="16">16:00 h</option>
                                        <option value="17">17:00 h</option>
                                        <option value="18">18:00 h</option>
                                        <option value="19">19:00 h</option>
                                        <option value="20">20:00 h</option>
                                        <option value="21">21:00 h</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Teléfono
                            <input type="text" class="form-control" style="margin-top: 3px" id="tlf" name="tlf" ng-model="formData.tlf" placeholder="Introducir Teléfono" pattern=".{8,}" title="Requiere 8 dígitos como mínimo" required>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="wrapper-select col-xs-12">
                    <label for="comments" id="contact-form-comments">Comentarios
                        <textarea class="form-control" id="comments" name="comments" ng-model="formData.comments" placeholder="Introducir Comentarios" style="margin: 0"></textarea>
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="wrapper-button col-xs-12 col-md-6 col-md-offset-6" style="margin-top: 0">
                    <div class="row">
                        <div class="col-xs-12">
                            <input class="btn btn-md btn-primary btn-search col-xs-12" type="submit" value="Enviar">
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
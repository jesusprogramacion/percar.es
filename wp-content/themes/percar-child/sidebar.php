<style>body{background:#e0e0e0;}</style>
<div class="sidebar-single-post">

<!-- 	<form role="search" method="get" id="searchform" class="clearfix" action="<?php  //esc_url( home_url( '/' ) ); ?>" >
	<label class="screen-reader-text" for="s"><?php //esc_html__('Search for:', 'cardealer'); ?></label>
	   <input type="text" value="<?php //get_search_query(); ?>" name="s" id="s" class="cardealer-default-search" placeholder="<?php //esc_attr__( 'Search...', 'cardealer' ); ?>" />
	   <input type="submit" id="searchsubmit" value="<?php //esc_attr__('Go','cardealer'); ?>" />
       <div class="cardealer-auto-compalte-default"><ul></ul></div>	
    </form> -->
<?php
if ( is_active_sidebar( 'sidebar-default' ) ){
	dynamic_sidebar( 'sidebar-default' );
}else{
	if( current_user_can('administrator') ){
		?>
		<span><?php echo sprintf( wp_kses( __( 'No widgets added in default sidebar.<br>Click <a href="%s">here</a> to add widgets.', 'cardealer' ),
				array(
					'a' => array(
						'class' => array(),
						'href'  => array(),
					),
					'br' => array(),
				)
			),
			admin_url('widgets.php')
		);
		?></span><?php
	}else{
		echo esc_html__('No widgets founds.', 'cardealer');
	}
}
?>


<div class="label-sidebar">Recibe las últimas novedades</div>
<div class="newsletter-single-sidebar">
<?php echo do_shortcode('[contact-form-7 id="1839"]'); ?>
</div>
<div class="redes-sidebar">
		<div class="label-sidebar">Síguenos en redes sociales</div>
		<div class="social-full">
			<a class="facebook" href="https://www.facebook.com/perezrumbaocar" target="_blank" title="Facebook"><i class="fa fa-facebook"></i> </a>
			<a class="twitter" href="https://twitter.com/PerezRumbaoCar" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
			<a class="instagram" href="https://www.instagram.com/perezrumbaocar/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a>
			<a class="youtube" href="https://www.youtube.com/channel/UCRdsKeKcZ-7b-8p1TSNpgJw" target="_blank" title="Youtube"><i class="fa fa-youtube-play"></i></a>
		</div>
</div>
<div class="buscador-single-sidebar">
<div class="label-sidebar bg-blue">Encuentra tu próximo coche</div>
<?php //echo do_shortcode('[vc_row][vc_column][cars_custom_filters cars_filters="car_make,car_model" filter_style="wide" filter_position="Mo" filter_background="#9ebde4" src_button_label="Ver coches" custom_filters_style="car_filter_style_2"][/vc_column][/vc_row]'); ?>
<?php echo do_shortcode('[formularios-busqueda-sidebar]'); ?>
</div>
</div>
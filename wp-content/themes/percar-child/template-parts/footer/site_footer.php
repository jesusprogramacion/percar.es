<?php
global $car_dealer_options;
do_action( 'cardealer_before_footer' );

$footer_class = array();
$footer_class[] = 'footer bg-2 bg-overlay-black-90';
$footer_class=array_merge($footer_class,cardealer_footer_class());   /* get the footer classes from base functions */
$footer_class = implode(' ', $footer_class);
?>
<footer id="footer" class="<?php echo esc_attr($footer_class);?>">
	<?php do_action('cardealer_before_footer_inner'); ?>
    <div class="container">
	<div class="row">
		<div class="logo-footer"></div>
		<div class="footer-links">
			<a href="<?php echo get_site_url(); ?>/contacto" title="Contacto">Contacto</a>
			<a href="<?php echo get_site_url(); ?>/aviso-legal" title="Aviso legal">Aviso legal</a>
			<a href="<?php echo get_site_url(); ?>/politica-de-cookies" title="Política de cookies">Política de cookies</a>
		</div>
		<hr>
		<p>Síguenos en redes sociales</p>
		<?php
		if( isset($car_dealer_options['show_footer_top']) && $car_dealer_options['show_footer_top'] == 'yes') {
			if( isset($car_dealer_options['social_icon_list']) && !empty($car_dealer_options['social_icon_list']) ) {?>
				<div class="social-full">
					<?php
						$socialIcons = $car_dealer_options['social_icon_list']['Icons to Add'];
						unset($socialIcons['placebo']);
						foreach( $socialIcons as $key => $icon ) {
							if(!empty($car_dealer_options[$key.'_url'])){
							echo '<a class="'. esc_attr($key) .'" href="'. esc_attr($car_dealer_options[$key.'_url']) .'" target="_blank" title="'. ucfirst(esc_attr($key)) .'"><i class="fa fa-'. esc_attr( str_replace('_', '-', $key) ). '"></i> </a>';
							}
						}
					?>
                    <a class="youtube" href="https://www.youtube.com/channel/UCRdsKeKcZ-7b-8p1TSNpgJw" target="_blank" title="Youtube"><i class="fa fa-youtube-play"></i></a>
				</div>
				<?php
			}
		}
		?>
	</div>
    <?php

   
	//get_template_part('template-parts/footer/additionalsidebar');    //show bottom copyright
	do_action('cardealer_after_footer_inner');
	?>
    </div>
	<!-- BOOTOM COPYRIGHT SECTION START -->
	<?php if(isset($car_dealer_options['enable_copyright_footer']) && $car_dealer_options['enable_copyright_footer']=='yes') {?>
	<div class="copyright-block">
		<div class="container">
			<div class="row">				
				<div class="col-lg-12 col-md-12 center">
					&copy; <?php echo date('Y'); ?> Pérez Rumbao Car | <a href="https://www.alola.es/marketing-online/diseno-web/" target="_blank" title="Desarrollo web | alola.es"><span id="desarrollo-pie">Desarrollo web:</span> alola.es</a>
				</div>
			</div>
		</div>	
	</div>
<?php }	?>
	<!-- BOOTOM COPYRIGHT SECTION END -->
</footer>
<?php do_action('cardealer_after_footer');?>
<?php global $car_dealer_options;


					// Check if post navigation is enabled or not.
					$post_nav = isset($car_dealer_options['post_nav']) ? $car_dealer_options['post_nav'] : '' ;
					if( $post_nav ){
						?>
						<nav class="nav-single">
							<?php cardealer_single_nav();?>
						</nav><!-- .nav-single -->
						
						<?php
					}

if( is_single() ){
	$blog_metas = isset($car_dealer_options['single_metas']) ? $car_dealer_options['single_metas'] : '';
	if( empty($blog_metas) ){
		$blog_metas = array (
			'date'      => '1',
			'author'    => '1',
			'categories'=> '1',
			'tags'      => '1',
			'comments'  => '1',
		);
	}
}else{
	$blog_metas = isset($car_dealer_options['blog_metas']) ? $car_dealer_options['blog_metas'] : '' ;
	if( empty($blog_metas) ){
		$blog_metas = array (
			'date'      => '1',
			'author'    => '1',
			'categories'=> '1',
			'tags'      => '1',
			'comments'  => '1',
		);
	}
}
?>
<div class="entry-meta">
	<?php
    if(!empty($blog_metas))
        echo "<ul>";
	foreach( $blog_metas as $blog_meta_k => $blog_meta_v ){
		if( $blog_meta_k == 'date' && !empty($blog_meta_v) ){
			echo sprintf( '<li class="fecha-single-articulo"><time class="entry-date" datetime="%3$s">%4$s</time></li>',
				esc_url( get_permalink() ),
				esc_attr( get_the_time() ),
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date('j \d\e F \d\e Y') )
			);
		}
		
		$category_list_args = array(
			'sep'    => ' ',
			'after'  => '',
		);
		$category_list = get_the_category_list( trim( $category_list_args['sep'] ) . ' ' );

		if( $blog_meta_k == 'categories' && !empty($blog_meta_v) && !empty($category_list) ){
					foreach((get_the_category()) as $category) { 
				    echo '<li><span class="entry-meta-categories '.$category->slug.'"><a href="'.get_site_url().'/categoria/'.$category->slug.'">'.$category->cat_name .'</a></span></li>';
				} 
		?>
		<!-- <li><span class="entry-meta-categories"><?php //echo $category_list;?></span></li> -->
			<?php
		}
		

	}
    if(!empty($blog_metas))
        echo "</ul>";
	//edit_post_link( '<i class="fa fa-pencil"></i> ' . esc_html__( 'Edit', 'cardealer' ), '', '' );
	?>
</div>
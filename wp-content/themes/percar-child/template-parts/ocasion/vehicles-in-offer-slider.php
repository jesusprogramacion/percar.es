<div class="col-md-6">

    <div class="wrapper-sidebar">

        <div class="vehicles-slider">

            <p class="title">Ofertas del Mes</p>

            <slick slides-to-show=3 slides-to-scroll=3 init-onload=true next-arrow="#vehicle-inOffer-next-arrow" prev-arrow="#vehicle-inOffer-prev-arrow" responsive="responsive" data="vehiclesInOffering">

                <div class="wrapper-slider-img" ng-repeat="vehicle in vehiclesInOffering">

                    <a class="slider-link" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/oferta/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}">

                        <div class="vehicle-image" custom-background-image="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg"></div>

                        <div class="text-slider">
                            <h3 class="vehicle-brand-slide">{{ vehicle.marca }} <span>{{ vehicle.gama | removeAccents }}</span></h3>
                            <p class="vehicle-brand-slide">{{ vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' }} €</p>
                        </div>

                    </a>

                </div>

                <a href="" id="vehicle-inOffer-prev-arrow" class="prev-arrow">
                    <img src="http://www.sibuscascoche.com/assets/img/prev.png">
                </a>
                <a href="" id="vehicle-inOffer-next-arrow" class="next-arrow">
                    <img src="http://www.sibuscascoche.com/assets/img/next.png">
                </a>
                <a href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/oferta" class="list-link">
                    <span>+</span> VER TODOS
                </a>

            </slick>

        </div>

    </div>

</div>
<div class="col-md-6">

    <div class="wrapper-sidebar" ng-controller="searcherCtrl">

        <div class="searcher-title">
            <h3>Tu coche de ocasión con garantía</h3>
        </div>

        <form id="searcher-form" class="form-horizontal" role="form" action="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches">
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Marca
                            <select class="form-control" id="brand-select" name="brand-select" ng-model="selectedBrand" ng-options="brand.marca for brand in brands track by brand.marca" ng-change='onBrandchange(selectedBrand)'>
                                <option value="">Seleccione Marca</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Modelo
                            <select class="form-control" id="model-select" name="model-select" ng-model="selectedModel" ng-disabled="!models" ng-options="model.gama for model in models track by model.gama" ng-change='onModelchange(selectedModel, {{ selectedBrand }} )'>
                                <option value="">Seleccione Modelo</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Combustible
                            <select class="form-control" id="fuel-select" name="fuel-select" ng-model="dataForm.fuel">
                                <option value="">Seleccione Combustible</option>
                                <option value="gasolina">Gasolina</option>
                                <option value="diesel">Diésel</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Cambio
                            <select id="change-select" class="form-control" name="change-select" ng-model="dataForm.change">
                                <option value="" selected="selected">Seleccione Cambio</option>
                                <option value="automatic">Automático</option>
                                <option value="manual">Manual</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12 super-label">Precio</label>
                        <div class="wrapper-select col-xs-12">
                            <div class="row">
                                <label class="col-xs-6">
                                    <select id="sincePrice-select" class="form-control" name="sincePrice-select" ng-model="dataForm.sincePrice" ng-init="dataForm.sincePrice='0'">
                                        <option value="0" selected="selected">Desde</option>
                                        <option value="3000">3.000 €</option>
                                        <option value="4000">4.000 €</option>
                                        <option value="5000">5.000 €</option>
                                        <option value="6000">6.000 €</option>
                                        <option value="7000">7.000 €</option>
                                        <option value="8000">8.000 €</option>
                                        <option value="9000">9.000 €</option>
                                        <option value="10000">10.000 €</option>
                                        <option value="12000">12.000 €</option>
                                        <option value="14000">14.000 €</option>
                                        <option value="30000">30.000 €</option>
                                        <option value="40000">40.000 €</option>
                                        <option value="50000">50.000 €</option>
                                    </select>
                                </label>
                                <label class="col-xs-6">
                                    <select id="untilPrice-select" class="form-control" name="untilPrice-select" ng-model="dataForm.untilPrice" ng-init="dataForm.untilPrice='100000'">
                                        <option value="100000" selected="selected">Hasta</option>
                                        <option value="3000">3.000 €</option>
                                        <option value="4000">4.000 €</option>
                                        <option value="5000">5.000 €</option>
                                        <option value="6000">6.000 €</option>
                                        <option value="7000">7.000 €</option>
                                        <option value="8000">8.000 €</option>
                                        <option value="9000">9.000 €</option>
                                        <option value="10000">10.000 €</option>
                                        <option value="12000">12.000 €</option>
                                        <option value="14000">14.000 €</option>
                                        <option value="30000">30.000 €</option>
                                        <option value="40000">40.000 €</option>
                                        <option value="50000">50.000 €</option>
                                        <option value="100001">Más de 50.000 €</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12">Carrocería
                            <select id="bodycar-select" class="form-control" name="bodycar-select" ng-model="selectedBodycar" ng-options="bodycar.tipoCarroceria for bodycar in bodycars track by bodycar.tipoCarroceria">
                                <option value="" selected="selected">Seleccione Carrocería</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="wrapper-select col-xs-12 col-md-6">
                    <div class="row">
                        <label class="col-xs-12 super-label">Fecha de Matriculación</label>
                        <div class="wrapper-select col-xs-12">
                            <div class="row">
                                <label class="col-xs-6">
                                    <select id="sinceYear-select" class="form-control" name="sinceYear-select" ng-model="dataForm.sinceYear" ng-init="dataForm.sinceYear='1900'">
                                        <option value="1900" selected="selected">Desde</option>
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                    </select>
                                </label>
                                <label class="col-xs-6">
                                    <select id="untilYear-select" class="form-control" name="untilYear-select" ng-model="dataForm.untilYear" ng-init="dataForm.untilYear='3000'">
                                        <option value="3000" selected="selected">Hasta</option>
                                        <option value="2000">2000</option>
                                        <option value="2001">2001</option>
                                        <option value="2002">2002</option>
                                        <option value="2003">2003</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper-button col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <input class="btn btn-md btn-primary btn-search col-xs-12" type="submit" value="Buscar">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <a href="http://sivendescoche.com/" target="_blank" title="¿Quieres vender tu coche?">
        <div class="banner-promotion">
            <img src="<?php echo WP_HOME ?>/wp-content/themes/percar/images/sivendescoche.jpg" width="100%">
        </div>
    </a>

</div>
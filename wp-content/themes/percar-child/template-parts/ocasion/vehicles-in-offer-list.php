<div class="col-md-6">

    <div class="navbar-slider">

        <a href="<?php echo PREV_URL ?>" class="prev prev-page" id="list-prev-buttom">Volver</a>

    </div>

    <div class="wrapper-sidebar wrapper-vehicle-list" ng-controller="vehiclesListSliderCtrl">

        <div class="vehicle-list-title">Vehículos en Ocasión</div>

        <div class="vehicles-list" ng-scrollbars ng-scrollbars-config="config">

            <a class="vehicle-itemList" ng-repeat="vehicle in vehiclesInOffering" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/oferta/detalles/?{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | replaceSpaces: '_'  | lowercase }}-{{ vehicle.empresa | lowercase }}-{{ vehicle.idVOPR }}">

                <div class="wrapper-photoIcon">
                    <div class="photoIcon" custom-background-image="http://www.sibuscascoche.com/img/coches/{{ vehicle.marca | removeAccents | replaceSpaces: '_' }}/{{ vehicle.gama | removeAccents | replaceSpaces: '_' }}/{{ vehicle.idVOPR }}/00.jpg">
                        <img ng-if="vehicle.oferta == 'oferta' && vehicle.estado != 'RESERVADO'" src="http://www.sibuscascoche.com/assets/img/etiqueta-oferta.png">
                        <img ng-if="vehicle.oferta == 'oferta' && vehicle.estado == 'RESERVADO'" src="http://www.sibuscascoche.com/assets/img/etiqueta-reservado.png">
                        <img ng-if="vehicle.estado == 'VENDIDO'" src="http://www.sibuscascoche.com/assets/img/etiqueta-vendido.png">
                    </div>
                </div>

                <div class="wrapper-infoVehicle">
                    <h3>{{ vehicle.marca }}<span> {{ vehicle.gama }} </span>{{ vehicle.version }}</h3>
                    <div class="vehicle-description">{{ vehicle.comentario | cut: true: 60: '...' }}</div>
                    <div class="vehicle-price">{{ vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' }} €</div>
                </div>

                <div class="vehicle-specifications">
                    <span>{{ vehicle.combustible }}</span>
                <span>
                    Matriculación:
                    {{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'dd' }}-
                    {{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'MM' }}-
                    {{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'yyyy' }}
                </span>
                <span ng-if="vehicle.garantia">
                    Garantía de:
                    <b class="guarantee">{{ vehicle.garantia | lowercase }}</b>
                </span>
                </div>
            </a>

        </div>

    </div>

</div>
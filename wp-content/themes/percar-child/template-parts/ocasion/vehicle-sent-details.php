<div class="col-md-6" ng-controller="vehiclesSentDetailsCtrl">
    <a href="<?php echo PREV_URL ?>" class="prev prev-page" id="details-prev-buttom">Volver</a>
    <div class="navbar-slider">
        <a class="wrapper-next" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/vendidos/detalles?{{ nextVehicle.marca | lowercase }}-{{ nextVehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ nextVehicle.version | replaceSpaces: '_'  | lowercase }}-{{ nextVehicle.empresa | lowercase }}-{{ nextVehicle.idVOPR }}">
            <span class="next">Siguiente</span>
        </a>
        <a class="wrapper-prev" href="<?php echo WP_HOME ?>/vehiculos-de-ocasion/listado-coches/vendidos/detalles?{{ prevVehicle.marca | lowercase }}-{{ prevVehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ prevVehicle.version | replaceSpaces: '_'  | lowercase }}-{{ prevVehicle.empresa | lowercase }}-{{ prevVehicle.idVOPR }}">
            <div class="prev">Anterior</div>
        </a>
    </div>
    <div class="wrapper-sidebar" id="wrapper-vehicle-data">
        <div class="vehicle-title" data-brand="{{ vehicle.marca }}" data-model="{{ vehicle.gama }}" data-version="{{ vehicle.version }}" data-bussiness="{{ vehicle.empresa }}" data-type="{{ vehicle.tipovo }}" data-idVOPR="{{ vehicle.idVOPR }}">
            {{ vehicle.marca }} {{ vehicle.gama }} {{ vehicle.version }}<span class="vehicle-price" ng-bind=" ( vehicle.pvpOfertaWeb | number: 0 | replace: ',': '.' ) + ' €'"> </span>
        </div>
        <div class="vehicle-details-slider">
            <slick class="slide-1" slides-to-show=1 slides-to-scroll=1 init-onload=true data="photoVehicleList" autoplay=true arrows=false as-nav-for=".slide-2">
                <div class="wrapper-slider-img" ng-repeat="photo in photoVehicleList">
                    <div class="main-slider-img" custom-background-image="http://www.sibuscascoche.com/img/coches/{{ photo }}">
                        <img ng-if="vehicle.estado == 'VENDIDO'" src="http://www.sibuscascoche.com/assets/img/etiqueta-vendido-single.png">
                    </div>
                </div>
            </slick>
            <slick class="slide-2" slides-to-show=5 slides-to-scroll=5 init-onload=true data="photoVehicleList" responsive="responsive" arrows=true focus-on-select=true as-nav-for=".slide-1" next-arrow=".next-arrow" prev-arrow=".prev-arrow">
                <div class="wrapper-slider-img" ng-repeat="photo in photoVehicleList">
                    <div class="carousel-img" custom-background-image="http://www.sibuscascoche.com/img/coches/{{ photo }}"></div>
                </div>
            </slick>
            <div class="wrapper-slider-arrows">
                <a href="" class="prev-arrow">
                    <img src="http://www.sibuscascoche.com/assets/img/prev.png">
                </a>
                <a href="" class="next-arrow">
                    <img src="http://www.sibuscascoche.com/assets/img/next.png">
                </a>
            </div>
            <div class="wrapper-vehicle-description" ng-show="vehicle.comentario">
                <div class="vehicle-description">
                    {{ vehicle.comentario }}
                </div>
            </div>
            <div class="vehicle-description-separator" ng-hide="vehicle.comentario"></div>
            <ul class="accordion">
                <li>
                    <div class="wrapper-pdf-icon">
                        <a href="http://www.sibuscascoche.com/pdf/{{ vehicle.marca | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.gama | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.version | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.empresa | removeAccents | replaceSpaces: '_' | lowercase }}-{{ vehicle.idVOPR }}" target="_blank">
                            <?php if ( !wp_is_mobile() ) echo 'Imprimir'; ?>
                            <img src="http://www.sibuscascoche.com/assets/img/imprimir.png">
                        </a>
                    </div>
                    <div class="accordion-title">
                        <img src="http://www.sibuscascoche.com/assets/img/flecha-li-abajo.png"><span>Ver mas detalles</span>
                    </div>
                    <div class="wrapper-accordion-content">
                        <div class="vehicle-data">
                            <div class="specification-oddRow">
                                <div class="specification-left-col">Combustible:</div>
                                <div class="specification-right-col">{{ vehicle.combustible | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-evenRow">
                                <div class="specification-left-col">Fecha de Matriculación:</div>
                                <div class="specification-right-col">
                                    {{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'dd' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'MM' }}-{{ vehicle.fechaMatriculacion | split: ' ': 0 | date : 'yyyy' }}
                                </div>
                            </div>
                            <div class="specification-oddRow">
                                <div class="specification-left-col">Kilometraje:</div>
                                <div class="specification-right-col">{{ vehicle.km  | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-evenRow">
                                <div class="specification-left-col">Número de puertas:</div>
                                <div class="specification-right-col">{{ vehicle.puertas | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-oddRow">
                                <div class="specification-left-col">Color:</div>
                                <div class="specification-right-col">{{ vehicle.color | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-evenRow">
                                <div class="specification-left-col">Metalizado:</div>
                                <div class="specification-right-col">{{ vehicle.metalizado | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-oddRow">
                                <div class="specification-left-col">Tipo de Carrocería:</div>
                                <div class="specification-right-col">{{ vehicle.tipoCarroceria | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-evenRow">
                                <div class="specification-left-col">Cilindrada:</div>
                                <div class="specification-right-col">{{ vehicle.cilindrada | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-oddRow">
                                <div class="specification-left-col">Garantía:</div>
                                <div class="specification-right-col">{{ vehicle.garantia | lowercase | ucfirst }}</div>
                            </div>
                            <div class="specification-evenRow">
                                <div class="specification-left-col">Número de Referencia:</div>
                                <div class="specification-right-col">{{ vehicle.idVOPR | lowercase }}</div>
                            </div>
                        </div>
                        <div class="equipment-info">
                            <p><b>Equipamiento:</b></p>
                            <p ng-repeat="equipment in vehicle.equipamiento | nl2br">{{ equipment }}</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

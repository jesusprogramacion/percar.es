<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog-2 <?php if ( !has_post_thumbnail() ) { echo esc_attr('blog-no-image'); }?>">
		<?php
		$excerpt = get_the_excerpt();
		$excerpt = cdhl_shortenString($excerpt,350, false, true);
		?>
		<a href="<?php echo esc_url(get_permalink()); ?>" class="blog-image">
			<?php
			if ( has_post_thumbnail() ) { ?>
				<img alt="" src="<?php the_post_thumbnail_url('cardealer-blog-thumb');?>" class="img-responsive">
			<?php
			}
			?>
			
		</a>
		<div class="blog-content">
			<div class="blog-description blog-description-slide">
				<a class="blog-title-slide" href="<?php echo esc_url(get_permalink());?>"><?php echo esc_html(get_the_title());?></a>
				<?php the_excerpt();?>

        		<a href="<?php echo esc_url(get_permalink()); ?>" class="blog-link-slide">Leer más &gt;</a>
			</div>
		</div>
	</div>
</article><!-- #post-## -->
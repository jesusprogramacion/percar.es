<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-entry">
    	<?php 

        get_template_part( 'template-parts/articulo_meta' );
		
		if( is_single() ) {

    	?>
    	<div class="entry-title articulo-titulo">
    		<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
            ?>

    	</div>
		<?php
		}
		//get_template_part( 'template-parts/entry_meta' );?>
    	
    	<div class="entry-content">
    		<?php
    		if( is_single() ){			
    			the_content();
    			wp_link_pages( array(
    				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages', 'cardealer' ) . ':</span>',
    				'after'       => '</div>',
    				'link_before' => '<span class="page-number">',
    				'link_after'  => '</span>',
    			) );
    		}else{
    			the_excerpt();
    		}
    		?>
    	</div>
    	<?php //echo do_shortcode('[ssba-buttons]'); ?>
    	<?php get_template_part( 'template-parts/entry_footer' );?>
	</div>
</article><!-- #post-## -->
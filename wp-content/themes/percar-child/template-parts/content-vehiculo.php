<?php
global $car_dealer_options, $post;
?>

<section class="banner-single-noticia inner-intro header_intro" Style="background-image:url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url( 'cardealer-blog-thumb' ) ;}?>');">

	<div class="<?php echo esc_attr($container_class); ?>">
			<?php
			$title = get_the_title();
			$subtitle = '';
			
			$show_on_front     	= get_option( 'show_on_front' );
			$page_on_front     	= get_option( 'page_on_front' );
			$page_for_posts_id 	= get_option( 'page_for_posts' );
			
			if(is_singular()){
				if(get_post_type()=='cars'){
				    $title = esc_html__('Car Details','cardealer');
                    $cars_details_title = (isset($car_dealer_options['cars-details-title']))?$car_dealer_options['cars-details-title']:'';
                    if(isset($cars_details_title) && !empty($cars_details_title)){
                        $title = $cars_details_title.'dsdfsdfs';
                    }
				}
                $subtitle = get_post_meta(get_the_ID(),'subtitle',true);
			}elseif( is_home() ){
				$blog_title = isset($car_dealer_options['blog_title']) ? $car_dealer_options['blog_title'] : '' ;
				$blog_subtitle = isset($car_dealer_options['blog_subtitle']) ? $car_dealer_options['blog_subtitle'] : '';
				
				if( $show_on_front == 'posts' ){
					$title = esc_html__( 'Blog', 'cardealer' );
					if( !empty($blog_title) ){
						$title = $blog_title;
					}
					if( !empty($blog_subtitle) ){
						$subtitle = $blog_subtitle;
					}
				}elseif( $show_on_front == 'page' ){
					$page_for_posts_data = get_post($page_for_posts_id);
					$title = $page_for_posts_data->post_title;
					$subtitle_meta = get_post_meta($page_for_posts_id,'subtitle', true);
					if( !empty($subtitle_meta) ){
						$subtitle = $subtitle_meta;
					}elseif( empty($subtitle_meta) && !empty($blog_subtitle) ){
						$subtitle = $blog_subtitle;
					}
				}
			}elseif( is_archive() ){
				if ( is_day() ){
					$title = esc_html__( 'Daily Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__( 'Date: %s', 'cardealer' ), '<span>' . get_the_date() . '</span>' );
				}elseif ( is_month() ){
					$title = esc_html__( 'Monthly Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__('Month: %s', 'cardealer' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'cardealer' ) ) . '</span>' );
				}elseif ( is_year() ){
					$title = esc_html__( 'Yearly Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__( 'Year: %s', 'cardealer' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'cardealer' ) ) . '</span>' );
				}elseif ( is_category() ){
					$title = esc_html__( 'Category Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__( 'Category Name: %s', 'cardealer' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				}elseif ( is_tag() ){
					$title = esc_html__( 'Tag Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__( 'Tag Name: %s', 'cardealer' ), '<span>' . single_tag_title( '', false ) . '</span>' );
				}elseif ( is_author() ){					
					$title = esc_html__('Author Archives', 'cardealer' );
					$subtitle = sprintf( esc_html__( 'Author Name: %s', 'cardealer' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
				}elseif(is_archive() && get_post_type()=='post'){
                    $title = esc_html__( 'Archives', 'cardealer' );
                } else{
					$post_id = cardealer_get_current_post_id();
					$subtitle = get_post_meta($post_id, 'subtitle', true);
					$queried_object = get_queried_object();
					if( $queried_object->name == 'cars' ){
						$page_path = 'cars';
						if( isset( $car_dealer_options['cars_inventory_page'] ) ){
							$car_page = get_post($post_id);
							$page_path = isset($car_page->post_name)? $car_page->post_name: 'cars';
						}
                        $page  = get_page_by_path( $page_path );
						if( $page ){
							$page_title = get_the_title( $page );
						}
                        $cars_listing_title = (isset($car_dealer_options['cars-listing-title']))?$car_dealer_options['cars-listing-title']:'';
                        if(isset($page_title) && !empty($page_title)){
                            $title = $page_title;
                        }elseif(isset($cars_listing_title) && !empty($cars_listing_title)){
                            $title = $cars_listing_title;
                        } else {
                            $title =  post_type_archive_title( '', false );
                        }
                    }else {
                        $title =  post_type_archive_title( '', false );
                    }
                }
			}elseif( is_search() ){
				$title = esc_html__( 'Search', 'cardealer' );
				$subtitle = '';
			}elseif( is_404() ){
				$title = esc_html__( '404 error', 'cardealer' );
				$subtitle = '';
			}

            if( function_exists('is_shop')){
				if(is_shop()){
					if(apply_filters('woocommerce_show_page_title' , true)){
						add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );
						function woo_hide_page_title() {
							return false;
						}
					}
				}
			}
			global $cardealer_title, $cardealer_subtitle;
			$cardealer_title = $title;
			$cardealer_subtitle = $subtitle;
			do_action( 'cardealer_before_title' );
			?>
			<div class="row intro-title title-<?php echo esc_attr($title_alignment); ?>">
				<?php
					if( $titlebar_view == 'title_l_bread_r' ) echo '<div class="col-sm-6 text-left">';
					elseif( $titlebar_view == 'bread_l_title_r' ) echo '<div class="col-sm-6 text-right col-sm-push-6">';
					?>
						<!-- <h1 class="text-orange"><?php //echo esc_html($cardealer_title);?></h1> -->
						<?php
                       // if(isset($cardealer_subtitle) && !empty($cardealer_subtitle)){?>
                     <!--        <p class="text-orange"> -->
    						<?php
    							// printf (
    							// 	wp_kses(
    							// 		$cardealer_subtitle,
    							// 		array(
    							// 			'span'=> array( 'style'=> array(), 'class'=> array()),
    							// 			'a'=> array( 'href'=> array(), 'class'=> array(), 'title'=> array(), 'rel'=> array())
    							// 		)
    							// 	)
    							// );
    						?>
    					<!-- 	</p> -->
                            <?php
                       // }
					if( $titlebar_view == 'title_l_bread_r' ) echo '</div><div class="col-sm-6 text-right">';
					elseif( $titlebar_view == 'bread_l_title_r' ) echo '</div><div class="col-sm-6 text-left col-sm-pull-6">';
				?>
			<?php
			if( function_exists( 'bcn_display_list' )
				&& ( !is_home() || ( is_home() && $show_on_front == 'page' && ( $page_on_front != 0 || $page_on_front == 0 ) ) )
				&& isset( $car_dealer_options['display_breadcrumb'] )
				&& !empty( $car_dealer_options['display_breadcrumb'] )
			){
				?>
    				<ul class="page-breadcrumb <?php echo esc_attr($mobile_breadcrumb_class);?>" typeof="BreadcrumbList" vocab="http://schema.org/">
    					<?php bcn_display_list();?>
    				</ul>
				<?php
			}
			if( $titlebar_view == 'title_l_bread_r' || $titlebar_view == 'bread_l_title_r' ) echo '</div>';
			?>
			</div>
	</div>
</section>
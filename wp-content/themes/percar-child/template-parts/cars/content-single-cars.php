<?php
global $car_dealer_options;
?>
<div class="ficha-head">
    <div class="container">

        <div class="row">

            
        </div>

        <div class="row car-detail-galeria">
            <div class="col-lg-12 col-md-12 no-margin">
                     <div class="col-lg-12 col-md-12">
                        <div class="col-lg-6 col-md-6 car-detail-galeria-arrow-left">
                            <?php
                                $prev_post = get_previous_post();
                                if($prev_post) {
                                   $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
                                   echo  '<a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class="port-arrow port-arrow-car-left tooltip-content-1" data-original-title="' . $prev_title. '" data-toggle="tooltip" data-placement="right"><i class="fa fa-angle-left"></i></a>';
                                }
                            ?>
                        </div>
                         <div class="col-lg-6 col-md-6  car-detail-galeria-arrow-right">
                            <?php
                                $next_post = get_next_post();
                                if($next_post) {
                                   $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
                                   echo '<div ><a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class="port-arrow port-arrow-car-right tooltip-content" data-original-title="' . $next_title. '" data-toggle="tooltip" data-placement="left"><i class="fa fa-angle-right"></i></a>';
                                }
                            ?>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-0"></div>

                        <div class="col-lg-8 col-md-8 col-sm-12 car-detail-galeria-inner">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <h3><?php
                                        $make = get_the_terms($post->ID,'car_make');
                                        $model = get_the_terms($post->ID,'car_model');
                                        $motorizacion = get_the_terms($post->ID,'car_engine');
                                        $acabado = get_the_terms($post->ID,'car_body_style');


                                    $titulo = explode(" ", get_the_title());
                                    echo "<span>".$make[0]->name."</span> ";
                                    echo $model[0]->name;
                                    echo '<div class="info-caracteristicas">'.$motorizacion[0]->name.' '.$acabado[0]->name.'</div>';
                                 ?></h3>
                          
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">    
                                <?php cardealer_car_price_html('text-right'); ?>  
                              
                            </div> 
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 galeria-vehiculo-details">

<!--             <div id="tabs">
                        <ul class="tabs tabs-view-detalles">
                        
                            
                            <li data-tabs="galeria" class="active">Galería</li>
                            <li data-tabs="exterior">Exterior</li>    

                            <li data-tabs="interior">Interior</li>  
                        </ul>

                        <div id="galeria" class="tabcontent">   -->      

                            <?php get_template_part('template-parts/cars/single-car/car-image');?> 

  <!--                       </div>
                        <div id="exterior" class="tabcontent">
                              <div class="threesixty-wrap">
                                <div class="threesixty car">
                                    <div class="spinner">
                                        <span>0%</span>
                                    </div>
                                    <ol class="threesixty_images"></ol>
                                </div>

                            </div>

                                <div class="pag">
                                    <a class="btn btn-danger custom_previous"><i class="fa fa-backward"></i></a>
                                    <a class="btn btn-inverse custom_play"><i class="fa fa-play"></i></a>
                                    <a class="btn btn-inverse custom_stop"><i class="fa fa-pause"></i></a>
                                    <a class="btn btn-danger custom_next"> <i class="fa fa-forward"></i></a>
                                </div>
                        </div>
                        <div id="interior" class="tabcontent">
                  



        <script>
            /* If you want to stop starting auto scroll panorama  just change 'false' value below. */
            var autoScrol = true;
         </script>

                                    <div class="panorama-body">
    
            <img id="panoramaImage" src="https://percar.aloladev.com/wp-content/themes/percar-child/images/interior/simulacion.jpg"> 

            <div id="panorama">
                <span id="fullScreenMode" >
                    <span id="fullscreenPan"><img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/fullscreen.png"></span>
                </span>
            </div>



     
        </div>
                <div class="keys">
                    <div class="control-key">
                         <img class="control_keys" id="left_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/left_button.PNG">
                        <img class="control_keys" id="right_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/right_button.PNG">
                        <img class="control_keys" id="up_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/up_button.PNG">
                        <img class="control_keys" id="down_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/down_button.PNG">
                        <img class="control_keys" id="elipse_key" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/elipse.PNG">
                    </div>
                </div> 
                  <div class="zoom_conrol">
                        <span id="zoom_in"><img class="zoom" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/zoom-in.png"></span>
                        <span id="zoom_out"><img class="zoom" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/virtual-tour/zoom-out.png"></span>
                    </div>




                         
                        </div>


            </div> -->

                        </div>                           
                    </div>

                   

                <div class="clearfix"></div>   
                <div class="details-nav">
                   <ul>              
                      <?php //get_template_part('template-parts/cars/single-car/forms/request_info');?>              
                      <?php //get_template_part('template-parts/cars/single-car/forms/make_an_offer');?>              
                      <?php //get_template_part('template-parts/cars/single-car/forms/schedule_test_drive');?>              
                      <?php //get_template_part('template-parts/cars/single-car/forms/email_to_friend');?>
        			  <?php //get_template_part('template-parts/cars/single-car/forms/financial_form');?>                                        
                      <?php //get_template_part('template-parts/cars/single-car/forms/pdf_brochure'); ?>
                      <?php //get_template_part('template-parts/cars/single-car/forms/print_form');?>              
                   </ul>
                </div>         

            </div>
        </div>
    </div>
</div>
<div class="car-detail-post-option-share">
    <div class="container">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="car-detail-post-option">
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>/informacion-vehiculo?id=<?php echo get_the_ID()?>&v=<?php echo get_the_title(); ?>&v-link=<?php echo $_SERVER['REQUEST_URI'];?>" class="btn-me-interesa">Me interesa</a></li>

                            <li><?php echo do_shortcode('[wp_ulike]'); ?></li>
                           <!--  <li><span  onclick="window.open('<?php //echo get_site_url(); ?>/pdf/?id=<?php //echo get_the_ID(); ?>');" class="btn-detalles-print"><i class="fa fa-print fa-sm"></i></span></li> -->
                        </ul>
                        <?php //get_template_part('template-parts/cars/single-car/share');?>
                        <div class="clearfix"></div>
                    </div>            
                </div>
                <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="ficha-metas">
    <div class="container">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php get_template_part('template-parts/cars/single-car/car-summary');?>               
                </div>
                <div class="clearfix"></div>
        </div>
    </div>
</div>
<button type="button" class="btn btn-ver-detalles" data-toggle="collapse" data-target="#demo">Ver detalles <i class="fa fa-angle-down"></i></button>
<div class="ficha-metas collapse" id="demo">
    <div class="container">
        <div class="row">
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 ver-detalles-tabs">
                    <?php get_template_part('template-parts/cars/single-car/tabs/tabs');?>
                </div>

        </div>
    </div>
</div>

<div class="related-single-cars">
    <div class="container">
        <div class="row">
                <div class="clearfix"></div>
                <div class="col-sm-12">
                   <?php 
                include(locate_template('template-parts/cars/single-car/related.php'));
                 //  get_template_part('template-parts/cars/single-car/related');?>
                </div>
        </div>
    </div>
</div>
<div class="filter-single-cars">
    <div class="container">
        <div class="row">
                <div class="clearfix"></div>
                <div class="section-title text-center">
                    <span></span>
                     <h2>¿Deseas hacer otra búsqueda en <span>km 0</span>?</h2>
                 </div>
                <div class="col-sm-12">
                   <?php echo do_shortcode('[cars_custom_filters cars_filters="car_make,car_model,car_fuel_type" filter_style="Mo" filter_position="default" src_button_label="Ver coches" custom_filters_style="car_filter_style_1"]');?>
                </div>
        </div>
    </div>
</div>

<script>


$(function() {


      var  car = $('.car').ThreeSixty({
            totalFrames: 52, // Total no. of image you have for 360 slider
            endFrame: 52, // end frame for the auto spin animation
            currentFrame: 1, // This the start frame for auto spin
            imgList: '.threesixty_images', // selector for image list
            progress: '.spinner', // selector to show the loading progress
            imagePath:'https://percar.es/wp-content/themes/percar-child/images/sprite/', // path of the image assets
            filePrefix: '', // file prefix if any
            ext: '.png', // extention for the assets
            height: 500,
            width: 500,
            navigation: true,
            responsive: true
        });

    $('.custom_previous').bind('click', function(e) {
      car.previous();
    });

    $('.custom_next').bind('click', function(e) {
      car.next();
    });

    $('.custom_play').bind('click', function(e) {
      car.play();
    });

    $('.custom_stop').bind('click', function(e) {
      car.stop();
    });

    $('#myTab a').on('click', function (e) {
      e.preventDefault()
      $(this).tab('show')
    });

    var ancho = $('.galeria-vehiculo-detail').width();
    var alto = $('.galeria-vehiculo-details').height();

 // // alert(ancho);
 //    var viewer = new Kaleidoscope.Image({
 //       source: 'https://percar.aloladev.com/wp-content/themes/percar-child/images/interior/simulacion.jpg',
 //        containerId: '#container360',
 //        height: 360,
 //        width: 700,
 //    });
 //    viewer.render();

 //    window.onresize = function() {
 
 //     ancho = $('.galeria-vehiculo-details').width();
 //     alto = $('.galeria-vehiculo-details').height();
 //        viewer.setSize({
 //        height: 360,
 //        width: ancho,
 //        });
 //    };




});








    window.onload = init;

    var car;
    function init(){


    }



</script>


<style>
.pag{
    text-align: center;
    margin-bottom: 20px;

}
.pag a{
    display: inline-block;
    color: black;
    padding: 5px;
    cursor: pointer;
    height: 40px;
    line-height: 1.9;
    width: 40px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    text-align: center;
    border-radius: 50%;
    color: #9ebde4;
    border: 2px solid #9ebde4;
}
.pag a:hover{
    background:white;
}
ol > li{
    list-style:none;
}
.threesixty-wrap{
    padding-top:60px;
    margin-bottom:20px;
    background-color:white;
    margin-top:20px !important;
}
.threesixty{
    padding:40px 0;
    min-height:300px;
}
ul.tabs-view-detalles{
    text-align: center;
    margin-top:20px !important;
}
ul.tabs-view-detalles li{
    border: 1px solid  #9ebde4 !important;
    padding: 0px 15px !important;
    font-weight: 500;
    color: #9ebde4 !important;
    -webkit-border-radius: 10px;
    -moz-border-radius:10px;
    border-radius: 10px;
    font-size: 12px !important;
}
ul.tabs-view-detalles li.active{
    border-bottom:0 !important;
    border: 1px solid  #9ebde4 !important;
    color:white !important;
    background-color:#9ebde4 !important;
}
.galeria-vehiculo-details .tabcontent{
    margin-top:0 !important;
}
</style>


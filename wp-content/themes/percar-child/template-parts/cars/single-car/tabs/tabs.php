<?php 
$carId = get_the_ID();
$location = get_post_meta($carId, 'vehicle_location', true);

$descripcion = get_post_meta($carId,'vehicle_overview',true);
$equipacion = get_post_meta($carId,'technical_specifications',true);
$informacion = get_post_meta($carId,'general_information',true);

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs tabs-ver-detalles" id="myTab" role="tablist">
  <?php if(!empty($descripcion)){ ?>
    <li class="nav-item active">
      <a class="nav-link " id="descripcion-tab" data-toggle="tab" href="#descripcion" role="tab" aria-controls="descripcion" aria-selected="true"><i aria-hidden="true" class="fa fa-sliders"></i> Descripción</a>
    </li>
  <?php } ?>
  <?php if(!empty($equipacion)){ ?>
    <li class="nav-item <?php if( empty( $descripcion) ){ ?>active<?php } ?>">
      <a class="nav-link" id="equipacion-tab" data-toggle="tab" href="#equipacion" role="tab" aria-controls="equipacion" aria-selected="false"><i aria-hidden="true" class="fa fa-list"></i> Equipación</a>
    </li>
  <?php } ?>
  <?php if(!empty($informacion)){ ?>
    <li class="nav-item <?php if( empty( $descripcion) && empty( $equipacion) ){ ?>active<?php } ?>">
      <a class="nav-link" id="informacion-tab" data-toggle="tab" href="#informacion" role="tab" aria-controls="informacion" aria-selected="false"><i aria-hidden="true" class="fa fa-info-circle"></i> Información General</a>
    </li>
  <?php } ?>
  <li class="nav-item">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#ubicacion" role="tab" aria-controls="settings" aria-selected="false"><i class="fa fa-map-marker" aria-hidden="true"></i> Ubicación</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <?php if(!empty($descripcion)){ ?>
  <div class="tab-pane active" id="descripcion" role="tabpanel" aria-labelledby="descripcion-tab">
         <?php echo do_shortcode( wpautop(get_post_meta($carId,'vehicle_overview',true)) );?>

  </div>
  <?php } ?>
  <?php if(!empty($equipacion)){ ?>
  <div class="tab-pane <?php if( empty( $descripcion) ){ ?>active<?php } ?>" id="equipacion" role="tabpanel" aria-labelledby="equipacion-tab">

        <?php echo do_shortcode( wpautop(get_post_meta($carId,'technical_specifications',true)) );?>

  </div>
  <?php } ?>
  <?php if(!empty($informacion)){ ?>
  <div class="tab-pane <?php if( empty( $descripcion) && empty( $equipacion) ){ ?>active<?php } ?>" id="informacion" role="tabpanel" aria-labelledby="informacion-tab">

        <?php echo do_shortcode( wpautop(get_post_meta($carId,'general_information',true)) );?>

  </div>
  <?php } ?>
  <div class="tab-pane" id="ubicacion" role="tabpanel" aria-labelledby="ubicacion-tab">
    <div class="acf-map-">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2954.847359446336!2d-8.704611584548232!3d42.217706879196676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd25882b185abd1f%3A0x217541b95a6fc3ec!2sPercar+%7C+Concesionario+Audi+y+Volkswagen!5e0!3m2!1ses!2ses!4v1519913721964" width="600" height="300" frameborder="0" style="border:0;width:100%" allowfullscreen></iframe>
    </div>
  </div>
</div>
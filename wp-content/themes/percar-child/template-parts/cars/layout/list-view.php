<div class="car-grid">
    <div class="row">
        <div <?php cardealer_list_view_class_1()?>>
            <div class="car-item gray-bg text-center">
                <div class="car-image">
                    <?php                        
                    $id = get_the_ID();
                    echo cardealer_get_cars_condition($id);
                    echo cardealer_get_cars_status($id);
                    echo cardealer_get_cars_image('car_catalog_image',$id);?>					
                    <div class="car-overlay-banner">
                        <ul> 
                            <?php
                            /**                            				 
                			 * car_overlay_banner hook.
                             * 
                			 * @hooked cardealer_view_cars_overlay_link - 10
                			 * @hooked cardealer_compare_cars_overlay_link - 20
                             * @hooked cardealer_images_cars_overlay_link - 30                            				 
                			 */
                            do_action( 'car_overlay_banner', $id  );?>                            
                        </ul>
                    </div>
                </div>                    
            </div>
        </div>
        <div <?php cardealer_list_view_class_2()?>>
            <div class="car-details">
                <div class="car-title">
                    <a href="<?php echo get_the_permalink()?>"><?php the_title();?></a>
                    <p><?php echo cardealer_car_short_content($id);?></p>
                </div>
                <?php cardealer_car_slide_price_html(get_the_ID());?>  
                <a class="button red pull-right" href="<?php echo get_the_permalink();?>"><?php esc_html_e('Details','cardealer')?></a>
                <?php cardealer_get_cars_list_attribute();?> 
            </div>
        </div>
    </div>
</div>
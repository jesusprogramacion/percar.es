<?php
add_action( 'wp_enqueue_scripts', 'cardealer_child_enqueue_styles', 11 );
function cardealer_child_enqueue_styles() {
	wp_enqueue_style( 'percar-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'percar-child-promotions', get_stylesheet_directory_uri() . '/style-promotions.css' );
	wp_enqueue_style( 'threesixty', get_stylesheet_directory_uri() . '/css/threesixty.css' );
	if(is_page('audi') || is_page('volkswagen') || is_page('vehiculos-comerciales')){
		wp_enqueue_style( 'slickcss', get_stylesheet_directory_uri() . '/css/slick.css' );
	}
	// wp_enqueue_style( 'prettyphoto-pr', plugin_dir_url() . '/js_composer/assets/lib/prettyphoto/css/prettyPhoto.min.css' );
	if(is_page('vehiculos-de-ocasion') || is_page('listado-coches') || is_page('detalles') || is_page('informacion-vehiculo')){
		//wp_enqueue_style( 'paaginate_css', get_stylesheet_directory_uri() . '/css/simplePagination.css' );
		wp_enqueue_style( 'slick-ocasion', get_stylesheet_directory_uri(). '/bower_components/slick-carousel/slick/slick.css' );
		wp_enqueue_style( 'slick-ocasion', get_stylesheet_directory_uri(). '/bower_components/slick-carousel/slick/slick-theme.css' );
	}
}


add_action('wp_enqueue_scripts', 'percar_js');
function percar_js() {




if(is_page('vehiculos-de-ocasion') || is_page('listado-coches') || is_page('detalles') || is_page('informacion-vehiculo') || is_front_page() || is_page('prueba') || is_single() || is_category() || is_page('vehiculos-favoritos') && ! is_admin()){

	// wp_enqueue_script( 'angular-core', get_stylesheet_directory_uri().'/bower_components/angular/angular.js', array(), '1.3.14', true );
	// wp_enqueue_script( 'angular-route', get_stylesheet_directory_uri().'/js/angular-route.min.js', array(), null, true );
   

	// wp_enqueue_script( 'angular-app', get_stylesheet_directory_uri().'/js/angular/app.js', array('angular-core'), null, true );
	// wp_enqueue_script( 'angular-controllers', get_stylesheet_directory_uri().'/js/angular/controllers.js', array(), null, true );
	// wp_enqueue_script( 'angular-filters', get_stylesheet_directory_uri().'/js/angular/filters.js', array(), null, true );
	// wp_enqueue_script( 'angular-directives', get_stylesheet_directory_uri().'/js/angular/directives.js', array(), null, true );
	// wp_enqueue_script( 'angular-services', get_stylesheet_directory_uri().'/js/angular/services.js', array(), null, true );	
	// wp_enqueue_script( 'angular-slick', get_stylesheet_directory_uri().'/bower_components/angular-slick/dist/slick.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-slick-carousel', get_stylesheet_directory_uri().'/bower_components/slick-carousel/slick/slick.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-ui-niceselect', get_stylesheet_directory_uri().'/js/ui-niceselect.min.js', array(), null, true );


	
	// wp_enqueue_script( 'angular-scrolls-malihu', get_stylesheet_directory_uri().'/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-scrolls', get_stylesheet_directory_uri().'/bower_components/ng-scrollbars/dist/scrollbars.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-ui-bootstrap', get_stylesheet_directory_uri().'/js/ui-bootstrap.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-resource', get_stylesheet_directory_uri().'/js/angular-resource.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-sanitize', get_stylesheet_directory_uri().'/js/angular-sanitize.min.js', array(), null, true );
	// wp_enqueue_script( 'angular-sanitize-map', get_stylesheet_directory_uri().'/js/angular-sanitize.min.js.map.js', array(), null, true );
	
	// wp_enqueue_script( 'owl-viewer', get_stylesheet_directory_uri().'/js/angular-owl-carousel.js', array(), null, true );


	wp_enqueue_script( 'uglify', get_stylesheet_directory_uri(). '/js/uglify-ocasion.min.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'jpages-pagination', get_stylesheet_directory_uri().'/js/jPages.min.js', array(), null, true );


}


	if(is_page('detalles') || is_singular('cars') ){

		wp_enqueue_script( 'virtual-tour-virtual', get_stylesheet_directory_uri().'/js/virtual-tour/virtual-tour.min.js', array(), null, true );
		wp_enqueue_script( 'threesixty-viewer', get_stylesheet_directory_uri().'/js/threesixty.min.js', array(), null, true );
		wp_enqueue_script( 'script_detalles', get_stylesheet_directory_uri(). '/js/script-details.js', array( 'jquery' ), '1.0', true );

	}

   	//wp_enqueue_script( 'ui.mobile.drag', '/js/jquery.ui.touch-punch.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'script', get_stylesheet_directory_uri(). '/js/script.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'form', get_stylesheet_directory_uri().'/js/form.js', array( 'jquery' ), '1.0', true );

    if(is_page('informacion-vehiculo')){
    	  wp_enqueue_script( 'script-info', get_stylesheet_directory_uri(). '/js/script-info.js', array( 'jquery' ), '1.0', true );
    }

}



 //  Los archivos modificados los plugins vienen reflejados en readme.html

 function disable_ulike_updates( $value ) { 
 	unset( $value->response['wp-ulike/wp-ulike.php'] ); 
 	return $value; 
 } 
 add_filter( 'site_transient_update_plugins', 'disable_ulike_updates' ); 

 function disable_ultimate_member_updates( $value ) { 
 	unset( $value->response['ultimate-member/index.php'] ); 
 	return $value; 
 } 
 add_filter( 'site_transient_update_plugins', 'disable_ultimate_member_updates' ); 

 function disable_cardealer_updates( $value ) { 
 	unset( $value->response['cardealer-helper-library/cardealer-helper-library.php'] ); 
 	return $value; 
 } 
 add_filter( 'site_transient_update_plugins', 'disable_cardealer_updates' ); 


 //  ** Los archivos modificados los plugins vienen reflejados en readme.html
 



add_filter('wp_ulike_count_box_template', 'wp_ulike_change_my_count_box_template', 10, 2);
function wp_ulike_change_my_count_box_template($string, $counter) {
	$num = preg_replace("/[^0-9,.]/", "", $counter);
	if($num == 0) return;
	else return $string;
}

function cardealer_left_menu(){
		// Fetch menu details and check whether mega menu data is found or not.
		$menu_args = array(
			'menu_id'       => 'left-main',
			'menu_class'    => 'menu-left-links',
			'container'     => 'ul',
		);		
		wp_nav_menu( $menu_args );
}

function my_custom_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/login-personalizado.css" />';
}
add_action('login_head', 'my_custom_login');

function modificarLogoInicio() {
	echo '<style type="text/css">
		.login h1 a{
		background-image:url('.get_bloginfo('stylesheet_directory').'/login/logo-alola.png) !important;
		width: auto !important;
        background-size: auto !important;
		}

	</style>';
}
add_action('login_head', 'modificarLogoInicio');

function modificarUrlLogo() {
    return 'http://www.alola.es/';
}
add_filter( 'login_headerurl', 'modificarUrlLogo' );

function modificarTituloLogo() {
    return 'Alola Media | Marketing Digital';
}
add_filter( 'login_headertitle', 'modificarTituloLogo' );

function gmap(){
    if (is_page( 'contacto' )){
    	wp_register_script('scriptmap', get_stylesheet_directory_uri(). '/js/mapa.js', array('jquery'), '1', true );
    	wp_enqueue_script('scriptmap');
    }
    
}
add_action('wp_enqueue_scripts', 'gmap');

function add_scripts_maps() {
  wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBxxepVEMDv88mR9SJoEL_n6jCcvaxxE5A');
  wp_enqueue_script('google-jsapi','https://www.google.com/jsapi');     
}
add_action('wp_enqueue_scripts', 'add_scripts_maps');





// function test_ajax_load_scripts() {
 
// 	// make the ajaxurl var available to the above script
// 	wp_localize_script( 'ajax-test', 'the_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
// }
// add_action('wp_print_scripts', 'test_ajax_load_scripts');

// if ( is_admin() ) {
//     add_action( 'wp_ajax_my_frontend_action', 'my_frontend_action_callback' );
//     add_action( 'wp_ajax_nopriv_my_frontend_action', 'my_frontend_action_callback' );
//     add_action( 'wp_ajax_my_backend_action', 'my_backend_action_callback' );
//     // Add other back-end action hooks here
// }


// add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

function pr_search(){// $atts

// $p = shortcode_atts( array (
//   'nombre' => 'Invitado',
//   'color' => 'blue'
//   ), $atts );

$html ='<div class="filter-single-cars-ocasion">';

    $html .='<div id="ocasion" ng-controller="searcherCtrl">';
    	$html .='<div >';
     
	            $html .= '<div class="section-title text-center">';
	                // $html .= '<span></span>';
	                $html .='<h2 class="titulo-formulario" ng-bind-html="content"></h2>';
	            $html .='</div>';
                $html .='<div >';
                		   $html .='<div class="row">';

	                            $html .='<div class="search-block select-type-vehicle wrapper-select col-xs-12 col-md-3" style="z-index:1"><select class="form-control selectpicker custom-filters-box" id="type-vehicle" name="type-vehicle"  ng-model="selectedtype"  ng-change="onTypeVehiclesChange(selectedtype)">';
	                                            $html .='<option value="" selected>Tipo de vehículo</option>';
	                                            $html .='<option value="km0">km 0</option>';
	                                             $html .='<option value="ocasion">Ocasión</option>';
	                            $html .='</select></div>';

                        $html .='<form id="searcher-form" class="form-horizontal search-block content form-vehiculos-ocasion" role="form" action="'. WP_HOME .'/vehiculos-de-ocasion/">';
                         

	                                $html .='<div class="wrapper-select col-xs-12 col-md-3">';
	                                            $html .='<select class="form-control selectpicker custom-filters-box" id="brand-select" name="brand-select" ng-model="selectedBrand" ng-options="brand.marca for brand in brands track by brand.marca" ng-change="onBrandchange(selectedBrand)" disabled>';
	                                            $html .='<option value="">Selecciona Marca</option>';
	                                            $html .='</select>';
	                                  
	                                $html .='</div>';

	                                $html .='<div class="wrapper-select col-xs-12 col-md-3">';
	                                    
	                                        $html .='<select class="form-control selectpicker custom-filters-box" id="model-select" name="model-select" ng-model="selectedModel" ng-disabled="!models" ng-options="model.gama for model in models track by model.gama" ng-change="onModelchange(selectedModel, {{ selectedBrand }} )" disabled>';
	                                            $html .='<option value="">Selecciona Modelo</option>';
	                                        $html .='</select>';
	                                   

	                                $html .='</div>';
								
                            
	                            $html .='<div class="both col-md-3 col-sx-12">';
		                               
		                                    $html .='<input class="btn btn-md btn-primary btn-search" type="submit" value="Ver coches" disabled >';
		                            
		                        $html .='</div>';
	                      
                        $html .='</form>';


                        $html .='<div id="searcher-form"  class="form-vehiculos-km0 search-block selected-box" style="display:none">';
                         
	                                $html .='<div class="wrapper-select col-xs-12 col-md-3">';
	                                            $html .='<select data-uid="5aafb20783fef" id="sort_car_make_5aafb20783fef" data-id="car_make" class="selectpicker custom-filters-box col-4" name="marca" style="display: none;">';
	                                            $html .='<option value="">Selecciona Marca</option>';
	                                            $html .='</select>';
	                                  
	                                $html .='</div>';

	                                $html .='<div class="wrapper-select col-xs-12 col-md-3">';
	                                    
	                                        $html .='<select data-uid="5aafb20783fef" id="sort_car_model_5aafb20783fef" data-id="car_model" class="selectpicker custom-filters-box" name="modelo" style="display: none;">';
	                                            $html .='<option value="">Selecciona Modelo</option>';
	                                        $html .='</select>';
	                                   		// límite de preciosprecio para búsqueda
					 						$html .='<div class="price">';	
												 $html .='<div class="display:none !importnat">';
													
														 $html .='<input id="pgs_min_price" name="min_price" value="" data-min="" type="hidden">';
														 $html .='<input id="pgs_max_price" name="max_price" value="" data-max="" type="hidden">';
												 $html .='</div>';					
											$html .='</div>';				

	                                $html .='</div>';
								
                            
	                            $html .='<div class="both col-md-3 col-sx-12">';
		                               
		                                    $html .='<a class="button cfb-submit-btn" href="javascript:void(0);" >Ver coches</a>';
		                            
		                        $html .='</div>';
	                      
                        $html .='</div>';


                     $html .='</div>';
                $html .='</div>';
	$html .='</div>';
$html .='</div>';

$html .='</div>';


return $html;

}
add_shortcode('formularios-busqueda','pr_search');



function pr_search_sidebar(){
$html ='<div class="filter-single-cars-ocasion  filter-single-cars-ocasion-sidebar"">';
    $html .='<div id="ocasion" ng-controller="searcherCtrl">';
    	$html .='<div>';
                $html .='<div>';
                	$html .='<div class="row">';
	                            $html .='<div class="search-block select-type-vehicle wrapper-select col-xs-12 col-md-12" style="z-index:1"><select class="form-control selectpicker custom-filters-box" id="type-vehicle" name="type-vehicle"  ng-model="selectedtype"  ng-change="onTypeVehiclesChange(selectedtype)">';
	                                            $html .='<option value="" selected>Tipo de vehículo</option>';
	                                            $html .='<option value="km0">km 0</option>';
	                                             $html .='<option value="ocasion">Ocasión</option>';
	                            $html .='</select></div>';
                        $html .='<form id="searcher-form" class="form-horizontal search-block content form-vehiculos-ocasion" role="form" action="'. WP_HOME .'/vehiculos-de-ocasion/">';
	                                $html .='<div class="wrapper-select col-xs-12 col-md-12">';
	                                            $html .='<select class="form-control selectpicker custom-filters-box" id="brand-select" name="brand-select" ng-model="selectedBrand" ng-options="brand.marca for brand in brands track by brand.marca" ng-change="onBrandchange(selectedBrand)" disabled>';
	                                            $html .='<option value="">Selecciona Marca</option>';
	                                            $html .='</select>';     
	                                $html .='</div>';
	                                $html .='<div class="wrapper-select col-xs-12 col-md-12">';
	                                        $html .='<select class="form-control selectpicker custom-filters-box" id="model-select" name="model-select" ng-model="selectedModel" ng-disabled="!models" ng-options="model.gama for model in models track by model.gama" ng-change="onModelchange(selectedModel, {{ selectedBrand }} )" disabled>';
	                                            $html .='<option value="">Selecciona Modelo</option>';
	                                        $html .='</select>';
	                                $html .='</div>';
	                            $html .='<div class="both col-md-12 col-sx-12">';  
		                                    $html .='<input class="btn btn-md btn-primary btn-search" type="submit" value="Ver coches" disabled >';
		                        $html .='</div>';
                        $html .='</form>';
                        $html .='<div id="searcher-form"  class="form-vehiculos-km0 search-block selected-box" style="display:none">';
	                                $html .='<div class="wrapper-select col-xs-12 col-md-12">';
	                                            $html .='<select data-uid="5aafb20783fef" id="sort_car_make_5aafb20783fef" data-id="car_make" class="selectpicker custom-filters-box col-4" name="marca" style="display: none;">';
	                                            $html .='<option value="">Selecciona Marca</option>';
	                                            $html .='</select>';
	                                $html .='</div>';
	                                $html .='<div class="wrapper-select col-xs-12 col-md-12">';
	                                        $html .='<select data-uid="5aafb20783fef" id="sort_car_model_5aafb20783fef" data-id="car_model" class="selectpicker custom-filters-box" name="modelo" style="display: none;">';
	                                            $html .='<option value="">Selecciona Modelo</option>';
	                                        $html .='</select>';
	                                   		// límite de precios para búsqueda
					 						$html .='<div class="price">';	
												 $html .='<div class="display:none !importnat">';
														 $html .='<input id="pgs_min_price" name="min_price" value="" data-min="" type="hidden">';
														 $html .='<input id="pgs_max_price" name="max_price" value="" data-max="" type="hidden">';
												 $html .='</div>';					
											$html .='</div>';				
	                                $html .='</div>';
	                            $html .='<div class="both col-md-12 col-sx-12">';
		                            $html .='<a class="button cfb-submit-btn" href="javascript:void(0);" >Ver coches</a>';
		                        $html .='</div>';
                        $html .='</div>';
                     $html .='</div>';
                $html .='</div>';
	$html .='</div>';
$html .='</div>';
$html .='</div>';


return $html;

}
add_shortcode('formularios-busqueda-sidebar','pr_search_sidebar');



function pr_proteccion_datos($atts){

	$p = shortcode_atts( array (
	  'color' => 'black'
	 	), $atts );


	$html ='<div class="proteccion_datos_form ' . esc_attr($p['color']) . '">';
	$html .='<p id="titulo">INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS</p>';
	$html .='<p><span>Responsable:</span> Pérez Rumbao Car S.L.<br>';
	$html .='<span>Finalidad:</span> Brindarte la mejor experiencia posible cuando adquieras nuestros productos y utilices nuestros servicios.<br>';
	$html .='<span>Destinatarios:</span> Los datos podrán ser cedidos a Volkswagen Group España Distribución S.A. y a otras empresas de Pérez Rumbao, S.A.U.<br>';
	$html .='<span>Plazo de conservación de los datos:</span> Hasta que no solicites su supresión.<br>';
	$html .='<span>Derechos:</span>&nbsp;Acceder, rectificar y suprimir los datos, así como otros derechos recogidos en nuestra <a href="https://www.percar.es/aviso-legal" title="Política de privacidad" target="_blank">política de privacidad</a>.';
	$html .='</p>';
	$html .='</div>';


	return $html;
}
add_shortcode('proteccion_datos','pr_proteccion_datos');


add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements' );

function mycustom_wpcf7_form_elements( $form ) {
$form = do_shortcode( $form );

return $form;
}

$acceptance = '<div class="proteccion_datos_form"><p id="titulo">INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS</p>
<p><span>Responsable:</span> Pérez Rumbao Car S.L.<br><span>Finalidad:</span> Brindarte la mejor experiencia posible cuando adquieras nuestros productos y utilices nuestros servicios.<br><span>Destinatarios:</span> Los datos podrán ser cedidos a Volkswagen Group España Distribución S.A. y a otras empresas de Pérez Rumbao, S.A.U.<br><span>Plazo de conservación:</span> Hasta que no solicites su supresión.<br><span>Derechos:</span> Acceder, rectificar y suprimir los datos, así como otros derechos recogidos en nuestra <a href="https://www.percar.es/aviso-legal" title="Política de privacidad" target="_blank">política de privacidad</a>.</p></div>';

function ft_acceptance_comments_logged_in( $form ) {
	global $acceptance;
	
	return $form . $acceptance;
}

function ft_acceptance_comments_logged_out() {
	global $acceptance;

	echo $acceptance;
}

if ( is_user_logged_in() ) {
	add_action( 'comment_form_field_comment', 'ft_acceptance_comments_logged_in' );
} else {
	add_action( 'comment_form_after_fields', 'ft_acceptance_comments_logged_out' );
}

add_filter( 'comment_form_field_comment', 'privacidad_en_comentarios' );
function privacidad_en_comentarios( $comment_field ) {
    return $comment_field.'<p class="privacidad"><input type="checkbox" name="privacidad" value="Acepto la política de privacidad aria-req="true">&nbsp;&nbsp;Acepto la <a href="https://www.percar.es/aviso-legal" title="Política de privacidad" target="_blank">política de privacidad</a>.<p>';
}

add_filter( 'preprocess_comment', 'validar_privacidad_comentarios' );
function validar_privacidad_comentarios( $commentdata ) {
    if ( ! isset( $_POST['privacidad'] ) )
        wp_die( __( 'Error: Debes aceptar nuestra política de privacidad marcando la casilla correspondiente. Vuelve a intentarlo.' ) );
    return $commentdata;
}

add_action( 'comment_post', 'guardar_privacidad' );
function guardar_privacidad( $comment_id ) {
    add_comment_meta( $comment_id, 'privacidad', $_POST[ 'privacidad' ] );
}

/* Aviso comentarios */

function email_aviso_comentarios($wp_customize) {
$section = 'email_aviso_comentarios';
$wp_customize->add_section( $section, array(
'title' => 'Aviso de comentarios',
'description' => 'Emails donde recibir aviso de nuevos comentarios',
'priority' => 125,
) );
$name = "email-aviso-comentarios";
$wp_customize->add_setting( $name, array(
'default' => '',
) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, $name, array(
'label' => 'Aviso de comentarios',
'description' => 'Un email por línea',
'section' => $section,
'settings' => $name,
'type' => 'textarea'
) ) );
}
add_action( 'customize_register', 'email_aviso_comentarios' );

function se_comment_moderation_recipients( $emails, $comment_id ) {
$emails = explode(PHP_EOL, get_theme_mod( 'email-aviso-comentarios' ) );
return $emails;
}
add_filter( 'comment_moderation_recipients', 'se_comment_moderation_recipients', 11, 2 );
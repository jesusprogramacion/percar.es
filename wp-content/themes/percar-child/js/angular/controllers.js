angular.module('SearcherApp.controllers', [])

.constant("CONFIG", {
    "URL_BASE": "http://www.sibuscascoche.com",
    "URL_LOCAL": "http://dev.sibuscascoche.com",
    "URL_SECURE": "https://sbc.aloladev.com",
    "SLICK_SETTINGS": [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 481,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 320,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
})


.controller('searcherCtrl', ['$scope', '$http', '$window',  'CONFIG', function($scope, $http, $window,  CONFIG) {
    
    var modelo = angular.element(document).find('#model-select');
    var marca = angular.element(document).find('#brand-select');
    var fuel = angular.element(document).find('#fuel-select');
    var cambio = angular.element(document).find('#change-select');
    //var selected = location.search.split('brand-select=')[1] ? location.search.split('brand-select=')[1] : '';

    $http.jsonp( CONFIG.URL_SECURE + '/api-brands/percar-brands?callback=JSON_CALLBACK').success(function(data) {

        $scope.brands = data.brands;
        $scope.bodycars = data.bodycars;

    });

    var form_ocasion = angular.element(document).find('.form-vehiculos-ocasion');
    var form_km0 = angular.element(document).find('.form-vehiculos-km0');
    var type = angular.element(document).find('#type-vehicle');
    var titulo = angular.element(document).find('h2.titulo-formulario');

    $scope.content = "¿Buscas un coche? ¡Eso es lo nuestro, deja que te ayudemos!";
    $scope.onTypeVehiclesChange = function(selectedtype) {

        if(selectedtype == 'km0'){
            form_ocasion.hide();
            form_km0.show();
            $scope.content = "¿Buscas un coche <span>de km 0</span>? ¡Eso es lo nuestro, deja que te ayudemos!";
        }else if (selectedtype == 'ocasion'){
            angular.element(document).find('.btn-search').removeAttr('disabled');
            form_km0.hide();
            form_ocasion.show();
            $scope.content = '¿Buscas un coche <span>de ocasión</span>? ¡Eso es lo nuestro, deja que te ayudemos!'; 
            modelo.niceSelect('update');
            marca.niceSelect('update');
           

        }

    };

    $scope.onBrandchange = function(selectedBrand) {

        if(selectedBrand != null) {

            $http.jsonp( CONFIG.URL_SECURE + '/api-models-bodycars-percar/' + selectedBrand.marca + '?callback=JSON_CALLBACK').success(function(data) {
                modelo.attr('disabled', false);

                $scope.selectedModel = '';
                $scope.models = data.models;

                setTimeout(function() { modelo.niceSelect('update') , 500});

            });
        }
    };

    $scope.onModelchange = function(selectedModel, selectedBrand) {

        // if(selectedModel != null) {

        //     $http.jsonp( CONFIG.URL_BASE + '/api-bodycars/percar/' + selectedModel.gama + '?callback=JSON_CALLBACK').success(function(data) {
        //         $scope.bodycars = data.bodycars;
        //     });
        // }
        // else {
        //     $http.jsonp( CONFIG.URL_LOCAL + '/api-models-bodycars-percar/' + selectedBrand.marca + '?callback=JSON_CALLBACK').success(function(data) {
        //         $scope.bodycars = data.bodycars;
        //     });
        // }
         
    };

    $scope.onModelinit = function(brandSelect, modelSelect, selectedModel) {
        if(brandSelect != null) {
            //alert(replaceAll( $scope.formData.split("&")[1].split('=')[1], '+', ' '));
            // $scope.urlBrand = replaceAll( $scope.formData.split("&")[0].split('=')[1], '+', ' ');
            // $scope.urlModel = replaceAll( $scope.formData.split("&")[1].split('=')[1], '+', ' ');
            $http.jsonp( CONFIG.URL_SECURE + '/api-models-bodycars-percar/' + brandSelect + '?callback=JSON_CALLBACK').success(function(data) {
                $scope.models = data.models;
                // modelo.niceSelect('update');
                // marca.niceSelect('update');
                // $scope.$watch(function(){
                //     $scope.selectedModel = replaceAll( $scope.formData.split("&")[1].split('=')[1], '+', ' ');
                // });
                // modelo.niceSelect('update');
            });
        } 
    };
    $scope.onFuelchange = function(selectedFuel) {

        $scope.fuels = ['Gasolina', 'Diésel'];
        $scope.selectFuel = selectedFuel; 

    };
    $scope.onCambiochange = function(selectedCambio) {
        
        $scope.cambio = ['Manual', 'Automático'];
        $scope.selectCambio = selectedCambio; 


    };

    $scope.$watch(function(){
        modelo.niceSelect('update');
        marca.niceSelect('update');
        fuel.niceSelect('update');
        cambio.niceSelect('update');
     });

    $window.onload = function() {
        modelo.niceSelect('update');
        marca.niceSelect('update');
        fuel.niceSelect('update');
        cambio.niceSelect('update');
    };


}])

.controller('ocasionLikes', ['$scope', '$rootScope', '$http', 'CONFIG', function($scope, $rootScope, $http, CONFIG) {

    $scope.vehiclesLikes = [];
    var vehiclesL =[];

    $scope.listLikes = function($likes, $subempresas, $user){

            var likes = $likes,
                subempresas = $subempresas;

            $http.jsonp( CONFIG.URL_SECURE + '/api-list/vehicles-likes/' + likes + '/' + subempresas + '?callback=JSON_CALLBACK').success(function(data) {

                $scope.vehiclesLikes = data.vehicles;
                vehiclesL = data.vehicles;

                $http.post('https://percar.aloladev.com/wp-content/themes/percar-child/templates/partials-ocasion/vehicles-list-ocasion.php',
                    {vehiclesL:vehiclesL,likes:likes,subempresas:subempresas}).success(function(data) {
 
                    $scope.likesContent = data;
                    setTimeout(function() { $('[data-toggle="tooltip"]').tooltip() , 2000});
                });



            });

    }
}])

.controller('vehiclesListSliderCtrl', ['$scope', '$http', 'CONFIG', 'byCardealership', function($scope, $http, CONFIG, byCardealership) {

    $scope.config = {
        autoHideScrollbar: false,
        theme: 'dark',
        axis: 'y'
    };

    $scope.vehiclesInOffering = [];

    $scope.vehiclesSent = [];

    $scope.responsive = [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 481,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ];

    $http.jsonp( CONFIG.URL_SECURE+ '/api-sliders/percar/list?callback=JSON_CALLBACK').success(function(data) {

        $scope.vehiclesInOffering = byCardealership( data.vehiclesInOffering, 'PERCAR' );

        $scope.vehiclesSent = byCardealership( data.vehiclesSent, 'PERCAR' );
    });


}])


.controller('vehiclesRelatedSliderCtrl', ['$scope', '$http', '$window', 'CONFIG', function($scope, $http, $window, CONFIG) {

    $scope.config = {
        autoHideScrollbar: false,
        theme: 'dark',
        axis: 'y'
    };

    $scope.vehiclesRelated = [];


    $scope.responsive = [
        {
            breakpoint: 992,
           // autoWidth:true,
          
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 481,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ];

    var vehiclesList =[];
    $scope.vehiclesRelated = function($data, $likes, $id_vehiculo, $subempresas, $user, $subempresa){

        var likes = $likes,
            subempresas = $subempresas;
            user  = $user;

        $http.jsonp( CONFIG.URL_SECURE + '/api-related-percar/'+ $data +'/'+ $id_vehiculo +'/'+ $subempresa +'?callback=JSON_CALLBACK').success(function(data) {

            $scope.vehiclesRelated = data.vehicles;
            vehiclesList = data.vehicles;

            $http.post('https://percar.aloladev.com/wp-content/themes/percar-child/templates/partials-ocasion/vehicles-slide-ocasion.php',
                {vehiclesL:vehiclesList,likes:likes,subempresas:subempresas,user:user}).success(function(data) {
                $scope.listContent = data;

                function initCarousel(){ 


                    var carousel_ocasion = angular.element('.pgs_cars_carousel-items-ocasion').find('.owl-stage-outer .owl-stage');
                    carousel_ocasion.append($scope.listContent);   

                    owl = jQuery(".pgs_cars_carousel-items-ocasion");
                    owl.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                    owl.find('.owl-stage-outer').children().unwrap();
                    
                    owl.each(function () {
                        var $this = jQuery(this),
                        $items = 1,
                        $loop = true,
                        $navdots = true,
                        $navarrow = true,
                        $autoplay =  true,
                        $space = 30;     
                        jQuery(this).owlCarousel({
                            loop: $loop,
                            items: $items,
                            responsive: {
                                0:{items: 1},
                                480:{items: 1},
                                768:{items: 3},
                                980:{items: 3},
                                1200:{items: 4}
                            },
                            dots: $navdots,
                            margin:$space,
                            nav: $navarrow,
                            navText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"],
                            autoplay: $autoplay,
                            autoplayHoverPause: true,
                        }); 
                           
                    });


                    $('[data-toggle="tooltip"]').tooltip();
                }

                setTimeout(function() { initCarousel() , 1500});

                    $scope.$watch('listContent',function(){
                        initCarousel();
                    });
 
            });

        });

    }

}])

.controller('vehiclesHomeSliderCtrl', ['$scope', '$http', 'CONFIG', function($scope, $http, CONFIG) {

    $scope.config = {
        autoHideScrollbar: false,
        theme: 'dark',
        axis: 'y'
    };

    $scope.vehiclesRelated = [];


    $scope.responsive = [
        {
            breakpoint: 992,
            autoWidth:true,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 481,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ];

    var vehiclesList =[];
    $scope.vehiclesSlideHome = function($likes,$subempresas,$user){
        var likes       = $likes,
            subempresas = $subempresas,
            user        = $user;
  
        $http.jsonp( CONFIG.URL_SECURE + '/api-slide-percar?callback=JSON_CALLBACK').success(function(data) {

            $scope.vehiclesRelated = data.vehicles;
            vehiclesList = data.vehicles;

            $http.post('https://percar.aloladev.com/wp-content/themes/percar-child/templates/partials-ocasion/vehicles-slide-ocasion.php',
                {vehiclesL:vehiclesList,likes:likes,subempresas:subempresas,user:user}).success(function(data) {
                $scope.listSlideContent = data;


                var carousel_ocasion = angular.element('.pgs_cars_carousel-items').find('.owl-stage-outer .owl-stage > div.owl-item:last-child');
                carousel_ocasion.after($scope.listSlideContent);   


                $('.pgs_cars_carousel-items').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                $('.pgs_cars_carousel-items').find('.owl-stage-outer').children().unwrap();

                function initCarousel(){ 
                    owl = jQuery(".pgs_cars_carousel-items");
                    owl.each(function () {
                        var $this = jQuery(this),
                        $items = 1,
                        $loop = true,
                        $navdots = true,
                        $navarrow = true,
                        $autoplay =  true,
                        $space = 30;     
                        jQuery(this).owlCarousel({
                            loop: $loop,
                            items: $items,
                            responsive: {
                                0:{items: 1},
                                480:{items: 1},
                                768:{items: 3},
                                980:{items: 3},
                                1200:{items: 4}
                            },
                            dots: $navdots,
                            margin:$space,
                            nav: $navarrow,
                            navText:["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"],
                            autoplay: $autoplay,
                            autoplayHoverPause: true,
                        }); 
                           
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                }

                setTimeout(function() { initCarousel() , 1500});

                    $scope.$watch(function(){
                        initCarousel();
                    });
 
            });

    });

    }

}])

.controller('vehiclesListController', ['$scope', '$rootScope', '$filter', '$http', '$location', '$window','CONFIG', 'replaceAll', 'orderByCardealership', 'byCardealership', function($scope, $rootScope, $filter, $http, $location, $window, CONFIG, replaceAll, orderByCardealership, byCardealership) {

    var marca = angular.element(document).find('#brand-select');
    var modelo = angular.element(document).find('#model-select');
    var listado = angular.element(document).find('#listado-vehiculos-ocasion');

    $scope.config = {
        autoHideScrollbar: false,
        theme: 'dark',
        axis: 'y'
    };

    $scope.formData = $location.absUrl().split('?').pop();
    // $scope.formData = $scope.formData.split('&').shift();
    // if($scope.formData = ''){
    //     $scope.formData = 'brand-select=&fuel-select=&change-select=&sincePrice-select=0&untilPrice-select=100000&bodycar-select=&sinceYear-select=1900&untilYear-select=3000';
    // }
	
    $scope.firstList = [];
    $scope.vehicles = [];


    // $scope.urlBrand = replaceAll( $scope.formData.split("&")[0].split('=')[1], '+', ' ');
    // $scope.urlModel = replaceAll( $scope.formData.split("&")[1].split('=')[1], '+', ' ');
    // if(!$scope.urlBrand || )  $scope.formData = $scope.formData+'&brand-select=';
    $scope.formData = $scope.formData+'&orderby=';

    var vehiclesL = [];

        $scope.likesInit = function($likes, $subempresas,$user){

                var likes       = $likes,
                    subempresas = $subempresas,
                    user        = $user;

                    $scope.user = user;
                    $scope.subempresas = subempresas;
                $http.jsonp( CONFIG.URL_SECURE+ '/api-list-percar/vehicles-list?' + $scope.formData +  '&callback=JSON_CALLBACK').success(function(data) {

                    $scope.vehiclesLength =   data.vehicles.length;
                    vehiclesL = data.vehicles;
                    $scope.vl = data.vehicles;

                    $http.post('https://percar.aloladev.com/wp-content/themes/percar-child/templates/partials-ocasion/vehicles-list-ocasion.php',
                        {vehiclesL:vehiclesL,likes:likes,subempresas:subempresas,user:user}).success(function(data) {

                           if(data){
                                $scope.ListContent = data;
                                $scope.resultados = true;
                            }else{
                                $scope.ListContent = '<div class="alert alert-warning">No se encontraron resultados que coincidan con su selección.</div>';
                                $scope.resultados = false;
                            }
                            
                        setTimeout(function() { listInit(), 1000});

                    });

                });

                 function listInit(){
                            $('[data-toggle="tooltip"]').tooltip();
                            $scope.paginate('undefined');
                }

        }

       $scope.paginate = function($itemsPage){
                        if($itemsPage === 'undefined'){
                            $(".paginate-ocasion").jPages({
                                      containerID : "listado-vehiculos-ocasion",
                                      perPage : 12
                            });
                        }else{
                            $(".paginate-ocasion").jPages("destroy").jPages({
                                            containerID   : "listado-vehiculos-ocasion",
                                            perPage       : parseInt($itemsPage)
                            });
                        }

                        $("#pgs_cars_pp-ocasion").change(function(){
                            var newPerPage = parseInt( $(this).val() );
                            if(isNaN(newPerPage)) { newPerPage = 12; }

                            $(".paginate-ocasion").jPages("destroy").jPages({
                                            containerID   : "listado-vehiculos-ocasion",
                                            perPage       : newPerPage
                            });            
                        });

        }

    $scope.onFilterchange = function(selectedFilter, $likes) {

            var fil;

            if(selectedFilter == 'pvpOfertaWeb'){
                fil = $scope.vl.sort(function(a,b){return new Date(a.pvpOfertaWeb) - new Date(b.pvpOfertaWeb);});
            }else if(selectedFilter == 'marca'){
                fil = $filter('orderBy')( $scope.vl, 'marca');
            }else if(selectedFilter == 'fechaMatriculacion'){
                fil = $scope.vl.sort(function(a,b){return new Date(a.fechaMatriculacion) - new Date(b.fechaMatriculacion);});
            }else{
                fil = $scope.vl;
            }

            $scope.formData = $scope.formData+'&orderby='+selectedFilter;

            $scope.vehiclesLikes = [];
            var vehiclesL = [];

            $http.post('https://percar.aloladev.com/wp-content/themes/percar-child/templates/partials-ocasion/vehicles-list-ocasion.php',
                {vehiclesL:fil,likes:$likes,subempresas:$scope.subempresas,user:$scope.user}).success(function(data) {

                $scope.ListContent = data;
                var nPerPage = parseInt( $("#pgs_cars_pp-ocasion option:selected").val() );

                if(isNaN(nPerPage)) {nPerPage = 12;}

                setTimeout(function() { $scope.paginate(nPerPage), 1000});
            });
    }

}])

.controller('brandCtrl', ['$scope', '$http', '$location', 'CONFIG', function($scope, $http, $location, CONFIG) {

    $scope.brand = $location.absUrl().split('/')[3];

    $scope.formData = 'brand=' + $scope.brand.toUpperCase() + '&cardealership=PERCAR';

    $scope.vehicles = [];


    $scope.responsive = [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 481,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ];

    $http.jsonp( CONFIG.URL_SECURE + '/api-list/by-brand?' + $scope.formData + '&callback=JSON_CALLBACK' ).success(function(data) {

        $scope.vehicles = data.vehicles;
    });
}])


// .controller('vehiclesDetailsController', ['$scope', '$http', '$location',  'CONFIG', function($scope, $http, $location,  CONFIG) {


//     $scope.formData = $location.absUrl().split('?').pop();

//     $scope.formData = $scope.formData.split('&').shift();


//     $scope.responsive = CONFIG.SLICK_SETTINGS;

//     $http.jsonp( CONFIG.URL_SECURE + '/api-details/percar/' + $scope.formData + '?callback=JSON_CALLBACK').success(function(data) {

//         $scope.vehicle          =   data.vehicle;
//         $scope.photoVehicleList =   data.photoVehicleList;
//         $scope.prevVehicle      =   data.prevVehicle;
//         $scope.nextVehicle      =   data.nextVehicle;
                  
//     });       
// }])

// .controller('vehiclesInOfferDetailsCtrl', ['$scope', '$http', '$location', 'CONFIG', function($scope, $http, $location, CONFIG) {

//     $scope.formData = $location.absUrl().split('?').pop();

//     $http.jsonp( CONFIG.URL_SECURE + '/api-details/percar/in-offer/' + $scope.formData + '?callback=JSON_CALLBACK').success(function(data) {
//         $scope.vehicle          =   data.vehicle;
//         $scope.photoVehicleList =   data.photoVehicleList;
//         $scope.prevVehicle      =   data.prevVehicle;
//         $scope.nextVehicle      =   data.nextVehicle;
        
//     }).error(function(data,status,headers,config){
    	
//     	swal('Error json al recuperar los datos de la base de datos');
    	
//     });

// }])

.controller('vehiclesDetailsController', ['$scope', '$http', '$location', 'CONFIG', function($scope, $http, $location, CONFIG) {

    var marca = angular.element(document).find('#brand-select');
    var modelo = angular.element(document).find('#model-select');

    //$scope.responsive = CONFIG.SLICK_SETTINGS;

    $scope.formData = $location.absUrl().split('?').pop();
    $scope.formData = $scope.formData.split('&').shift();

    $http.jsonp( CONFIG.URL_SECURE + '/api-details-percar/percar/' + $scope.formData + '?callback=JSON_CALLBACK').success(function(data) {
        $scope.vehicle          =   data.vehicle;
        $scope.photoVehicleList =   data.photoVehicleList;
        $scope.prevVehicle      =   data.prevVehicle;
        $scope.nextVehicle      =   data.nextVehicle;

        $scope.fotosGaleria = '';
        angular.forEach($scope.photoVehicleList, function(value, key) {
          $scope.fotosGaleria += 'http://www.sibuscascoche.com/img/coches/'+value;
          if(key < $scope.photoVehicleList.length-1){ $scope.fotosGaleria += ',';}
        });

    });

}])

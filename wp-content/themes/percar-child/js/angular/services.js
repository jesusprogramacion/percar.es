angular.module('SearcherApp.services', [])

.factory('urlParameters', function () {

    return {
        get: function ($parameter) {

            var urlPage         =   window.location.search.substring(1);
            var urlVariables    =   urlPage.split('&');

            for (var i = 0; i < urlVariables.length; i++) {
                var parameterName = urlVariables[i].split('=');

                if (parameterName[0] == $parameter) {
                    return parameterName[1];
                }
            }
        }
    }
})

.factory('replaceAll', function() {

    return function( text, busca, reemplaza ) {

        while (text.toString().indexOf(busca) != -1) {
            text = text.toString().replace(busca,reemplaza);
        }
        return text;
    }
})

.factory('orderByCardealership', function() {

    return function( firstList, cardealership ) {

        var finalList = [];

        angular.forEach( firstList, function(value, key) {

            if( value.empresa == cardealership ) {
                this.unshift(value);
            }
            else {
                this.push(value);
            }
        }, finalList);

        return finalList;
    }
})

.factory('byCardealership', function() {

    return function( firstList, cardealership ) {

        var finalList = [];

        angular.forEach( firstList, function(value, key) {

            if( value.empresa == cardealership ) {
                this.push(value);
            }

        }, finalList);

        return finalList;
    }
});
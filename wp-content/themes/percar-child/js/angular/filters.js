angular.module('SearcherApp.filters', [])

.filter('removeAccents', function(){

    return function (source) {
        if (!source || !source.length) { return; }
        var accent = [
                /[\300-\306]/g, /[\340-\346]/g, // A, a
                /[\310-\313]/g, /[\350-\353]/g, // E, e
                /[\314-\317]/g, /[\354-\357]/g, // I, i
                /[\322-\330]/g, /[\362-\370]/g, // O, o
                /[\331-\334]/g, /[\371-\374]/g, // U, u
                /[\321]/g, /[\361]/g, // N, n
                /[\307]/g, /[\347]/g // C, c
            ],
            noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];

        for (var i = 0; i < accent.length; i++) {
            source = source.replace(accent[i], noaccent[i]);
        }

        return source;
    };
})

.filter('split', function() {
     
    return function(input, splitChar, splitIndex) {
        if (!input || !input.length) { return; }
        return input.split(splitChar)[splitIndex];
    }
})

.filter('replaceSpaces', function () {

    return function( text, reemplaza ) {
        if (!text || !text.length) { return; }
        while (text.toString().indexOf(' ') != -1) {
            text = text.replace(/\s+/g,reemplaza);
        }
        return text;
    }
})

.filter('replace', function () {

    return function (text, search, value) {
        if (!text || !text.length) { return; }
        while (text.toString().indexOf(search) != -1) {
            text = text.toString().replace(search,value);
        }
        return text;
    };
})

.filter('cut', function () {

    return function (value, wordwise, max, tail) {

        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
})

.filter('nl2br', function() {

    return function (text) {
        if (!text || !text.length) { return; }
        var value = text.replace(/\n/g, ',');

        return value.split(',');
    }
})

.filter('ucfirst', function() {

    return function ucfirst (str) {
       if (!str || !str.length) { return; }
        var f = str.charAt(0).toUpperCase();

        return f + str.substr(1);
    }
})

.filter('convert', function() {
  return function(input) {
    return input.replace(/\B(?=(\d{3})+(?!\d))/g, ",");  
  }
})

.filter('to_trusted', ['$sce', function($sce) {
      return function(text) {
        return $sce.trustAsHtml(text);
      };
}]);

// .filter('trusted', ['$sce', function ($sce) {
//    return $sce.trustAsResourceUrl;
// }]);



jQuery(document).ready(function() {

    jQuery('.accordion > li > .accordion-title').click(function(){

        if(!jQuery(this).next().is(':visible')) {

            $('.accordion .wrapper-accordion-content').slideUp(300).css('padding', '15px 0 0');
        }

        jQuery(this).next().slideToggle(300).css('padding', '15px 0 0');
    });
});
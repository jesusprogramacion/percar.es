"use strict";
$(document).ready(function(){
var camera,
        scene,
        element = document.getElementById('panorama'), // Inject scene into this
        renderer,
        fullscreenMode= false,
        onPointerDownPointerX,
        onPointerDownPointerY,
        onPointerDownLon,
        onPointerDownLat,
        fov = 70, // Field of View
        isUserInteracting = false,
        lon = 0,
        lat = 0,
        phi = 0,
        theta = 0,
        onMouseDownMouseX = 0,
        onMouseDownMouseY = 0,
        onMouseDownLon = 0,
        onMouseDownLat = 0,
        width = 1440, // int || window.innerWidth
        height = 860, // int || window.innerHeight
        windowHeight,
        ratio = width / height,
        touchX, 
        touchY;

        var panoramaImage= document.getElementById('panoramaImage').src;

var texture = THREE.ImageUtils.loadTexture(panoramaImage, new THREE.UVMapping(), function() {
    init();
    animate();
});
function init() {
    camera = new THREE.PerspectiveCamera(fov, ratio, 1, 1000);
    scene = new THREE.Scene();
    var mesh = new THREE.Mesh(new THREE.SphereGeometry(500, 60, 40), new THREE.MeshBasicMaterial({map: texture}));
    mesh.scale.x = -1;
    scene.add(mesh);
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(width, height);
    element.appendChild(renderer.domElement);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
updateProjection();
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    element.addEventListener('mousedown', onDocumentMouseDown, false);
    element.addEventListener('mousewheel', onDocumentMouseWheel, false);
    element.addEventListener('DOMMouseScroll', onDocumentMouseWheel, false);
    window.addEventListener( 'resize', onWindowResize, false );
}


function updateProjection(){
        camera.projectionMatrix.makePerspective(fov, window.innerWidth / window.innerHeight, 1, 1100);
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function onWindowResized(event) {
    renderer.setSize(width, height);
    camera.projectionMatrix.makePerspective(fov, ratio, 1, 1100);
}

function onDocumentMouseDown(event) {
    event.preventDefault();
    onPointerDownPointerX = event.clientX;
    onPointerDownPointerY = event.clientY;
    onPointerDownLon = lon;
    onPointerDownLat = lat;
    element.addEventListener('mousemove', onDocumentMouseMove, false);
    element.addEventListener('mouseup', onDocumentMouseUp, false);
    /* TOUCHSCREENS */
    element.addEventListener('touchstart', onDocumentTouchStart, false);
    element.addEventListener( 'touchmove', onDocumentTouchMove, false );
}

            function onDocumentTouchMove( event ) {

                event.preventDefault();
                var touch = event.touches[ 0 ];
                lon -= ( touch.screenX - touchX ) * 0.2;
                lat -= ( touch.screenY - touchY ) * 0.2;
                touchX = touch.screenX;
                touchY = touch.screenY;
 }
            function onDocumentTouchStart( event ) {

                event.preventDefault();
                var touch = event.touches[ 0 ];
                touchX = touch.screenX;
                touchY = touch.screenY;

            }

function onDocumentMouseMove(event) {
    lon = (event.clientX - onPointerDownPointerX) * -0.175 + onPointerDownLon;
    lat = (event.clientY - onPointerDownPointerY) * -0.175 + onPointerDownLat;
}
function onDocumentMouseUp(event) {
    element.removeEventListener('mousemove', onDocumentMouseMove, false);
    element.removeEventListener('mouseup', onDocumentMouseUp, false);
}
function onDocumentMouseWheel(event) {
    // WebKit
    if (event.wheelDeltaY) {
        fov -= event.wheelDeltaY * 0.05;
        // Opera / Explorer 9
    } else if (event.wheelDelta) {
        fov -= event.wheelDelta * 0.05;
        // Firefox
    } else if (event.detail) {
        fov += event.detail * 1.0;
    }
    if (fov < 45 || fov > 90) {
        fov = (fov < 45) ? 45 : 90;
    }
        camera.projectionMatrix.makePerspective(fov, window.innerWidth / window.innerHeight, 1, 1100);

}
function animate() {
    requestAnimationFrame(animate);
    render();
}

Mousetrap.bind('right', function() {onKeyRight(); });
Mousetrap.bind('left', function() {onKeyLeft(); });
Mousetrap.bind('down', function() {onKeyDown(); });
Mousetrap.bind('up', function() {onKeyUp(); });

function onKeyRight(){
    var counter = 0;
    var tt=setInterval(function(){startTime()},10);
    function startTime() {
        lon+=0.25;
        if(counter == 40) {
            clearInterval(tt);
        } else {
            counter++;
        }
    }
}

function onKeyLeft(){
    var counter = 0;
    var tt=setInterval(function(){startTime()},10);
    function startTime() {
        lon-=0.25;
        if(counter == 40) {
            clearInterval(tt);
        } else {
            counter++;
        }
    }

}


function onKeyDown(){
    var counter = 0;
    var tt=setInterval(function(){startTime()},10);
    function startTime() {
        lat+=0.25;
        if(counter == 40) {
            clearInterval(tt);
        } else {
            counter++;
        }
    }
}


function onKeyUp(){
    var counter = 0;
    var tt=setInterval(function(){startTime()},10);
    function startTime() {
        lat-=0.25;
        if(counter == 40) {
            clearInterval(tt);
        } else {
            counter++;
        }
    }
}

function render() {

    if (isUserInteracting === false) {
        lon += .05;
    }
    lat = Math.max(-85, Math.min(85, lat));
    phi = THREE.Math.degToRad(90 - lat);
    theta = THREE.Math.degToRad(lon);
    camera.position.x = 100 * Math.sin(phi) * Math.cos(theta);
    camera.position.y = 100 * Math.cos(phi);
    camera.position.z = 100 * Math.sin(phi) * Math.sin(theta);
    var log = ("x: " + camera.position.x);
    log = log + ("<br/>y: " + camera.position.y);
    log = log + ("<br/>z: " + camera.position.z);
    log = log + ("<br/>fov: " + fov);
    
    camera.lookAt(scene.position);
    renderer.render(scene, camera);
}

    /* Functions */

    panoramaCaruserl();
    buttonControl();
    fullscreenPanorama();
    buttonZoom();
    playMusic();
    fullPagePanorama();
    leaveFullScreen();

    function panoramaCaruserl(){
        if(autoScrol===false) {
           isUserInteracting = true; 
        }
        $('#elipse_key').click(function(){
            if(isUserInteracting === false){
                isUserInteracting = true;
            }
            else{
                isUserInteracting = false; 
            }
            render();
        });
    }

    function buttonControl() {

        $('#right_key').on('mousedown touchstart', onKeyRight);
        $('#left_key').on('mousedown touchstart', onKeyLeft);
        $('#down_key').on('mousedown touchstart', onKeyDown);
        $('#up_key').on('mousedown touchstart', onKeyUp);
    }

    function buttonZoom(){
        $('#zoom_in').on('mousedown touchstart', zoomIn);
         $('#zoom_out').on('mousedown touchstart', zoomOut);
    }
    

   function zoomIn() {
        var counter = 0;
        var tt=setInterval(function(){startTime()},10);
        function startTime() {
            if (fov < 45 || fov > 90) {
            fov = (fov < 45) ? 45 : 90;
            }

            fov-=0.25;
              camera.projectionMatrix.makePerspective(fov, window.innerWidth / window.innerHeight, 1, 1100);
            if(counter == 40) {
                clearInterval(tt);
            } else {
                counter++;
            }
    }
 
    }

   function zoomOut() {
        var counter = 0;
        var tt=setInterval(function(){startTime()},10);
        function startTime() {
            if (fov < 45 || fov > 90) {
            fov = (fov < 45) ? 45 : 90;
             }
            fov+=0.25;
            camera.projectionMatrix.makePerspective(fov, window.innerWidth / window.innerHeight, 1, 1100);
            if(counter == 40) {
                clearInterval(tt);
            } else {
                counter++;
            }
        }

    }

    function fullscreenPanorama(){
         $('#fullscreenPan').bind('touchstart click', function() {
            $('#panorama').toggleFullScreen();
            fullscreenMode = true;
        });
            leaveFullScreen();
        }



    function leaveFullScreen(){
            $('#panorama').fullScreen(false);
            fullscreenMode = false;
    }
  

    function playMusic(){
        $('#pButton').click(function() {
             if (music.paused) {
            music.play();
            // remove play, add pause
            pButton.className = "";
            pButton.className = "fa fa-volume-up";
        } else { // pause music
            music.pause();
            // remove pause, add play
            pButton.className = "";
            pButton.className = "fa fa-volume-off";
        }
        });
    }


    function fullPagePanorama(){
        $('#panorama').css('height', windowHeight + 'px');

    }
    function resizeFullPage(){
         $('#panorama').css('height', $(document).height() + 'px');
    }



});
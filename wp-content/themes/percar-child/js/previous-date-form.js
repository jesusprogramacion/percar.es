(function() {

    var brand = jQuery("select[name=brand]"),
        model = jQuery("select[name=model]");

    var brands = {
        'audi': {
            'name': 'Audi',
            'models' : ['A1', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'Q3', 'Q5', 'Q7', 'TT', 'R8']
        },
        'volkswagen': {
            'name': 'Volkswagen',
            'models': ['Up!', 'Polo', 'Golf', 'The Beetle', 'Scirocco', 'Jetta', 'Passat', 'CC', 'Touran', 'Sharan', 'Tiguan', 'Touareg', 'Phaeton', 'XL1']
        },
        'volkswagen-comerciales': {
            'name': 'Volkswagen Comerciales',
            'models': ['Caddy', 'Transporter', 'Multivan', 'California', 'Caravelle', 'Crafter', 'Amarok']
        }
    };

    for(var key in brands) {

        brand.append('<option value="' + key + '" >' + brands[key].name + '</option>');
    }

    if( brand.val() === "Seleccione Marca" ) {

        model.attr('disabled', true);
    }

    brand.on('change', function() {

        if( brand.val() !== "Seleccione Marca" ) {

            model.attr('disabled', false)
                 .empty()
                 .append('<option value="default">Selecccione Modelo</option>');

            var list = brands[brand.val()].models;

            for( var i = 0; i < list.length; i++) {

                model.append('<option value="' + list[i].toLowerCase().replace(' ', '-') + '" >' + list[i] + '</option>');
            }
        }
        else {

            model.empty()
                 .append('<option value="default">Selecccione Modelo</option>')
                 .attr('disabled', true);
        }
    });

})(jQuery);

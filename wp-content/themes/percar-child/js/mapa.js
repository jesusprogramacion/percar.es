function initialize() {
    
    var coordenadasMapa = new google.maps.LatLng(42.2180209,-8.7027725);
    
    var ubiAudi = new google.maps.LatLng(42.218189, -8.703011);
    var ubiVW = new google.maps.LatLng(42.217916, -8.702641);
        
    var color = [
        {
          featureType: "all",
          elementType: "all",
          stylers: [
            { saturation: -100 }
          ]
        }
    ];
 
    var opciones = {
        zoom: 19,
        center: coordenadasMapa,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    var mapa = new google.maps.Map(document.getElementById('mi-mapa'), opciones);
 
    var estilo = new google.maps.StyledMapType(color);
    mapa.mapTypes.set('mapa-bn', estilo);
    mapa.setMapTypeId('mapa-bn');
 
    var iconoAudi = 'https://percar.es/wp-content/themes/percar-child/images/iconos/iconoAudi.png';
    var iconoVW = 'https://percar.es/wp-content/themes/percar-child/images/iconos/iconoVW.png';
    
    var marker = new google.maps.Marker({
          position: ubiAudi,
          map: mapa,
          title: 'Pérez Rumbao Car | Concesionario Oficial Audi',
          icon: iconoAudi
        });
    
    var marker2 = new google.maps.Marker({
          position: ubiVW,
          map: mapa,
          title: 'Pérez Rumbao Car | Concesionario Oficial Volkswagen',
          icon: iconoVW
        });
 
}
 
google.maps.event.addDomListener(window, 'load', initialize);
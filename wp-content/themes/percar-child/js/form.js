(function() {

    var marca = jQuery("select[name=marca]"),
        modelo = jQuery("select[name=modelo]"),
        wrapModel = jQuery(".cita-select-model"),
        wrapInputBrand = jQuery(".cita-input-brand");

    var path = location.pathname.substr(1);

    if(path == 'cita-previa/'){
        var marcas = {
            'audi': {
                'name': 'Audi',
                'modelos' : ['A1', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'Q3', 'Q5', 'Q7', 'TT', 'R8']
            },
            'volkswagen': {
                'name': 'Volkswagen',
                'modelos': ['Up!', 'Polo', 'Golf', 'The Beetle', 'Scirocco', 'Jetta', 'Passat', 'CC', 'Touran', 'Sharan', 'Tiguan', 'Touareg', 'Phaeton', 'XL1']
            },
            'volkswagen-comerciales': {
                'name': 'Volkswagen Comerciales',
                'modelos': ['Caddy', 'Transporter', 'Multivan', 'California', 'Caravelle', 'Crafter', 'Amarok']
            },
            'Otros': {
                'name': 'Otro',
                'modelos': []
            }
        };
    }else{
        var marcas = {
            'audi': {
                'name': 'Audi',
                'modelos' : ['A1', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'Q3', 'Q5', 'Q7', 'TT', 'R8']
            },
            'volkswagen': {
                'name': 'Volkswagen',
                'modelos': ['Up!', 'Polo', 'Golf', 'The Beetle', 'Scirocco', 'Jetta', 'Passat', 'CC', 'Touran', 'Sharan', 'Tiguan', 'Touareg', 'Phaeton', 'XL1']
            },
            'volkswagen-comercial': {
                'name': 'Volkswagen Comerciales',
                'modelos': ['Caddy', 'Transporter', 'Multivan', 'California', 'Caravelle', 'Crafter', 'Amarok']
            }
        };
    }


    for(var key in marcas) {
        // if(key == 'volkswagen-comerciales'){
        //     value = 'volkswagen-comercial';
        // }else{value = key;} 
        marca.append('<option value="' + key + '" >' + marcas[key].name + '</option>');
    }

    if( marca.val() === "Selecciona marca" ) {

        modelo.attr('disabled', true);
    }

    marca.on('change', function() {

        if( marca.val() !== "Selecciona marca") {

            if(marca.val() === 'Otros'){
                wrapModel.hide();
                wrapInputBrand.show();
            }else{
                wrapModel.show();
                wrapInputBrand.hide();
                modelo.attr('disabled', false)
                 .empty()
                 .append('<option value="">Selecciona modelo</option>');

                var list = marcas[marca.val()].modelos;

                for( var i = 0; i < list.length; i++) {

                    modelo.append('<option value="' + list[i].toLowerCase().replace(' ', '-') + '" class="prov">' + list[i] + '</option>');
                }
            }

        }
        else {

    
                 modelo.empty()
                 .append('<option value="">Selecciona modelo</option>')
                 .attr('disabled', true);
       
           
        }
        modelo.niceSelect('update');
    });
           
})(jQuery);

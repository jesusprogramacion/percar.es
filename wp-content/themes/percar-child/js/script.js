jQuery(document).ready(function(){
    
   jQuery('.vehiculos-slider').slick({
        infinite: true,
        touchMove: true,
        adaptiveHeight: true,
        autoplay: true,
        speed: 300,
        arrows: true,
        prevArrow: getCarouselUrlArrows()[0],
        nextArrow: getCarouselUrlArrows()[1],
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 702,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    jQuery('.vehiculos-slider-blue').slick({
        infinite: true,
        touchMove: true,
        adaptiveHeight: true,
        autoplay: true,
        speed: 300,
        arrows: true,
        prevArrow: getCarouselUrlArrows()[2],
        nextArrow: getCarouselUrlArrows()[3],
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 702,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    function getCarouselUrlArrows() {

        var baseUrl = window.location.protocol + "//" + window.location.host + "/";

        return [
            '<div type="button" data-role="none" class="slick-prev" id="vehicles-slider-prev"><img src="' + baseUrl + '/wp-content/themes/percar-child/images/prev.png"></div>',
            '<div type="button" data-role="none" class="slick-next" id="vehicles-slider-next"><img src="' + baseUrl + '/wp-content/themes/percar-child/images/next.png"></div>',
            '<div type="button" data-role="none" class="slick-prev" id="vehicles-slider-prev"><img src="' + baseUrl + '/wp-content/themes/percar-child/images/prev-vw.png"></div>',
            '<div type="button" data-role="none" class="slick-next" id="vehicles-slider-next"><img src="' + baseUrl + '/wp-content/themes/percar-child/images/next-vw.png"></div>'
        ];
    }
	
	function shuffleArray(array) {
		for (var i = array.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
		return array;
	}
	
	/* shuffle people
	(function() {
		// 1 - 32
		var path = '../wp-content/themes/percar-child/images/galerias/empleados/';
		var people = shuffleArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]);
		
		// background-image
		// <a class="psimages" data-image
		
		$('.emp').each(function(index, elem) {
			var someone = people.pop();
			var pic = 'url(' + path + 'empleado' + someone + '.jpg' + ')';
			console.log(pic);
			$(elem).css("background-image", pic);
		});
	})();*/
    
    (function() {
        var path = '../wp-content/themes/percar-child/images/galerias/empleados/miniaturas/';
        var people_v = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
        var people_h = [6, 7, 8, 9, 24, 25, 26, 27, 28, 29, 30, 31];
        var people_c = [1, 2, 3, 4, 5];
		
		function shuffle_employes(className, array) {
			array = shuffleArray(array);
			$(className).each(function(index, elem) {
				var someone = array.pop();
				var gal_url = '/wp-content/themes/percar-child/images/galerias/empleados/empleado' + someone + '.jpg';
				var css_url = 'url(' + path + 'empleado' + someone + '.jpg' + ')';
				var gallery = $(elem).find('a.psimages')[0];
				var gal_urls = $(gallery).data('image').split(',');
				var index = gal_urls.indexOf(gal_url);
				if (index > -1) {
					gal_urls.splice(index, 1);
				}
				gal_urls.splice(0, 0, gal_url);
				$(gallery).data('image', gal_urls.join());
				$(elem).css("background-image", css_url);
			});
		}
		
		shuffle_employes('.emp-v', people_v);
		shuffle_employes('.emp-h', people_h);
		shuffle_employes('.emp-c', people_c);
        
    })();

    init();
});


function touchHandler() {
    var touch = event.changedTouches[0];

    var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent({
        touchstart: "mousedown",
        touchmove: "mousemove",
        touchend: "mouseup"
    }[event.type], true, true, window, 1,
        touch.screenX, touch.screenY,
        touch.clientX, touch.clientY, false,
        false, false, false, 0, null);

    touch.target.dispatchEvent(simulatedEvent);
    // event.preventDefault();
}

function init() {
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);
}

function nImagen(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}

jQuery(document).ready(function($){

	var urlAleatoria = "url(../wp-content/themes/percar-child/images/conocenos" + nImagen(1, 2) + ".jpg)";
    $('.conocenos').css("background-image", urlAleatoria);

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("#boton-conocenos").toggleClass("hidden");
		$("div.velo").toggleClass("velo-off");
		$("div.velo").toggleClass("velo-on");
	}

	$("#link-conocenos").hover(function() {
		$("#boton-conocenos").toggleClass("hidden");
		$("div.velo").toggleClass("velo-off");
		$("div.velo").toggleClass("velo-on");
	});
	
});




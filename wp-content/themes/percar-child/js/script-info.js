jQuery(document).ready(function(){

    var usuario = jQuery('.info-usuario');

    var nombre = usuario.data('nombre'),
        email = usuario.data('email'),
        telefono = usuario.data('telefono'),
        imagen = usuario.data('imagen');


    if(nombre){
        jQuery("input[name*='nombre']").val(nombre);
    }
    if(email){
        jQuery("input[name*='email']").val(email);
    }
    if(telefono){
        jQuery("input[name*='telefono']").val(telefono);
    }
    if(imagen){
        jQuery("input[name*='imagen']").val(imagen);
    }

});

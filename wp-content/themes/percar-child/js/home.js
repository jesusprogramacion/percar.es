jQuery(document).ready(function () {
    
    var index = null,
        video = null;

    //function centers the render
    function changeVideoSize() {

        // navbar height => 69px
        // footer height => 93px
        // wp navbar height => 32px

        var homeHeight = jQuery(window).height() - 69 - 93,
            wrapperHome = jQuery('.wrapper-home'),
            wrapperVideo = jQuery('.wrapper-video-section');

        if( jQuery(window).width() > 992 ) {

            wrapperHome.css('height', homeHeight);
            wrapperVideo.css('height', homeHeight);
        }
        else {

            wrapperHome.css('height', 'auto');
            wrapperVideo.css('height', homeHeight + 69);

            jQuery('.wrapper-video-section:eq(0)').css('height', homeHeight + 69);
            jQuery('.wrapper-video-section:eq(1), .wrapper-video-section:eq(2)').css('height', $(window).height());
        }

        var resizeVideo = function(width, height) {

            jQuery('.wrapper-video-section video').css({
                'width': width,
                'height': height
            });
        };

        if( wrapperVideo.width() > wrapperVideo.height() ) {

            resizeVideo(wrapperVideo.width() + 100, 'auto');
        }
        else {

            resizeVideo('auto', jQuery(window).height() + 100);
        }
    }

    // Resize the video
    changeVideoSize();

    jQuery(window).resize(function () {

        changeVideoSize();
    });

    // Animate layouts
    jQuery(".white-layer").mouseover(function () {

        index = jQuery(this).data('video');
        video = jQuery('.video-' + index);
        video[0].currentTime = 0;
        video[0].play();
    });

    jQuery(".wrapper-animation").mouseover(function () {

        jQuery('.white-layer:eq('+ --index +')').css('display', 'none');

    }).mouseout(function () {

        jQuery('.white-layer:eq('+ index +')').css('display', 'block');
    });
});